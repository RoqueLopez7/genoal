-- MySQL dump 10.13  Distrib 5.5.24, for Win32 (x86)
--
-- Host: localhost    Database: genoal
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `genoal`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `genoal` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `genoal`;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Asignatura` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
INSERT INTO `asignatura` VALUES (1,'Algoritmo 1'),(2,'Algoritmo 2');
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacto`
--

DROP TABLE IF EXISTS `contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Telefono` varchar(11) DEFAULT NULL,
  `Facebook` varchar(75) DEFAULT NULL,
  `Twitter` varchar(75) DEFAULT NULL,
  `Instagram` varchar(75) DEFAULT NULL,
  `Correo` varchar(150) NOT NULL,
  `Telegram` varchar(75) DEFAULT NULL,
  `LoL` varchar(100) DEFAULT NULL,
  `WoW` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacto`
--

LOCK TABLES `contacto` WRITE;
/*!40000 ALTER TABLE `contacto` DISABLE KEYS */;
INSERT INTO `contacto` VALUES (1,'04123411367','Roque','RELL','Roque','roll@gmail.com','RoL','rell','uzuman'),(2,'','Roberto Lara','RoLA','RobertoLara','Roberto@gmail.com','RL','Rool','Rool'),(3,'04144315587','Carlos Perez','@CarlosP','@CarlosP','carlos@gmail.com','@CarlosP','Carlox','Carlox'),(4,'STREET','Fifero','Fifero','Fifero','FIFAAAA@gmail.com','Fifero','FiferoSoy','SoyFifero'),(5,'','Pedro Garcia','@Pedro','@Pedro','pedrogarcia@gmail.com','@Pedro','Pedro','Pedro'),(6,'3244','da','ad','da','pedro@gmail.com','da','da','da'),(7,'454545','PAblo','@Pablo','@Pablo','Pablo@gmail.com','@Pablo','Pablo','Pablo'),(8,'4566765','Leo','Leo','Leo','Leo@gmail.com','Leo','Leo','Leo'),(9,'431234','Luis','Luis','Luis','Luis@gmail.com','Luis','Luis','Luis'),(10,'','Luisa Hernandez','Luisa Hernandez','Luisa Hernandez','Luisa@gmail.com','Luisa Hernandez','Luisa Hernandez','Luisa Hernandez'),(11,'','Roque','Roque','roque','roque@gmail.com','Roque','Roque','Roque');
/*!40000 ALTER TABLE `contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `control_estudiantes`
--

DROP TABLE IF EXISTS `control_estudiantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_estudiantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_Estudiante` int(11) NOT NULL,
  `id_Notas_A1` int(11) DEFAULT NULL,
  `id_Notas_A2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_estudiantes`
--

LOCK TABLES `control_estudiantes` WRITE;
/*!40000 ALTER TABLE `control_estudiantes` DISABLE KEYS */;
INSERT INTO `control_estudiantes` VALUES (1,1,1,NULL),(2,2,2,NULL),(3,3,3,NULL),(4,4,4,NULL),(5,7,NULL,1),(6,10,NULL,6),(7,11,5,NULL);
/*!40000 ALTER TABLE `control_estudiantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudiante`
--

DROP TABLE IF EXISTS `estudiante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estudiante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `cedula` varchar(15) NOT NULL,
  `id_contacto` int(11) NOT NULL,
  `Fecha_Ingreso` date DEFAULT NULL,
  `seccion` int(11) NOT NULL,
  `id_asignatura` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudiante`
--

LOCK TABLES `estudiante` WRITE;
/*!40000 ALTER TABLE `estudiante` DISABLE KEYS */;
INSERT INTO `estudiante` VALUES (1,'Roque','Lopez','28012038',1,'2021-02-04',7,1),(2,'Roberto Andres','Lara Lara','29333333',2,'2021-03-05',4,1),(3,'Carlos Eduardo','Acosta Perez','30012038',3,'2021-04-05',1,1),(4,'Fifero','FA','121212',4,'2021-03-09',4,1),(5,'Pedro Emilio','Hernandez Garcia','29031391',5,'2021-04-26',8,2),(6,'Pedro','Garcia','29000000',6,'2021-03-27',8,2),(7,'Pablo','Sanchez','30012040',7,'2021-03-24',8,2),(8,'Carlos','Suarez','25555555',8,'2021-03-24',1,2),(9,'Luis','Torres','28900900',9,'2021-03-13',2,2),(10,'Luisa','Hernandez','23456789',10,'2021-04-14',5,2),(11,'Roque Emilio','Lopez Loreto','28012000',11,'2021-03-18',1,1);
/*!40000 ALTER TABLE `estudiante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maraton`
--

DROP TABLE IF EXISTS `maraton`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maraton` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha_Inicio` date DEFAULT NULL,
  `id_Asignatura` int(11) DEFAULT NULL,
  `Seccion` int(11) DEFAULT NULL,
  `Unidad` int(11) DEFAULT NULL,
  `id_Ganador_Oro` int(11) DEFAULT NULL,
  `id_Ganador_Plata` int(11) DEFAULT NULL,
  `id_Ganador_Bronce` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maraton`
--

LOCK TABLES `maraton` WRITE;
/*!40000 ALTER TABLE `maraton` DISABLE KEYS */;
INSERT INTO `maraton` VALUES (1,'2021-01-01',1,1,1,0,0,0),(2,'2021-04-16',2,1,1,0,0,0),(3,'2021-04-13',2,8,1,5,7,6);
/*!40000 ALTER TABLE `maraton` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notas_algoritmo1`
--

DROP TABLE IF EXISTS `notas_algoritmo1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notas_algoritmo1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nota1` int(11) DEFAULT NULL,
  `Nota2` int(11) DEFAULT NULL,
  `Nota3` int(11) DEFAULT NULL,
  `Nota4` int(11) DEFAULT NULL,
  `Nota5` int(11) DEFAULT NULL,
  `Promedio` float DEFAULT NULL,
  `id_Estudiante` int(11) DEFAULT NULL,
  `Seccion` int(11) NOT NULL,
  `Estado` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notas_algoritmo1`
--

LOCK TABLES `notas_algoritmo1` WRITE;
/*!40000 ALTER TABLE `notas_algoritmo1` DISABLE KEYS */;
INSERT INTO `notas_algoritmo1` VALUES (1,0,0,0,0,0,0,1,7,'Cursando'),(2,0,0,0,0,0,60,2,4,'Aprobado'),(3,0,1,0,0,0,0,3,1,'Cursando'),(4,25,0,0,0,0,0,4,4,'Cursando'),(5,93,0,0,0,0,0,11,1,'Cursando');
/*!40000 ALTER TABLE `notas_algoritmo1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notas_algoritmo2`
--

DROP TABLE IF EXISTS `notas_algoritmo2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notas_algoritmo2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nota1` int(11) DEFAULT NULL,
  `Nota2` int(11) DEFAULT NULL,
  `Nota3` int(11) DEFAULT NULL,
  `Nota4` int(11) DEFAULT NULL,
  `Nota5` int(11) DEFAULT NULL,
  `Promedio` float DEFAULT NULL,
  `id_Estudiante` int(11) DEFAULT NULL,
  `Seccion` int(11) NOT NULL,
  `Estado` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notas_algoritmo2`
--

LOCK TABLES `notas_algoritmo2` WRITE;
/*!40000 ALTER TABLE `notas_algoritmo2` DISABLE KEYS */;
INSERT INTO `notas_algoritmo2` VALUES (1,60,0,0,0,0,0,5,8,'Cursando'),(2,70,0,0,0,0,0,6,8,'Cursando'),(3,50,0,0,0,0,0,7,8,'Cursando'),(4,0,0,0,0,0,0,8,1,'Cursando'),(5,0,0,0,0,0,0,9,2,'Cursando'),(6,79,0,0,0,0,0,10,5,'Cursando');
/*!40000 ALTER TABLE `notas_algoritmo2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seguridad`
--

DROP TABLE IF EXISTS `seguridad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seguridad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Contrasena` varchar(100) NOT NULL,
  `Email` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seguridad`
--

LOCK TABLES `seguridad` WRITE;
/*!40000 ALTER TABLE `seguridad` DISABLE KEYS */;
INSERT INTO `seguridad` VALUES (1,'GENOAL202','ejemplo@gmail.com');
/*!40000 ALTER TABLE `seguridad` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-12 16:40:17
