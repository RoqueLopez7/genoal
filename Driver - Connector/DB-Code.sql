create table Control_Estudiantes(
id int not null Auto_Increment,
id_Estudiante int not null,
id_Asignatura int not null,
id_Notas_A1 int ,
id_Notas_A2 int,
Primary Key(id)
);

create table Estudiante(
id int not null Auto_Increment,
nombres varchar(100) not null,
apellidos varchar(100) not null,
cedula varchar(15) not null,
id_contacto int not null,
Fecha_Ingreso date,
seccion int not null,
Primary Key(id)
);

create table contacto(
id int not null Auto_Increment,
Telefono varchar(11),
Facebook varchar(75),
Twitter varchar(75),
Instagram varchar(75),
Correo varchar(150) not null,
Telegram varchar(75),
LoL varchar(100),
WoW varchar(100),
Primary key(id)
);

create table Notas_Algoritmo1(
id int not null Auto_Increment,
Nota1 int,
Nota2 int,
Nota3 int,
Nota4 int,
Nota5 int,
Promedio float,
id_Estudiante int,
Primary Key(id)
);

create table Notas_Algoritmo2(
id int not null Auto_Increment,
Nota1 int,
Nota2 int,
Nota3 int,
Nota4 int,
Nota5 int,
Promedio float,
id_Estudiante int,
Primary Key(id)
);

create table Maraton(
id int not null Auto_Increment,
Fecha_Inicio date,
id_Asignatura int,
Seccion int,
Unidad int,
id_Ganador_Oro int,
id_Ganador_Plata int,
id_Ganador_Bronce int,
Primary Key (id)
);

create table Asignatura(
id int not null Auto_Increment,
Asignatura varchar(11),
Primary Key(id)
);

create table Seguridad(
id int Auto_Increment,
Contrasena varchar(100) not null,
Email varchar(150) not null,
Primary Key(id) 
);