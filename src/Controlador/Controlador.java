package Controlador;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import Vista.Vista;
import Vista.MenuPrincipal;
import Vista.Bienvenida;
import Vista.Estudiante;

public class Controlador implements ActionListener{

	//Clases vistas
	private Vista V;
	private MenuPrincipal MP;
	private Bienvenida B;
	private Estudiante Student;

	//Imagenes
    private ImageIcon ImagenClaro,ImagenOpaco, ImagenBlanco;
    private Icon Icono;

	//Atributos
	private int Contador = 0; //Este contador se va al controlador

	public Controlador(Vista V, MenuPrincipal MP, Bienvenida B, Estudiante Student) {
		
		//Iniciar las conexiones
		this.V = V;
		this.MP = MP;
		this.B = B;
		this.Student = Student;
		
		//Añadir oyentes
		Oyentes();
	}
	
	public void Oyentes() {
		//Oyentes de login
		Login(V);
		//Oyentes del menu principal
		MenuPrincipal(MP);
	}
	
	public void Login(Vista V) {
		//V.enviar.addActionListener(this);
	}
	
	public void MenuPrincipal(MenuPrincipal MP) {
		MP.Home.addActionListener(this);
		MP.Student.addActionListener(this);
		MP.Algoritmo1.addActionListener(this);
		MP.Algoritmo2.addActionListener(this);
		MP.Opciones.addActionListener(this);
		MP.Salir.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		/*-----------------------------------------------------------
		*--------------------------->Login<--------------------------
		*-----------------------------------------------------------
		//Si presiona enviar la contraseña para entrar
		if(e.getSource().equals(V.enviar)) {
	
			//Si la contraseña es la clave del desarrollador
			if(V.clave.getText().toString().equals("GENOAL2020")) {
				/*JOptionPane.showMessageDialog(null, "Contraseña correcta");
				V.AgregarPaneles();
				V.Login.setVisible(false);
				MP.BarraFlotante.setVisible(true);
				MP.MenuLateral.setVisible(true);
				Contador = 0;
				V.remove(V.Login);
			} 
			else if(V.clave.getText().toString().equals("")){ //Si el campo esta vacio
				
				JOptionPane.showMessageDialog(null, "El campo está vacío, ingrese la contraseña.");
				
				}else {//Si se equivoca
					
				Contador = Contador + 1; //Contador para seguridad del programa
				
				if(Contador < 3) {//Mensaje de contraseña incorrecta
					JOptionPane.showMessageDialog(null, "Contraseña incorrecta.\nVuelva a intentar.");
				}
			}
		}
		
		//Seguridad y cierre del programa
		if(Contador == 3) { 
			JOptionPane.showMessageDialog(null, "Demasiados intentos, vuelva pronto.");
			System.exit(0);
		}
*/
		/*-----------------------------------------------------------
		*---------------------->Menu Principal<----------------------
		*-----------------------------------------------------------*/
		if(e.getSource().equals(MP.Home)) {
			B.Bienvenida.setVisible(true);
			Student.Estudiante.setVisible(false);
		}
		
		if(e.getSource().equals(MP.Student)) {
			B.Bienvenida.setVisible(false);
			Student.Estudiante.setVisible(true);
			JOptionPane.showMessageDialog(null, "Panel estudiante");
			/*V.add(Stu.Estudiante);
			remove(B.Bienvenida);*/
		}
		
		if(e.getSource().equals(MP.Student)) {
			V.add(Student.Estudiante);
			//remove(Stu.Estudiante);
		}
		
		if(e.getSource().equals(MP.Salir)) {
			System.exit(0);
		}
	}
	

	//--------------->METODO PARA PERSONALIZAR BOTON<---------------
	public void PersonalizarBoton(JButton Boton, ImageIcon ImagenClaro, ImageIcon ImagenOpaco) {
		    
		Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
		     Boton.getHeight(), Image.SCALE_DEFAULT));
		Boton.setIcon(Icono);
		Boton.setOpaque(false);
		Boton.setContentAreaFilled(false);
		Boton.setBorderPainted(false);
				
		MouseListener OyentedeRatonAtras = new MouseListener() {
				
			public void mouseClicked(MouseEvent e) {}

			public void mouseEntered(MouseEvent e) {
						
				Icono = new ImageIcon(ImagenClaro.getImage().getScaledInstance(Boton.getWidth(), 
						Boton.getHeight(), Image.SCALE_DEFAULT));
					Boton.setIcon(Icono);
					Boton.setCursor(new Cursor(Cursor.HAND_CURSOR));
				}

				public void mouseExited(MouseEvent e) {
						
					Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
							Boton.getHeight(), Image.SCALE_DEFAULT));
					Boton.setIcon(Icono);
							
				}

				public void mousePressed(MouseEvent e) {}

				public void mouseReleased(MouseEvent e) {}
					
			};
				
			Boton.addMouseListener(OyentedeRatonAtras);
		    	
	    }
}
