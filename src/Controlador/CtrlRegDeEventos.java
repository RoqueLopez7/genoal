/*package Controladores;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Vistas.Acercade;
import Vistas.Asistencia;
import Vistas.PrincipalRegistro;
import Vistas.Reportes;
import Vistas.VistaPrincipal;

public class CtrlRegDeEventos implements ActionListener {

	
    //Clases externas
    //---->Clases de Vistas<----
    public Asistencia ModuloAsistencia;
    public PrincipalRegistro PanelRegistroPersona;
    public Acercade ModuloAcercade;
    public Reportes Report;
    public VistaPrincipal VP;
    
    Icon Icono;
    
	public CtrlRegDeEventos(VistaPrincipal VP,Asistencia ModuloAsistencia, PrincipalRegistro PanelRegistroPersona, Acercade ModuloAcercade, Reportes Report) {
		
		this.VP = VP;
		this.ModuloAsistencia = ModuloAsistencia;
		this.PanelRegistroPersona = PanelRegistroPersona;
		this.ModuloAcercade = ModuloAcercade;
		this.Report = Report;
        
        //añadir eventos a los componentes de las clases
        VPaddaction(VP);
        PanelRegistroPersonaaddaction(PanelRegistroPersona);
        ModuloAsistenciaaddaction();
        Reportaddaction();
	}
	
	//----------->añadir las acciones de los componentes<-----------
	public void VPaddaction(VistaPrincipal VP){
		//Boton de modo Dark en la barra flotante
        //VP.MDark.addMouseListener(this);
        VP.MDark.addActionListener(this);
        //Botones del menu vertical
        VP.Home.addActionListener(this);
		VP.Registrar.addActionListener(this);
		VP.Asistencia.addActionListener(this);
		VP.Reportes.addActionListener(this);
		VP.acerca.addActionListener(this);
		VP.Salir.addActionListener(this);
	}

    public void PanelRegistroPersonaaddaction(PrincipalRegistro PanelRegistroPersona){
    	//Botones del panel principal de registro
    	PanelRegistroPersona.btnEvento.addActionListener(this);
    	PanelRegistroPersona.btnPersona.addActionListener(this);
        //Botones de Registro de Eventos
    	PanelRegistroPersona.btnAtras.addActionListener(this);
    	PanelRegistroPersona.btnEveRegistrar.addActionListener(this);
    	PanelRegistroPersona.btnEveBuscar.addActionListener(this);
        PanelRegistroPersona.btnEveEditar.addActionListener(this);
        PanelRegistroPersona.btnEveEliminar.addActionListener(this);
        //Botones de Editar un Evento
        PanelRegistroPersona.btnAtras_EditarRegistro.addActionListener(this);
        PanelRegistroPersona.btnRegistrar_EditarEvento.addActionListener(this);
        //Botones de Registro de personas
        PanelRegistroPersona.btnRegistrar.addActionListener(this);
        PanelRegistroPersona.btnBS.addActionListener(this);
        PanelRegistroPersona.btnED.addActionListener(this);
        PanelRegistroPersona.btnELI.addActionListener(this);
        PanelRegistroPersona.btnAS.addActionListener(this);
        //Lista desplegables de Registro persona
        PanelRegistroPersona.TIP.addActionListener(this);
        //Botones de editar personas
        PanelRegistroPersona.btnRegistrare.addActionListener(this);
        PanelRegistroPersona.btnASe.addActionListener(this);
        //Lista desplegables de Editar persona
        PanelRegistroPersona.TIPe.addActionListener(this);
    }

	public void ModuloAsistenciaaddaction(){
        //Añadiendo oyentes a los botones 
        ModuloAsistencia.BuscarAsistenciaButton.addActionListener(this);
        ModuloAsistencia.Aceptar.addActionListener(this);
        //Añadiendo oyentes a los botones 
        ModuloAsistencia.Si.addActionListener(this);
        ModuloAsistencia.No.addActionListener(this);
	}

    public void Reportaddaction(){

    	//Botones
		Report.Acreditacion.addActionListener(this);
		Report.Inscritos.addActionListener(this);
		Report.Certificados.addActionListener(this);
		Report.LibroEvento.addActionListener(this);
		Report.TotalEventos.addActionListener(this);
		//Listas desplegables
        Report.ListaEventos.addActionListener(this);
		Report.TipoParticipante.addActionListener(this);
    }

    //<------------------------------------------------------------>
    //----------->acciones Listener de los componentes<-----------
	@Override
	public void actionPerformed(ActionEvent e) {

	    //---------->Control de Eventos<----------
		//---------->Eventos de VistaPrincipal<----------    
			//activar el modo luminoso
			if(e.getSource().equals(VP.MDark) && VP.ModoN) {
				VP.ModoLuminoso();
				PanelRegistroPersona.ModoLuminoso();
				ModuloAsistencia.ModoLuminoso();
				ModuloAcercade.ModoLuminoso();
				Report.ModoLuminoso();
				VerificarBotonPresionado();
			} else if (e.getSource().equals(VP.MDark)){//activar el modo nocturno
				VP.ModoNocturno();
				PanelRegistroPersona.ModoNocturno();
				ModuloAsistencia.ModoNocturno();
				ModuloAcercade.ModoNocturno();
				Report.ModoNocturno();
				VerificarBotonPresionado();
			}
			
			//El color de los botones depende del modo en el que este
			if(VP.ModoN) {//Modo Nocturno activado
			
			if(e.getSource().equals(VP.Home)) {

				VP.AsignaciondeBotonesModoNocturno();
				
	    		//Cambio de paneles
	    		//Desactiva
				PanelRegistroPersona.PrincipalRegistro.setVisible(false); //Panel principal de Registrar
				PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un evento
				PanelRegistroPersona.RegistroEventos.setVisible(false);//Registrar un evento
				PanelRegistroPersona.RegistroPersona.setVisible(false);//Registrar una Persona
				PanelRegistroPersona.EditarPersona.setVisible(false);//Editar una Persona
	    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
	    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
	    		Report.Reportes.setVisible(false); //Reportes
	    		//Activa
	    		VP.Bienvenida.setVisible(true); //Home
	        }
			
			if(e.getSource().equals(VP.Registrar)){

				VP.AsignaciondeBotonesModoNocturno();
	    		
	    		//Cambio de paneles
	    		//Desactiva
				VP.Bienvenida.setVisible(false); //Home
				PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un evento
				PanelRegistroPersona.RegistroEventos.setVisible(false);//Registrar un evento
				PanelRegistroPersona.RegistroPersona.setVisible(false);//Registrar una Persona
				PanelRegistroPersona.EditarPersona.setVisible(false);//Editar una Persona
	    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
	    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
	    		Report.Reportes.setVisible(false); //Reportes
	    		
	    		//Activa
	    		PanelRegistroPersona.PrincipalRegistro.setVisible(true); //Registrar una Persona 
			}
			
			if(e.getSource().equals(VP.Asistencia)){

				VP.AsignaciondeBotonesModoNocturno();

	    		//Cambio de paneles
	    		//Desactiva
				VP.Bienvenida.setVisible(false); //Home
	    		PanelRegistroPersona.PrincipalRegistro.setVisible(false); //Panel principal de Registrar
	    		PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un evento
	    		PanelRegistroPersona.RegistroEventos.setVisible(false);//Registrar un evento
	    		PanelRegistroPersona.RegistroPersona.setVisible(false);//Registrar una Persona
	    		PanelRegistroPersona.EditarPersona.setVisible(false);//Editar una Persona
	    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
	    		Report.Reportes.setVisible(false);
	    		//Activa   		
	    		ModuloAsistencia.Asistencia.setVisible(true); //Asistencia
			}
			
			if(e.getSource().equals(VP.Reportes)){

				VP.AsignaciondeBotonesModoNocturno();
				
	    		//Cambio de paneles
	    		//Desactiva
				VP.Bienvenida.setVisible(false); //Home
	    		PanelRegistroPersona.PrincipalRegistro.setVisible(false); //Panel principal de Registrar
	    		PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un evento
	    		PanelRegistroPersona.RegistroEventos.setVisible(false);//Registrar un evento
	    		PanelRegistroPersona.RegistroPersona.setVisible(false);//Registrar una Persona
	    		PanelRegistroPersona.EditarPersona.setVisible(false);//Editar una Persona
	    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
	    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
	    		//Activa   		
	    		Report.Reportes.setVisible(true);
			}
			
			if(e.getSource().equals(VP.acerca)){

				VP.AsignaciondeBotonesModoNocturno();
				
	    		//Cambio de paneles
	    		//Desactiva
				VP.Bienvenida.setVisible(false); //Home
	    		PanelRegistroPersona.PrincipalRegistro.setVisible(false); //Panel principal de Registrar
	    		PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un evento
	    		PanelRegistroPersona.RegistroEventos.setVisible(false);//Registrar un evento
	    		PanelRegistroPersona.RegistroPersona.setVisible(false);//Registrar una Persona
	    		PanelRegistroPersona.EditarPersona.setVisible(false);//Editar una Persona    		
	    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
	    		Report.Reportes.setVisible(false);
	    		//Activa   		
	    		ModuloAcercade.Acercade.setVisible(true); //Acerca de
			}
			
			if(e.getSource().equals(VP.Salir)){
				
				VP.AsignaciondeBotonesModoNocturno();
				
				System.exit(0);
			}
			//VerificarBotonPresionado();
		  } else {//Modo Luminoso activado
			  
			  if(e.getSource().equals(VP.Home)) {
				  
				  VP.AsignaciondeBotonesModoLuminoso();
					
		    		//Cambio de paneles
		    		//Desactiva
		    		PanelRegistroPersona.PrincipalRegistro.setVisible(false); // Panel principal del registro
					PanelRegistroPersona.RegistroEventos.setVisible(false);
		    		PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un Evento 
		    		PanelRegistroPersona.RegistroPersona.setVisible(false); //Registrar una Persona 
		    		PanelRegistroPersona.EditarPersona.setVisible(false); //Editar una Persona 
		    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
		    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
		    		Report.Reportes.setVisible(false);
		    		//Activa
		    		VP.Bienvenida.setVisible(true); //Home
		        }
				
				if(e.getSource().equals(VP.Registrar)){

					VP.AsignaciondeBotonesModoLuminoso();
					
		    		//Cambio de paneles
		    		//Desactiva
					VP.Bienvenida.setVisible(false); //Home
					PanelRegistroPersona.RegistroEventos.setVisible(false);
					PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un Evento 
		    		PanelRegistroPersona.RegistroPersona.setVisible(false); //Registrar una Persona 
		    		PanelRegistroPersona.EditarPersona.setVisible(false); //Editar una Persona 
		    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
		    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
		    		Report.Reportes.setVisible(false);
		    		//Activa
		    		PanelRegistroPersona.PrincipalRegistro.setVisible(true); //Registrar una Persona 
				}
				
				if(e.getSource().equals(VP.Asistencia)){

					VP.AsignaciondeBotonesModoLuminoso();
		    		
		    		//Cambio de paneles
		    		//Desactiva
					VP.Bienvenida.setVisible(false); //Home
		    		PanelRegistroPersona.PrincipalRegistro.setVisible(false); // Panel principal del registro
					PanelRegistroPersona.RegistroEventos.setVisible(false);
		    		PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un Evento 
		    		PanelRegistroPersona.RegistroPersona.setVisible(false); //Registrar una Persona 
		    		PanelRegistroPersona.EditarPersona.setVisible(false); //Editar una Persona 
		    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
		    		Report.Reportes.setVisible(false);
		    		//Activa   		
		    		ModuloAsistencia.Asistencia.setVisible(true); //Asistencia
				}
				
				if(e.getSource().equals(VP.Reportes)){

					VP.AsignaciondeBotonesModoLuminoso();
		    		
		    		//Cambio de paneles
		    		//Desactiva
					VP.Bienvenida.setVisible(false); //Home
		    		PanelRegistroPersona.PrincipalRegistro.setVisible(false); // Panel principal del registro
					PanelRegistroPersona.RegistroEventos.setVisible(false);
		    		PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un Evento 
		    		PanelRegistroPersona.RegistroPersona.setVisible(false); //Registrar una Persona 
		    		PanelRegistroPersona.EditarPersona.setVisible(false); //Editar una Persona 
		    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
		    		ModuloAcercade.Acercade.setVisible(false); //Acerca de
		    		//Activa   		
		    		Report.Reportes.setVisible(true);
				}
				
				if(e.getSource().equals(VP.acerca)){

					VP.AsignaciondeBotonesModoLuminoso();
		    		
		    		//Cambio de paneles
		    		//Desactiva
					VP.Bienvenida.setVisible(false); //Home
					PanelRegistroPersona.RegistroEventos.setVisible(false);
		    		PanelRegistroPersona.PrincipalRegistro.setVisible(false); // Panel principal del registro
		    		PanelRegistroPersona.EditarEvento.setVisible(false); //Editar un Evento 
		    		PanelRegistroPersona.RegistroPersona.setVisible(false); //Registrar una Persona 
		    		PanelRegistroPersona.EditarPersona.setVisible(false); //Editar una Persona 
		    		ModuloAsistencia.Asistencia.setVisible(false); //Asistencia
		    		Report.Reportes.setVisible(false);
		    		//Activa   		
		    		ModuloAcercade.Acercade.setVisible(true); //Acerca de
				}
				
				if(e.getSource().equals(VP.Salir)){

					VP.AsignaciondeBotonesModoLuminoso();
					
					System.exit(0);
				}
				
				VP.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				//VerificarBotonPresionado();
		    }	
			//<-------------------------------------------------------------->
	        //----------------->Eventos de PrincipalRegistro<----------------- 
			//---------->Eventos de Menu de Registro<---------- 
		    if(e.getSource().equals(PanelRegistroPersona.btnEvento)) {
				
			    PanelRegistroPersona.PrincipalRegistro.setVisible(false);
			    PanelRegistroPersona.RegistroEventos.setVisible(true);
			    PanelRegistroPersona.RegistroPersona.setVisible(false);
				
			}
		    
			if(e.getSource().equals(PanelRegistroPersona.btnPersona)) {
				
				PanelRegistroPersona.txtEPFe.setVisible(false);
				PanelRegistroPersona.txtEPF.setVisible(false);					
				PanelRegistroPersona.PrincipalRegistro.setVisible(false);
				PanelRegistroPersona.RegistroEventos.setVisible(false);
				PanelRegistroPersona.RegistroPersona.setVisible(true);
			}
			
			//--------------->Eventos de PANEL REGISTRO EVENTO<---------------
			if(e.getSource().equals(PanelRegistroPersona.btnAtras)) {
				
				PanelRegistroPersona.PrincipalRegistro.setVisible(true);
				PanelRegistroPersona.RegistroEventos.setVisible(false);
			}
			
			String [] Botones = {"Aceptar", "Cancelar"};
			
			if(e.getSource().equals(PanelRegistroPersona.btnEveEditar)) {
				
				int R = JOptionPane.showOptionDialog(null, "¿Esta Seguro que desea editar este registro?",
						"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
						null, Botones, Botones[0]);
				
				if(R==0) {
					
					PanelRegistroPersona.PrincipalRegistro.setVisible(false);
					PanelRegistroPersona.RegistroEventos.setVisible(false);
					PanelRegistroPersona.EditarEvento.setVisible(true);
					
				}
			}
			
			//--------------->Eventos de PANEL EDITAR EVENTO<---------------
			if(e.getSource().equals(PanelRegistroPersona.btnAtras_EditarRegistro)) {
				
				PanelRegistroPersona.PrincipalRegistro.setVisible(false);
				PanelRegistroPersona.RegistroEventos.setVisible(true);
				PanelRegistroPersona.EditarEvento.setVisible(false);
				
			}

            //Si registran el cambio
			if(e.getSource().equals(PanelRegistroPersona.btnRegistrar_EditarEvento)){

			}
            
            //--------------->Eventos de PANEL Registrar una persona<---------------
            //Si presionan registrar
            if(e.getSource().equals(PanelRegistroPersona.btnRegistrar)){}
            //Si presionan buscar
            if(e.getSource().equals(PanelRegistroPersona.btnBS)){}	
            //Si presionan editar
			if(e.getSource().equals(PanelRegistroPersona.btnED)) {
						
				int R = JOptionPane.showOptionDialog(null, "¿Esta Seguro que desea editar este registro?",
						"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
						null, Botones, Botones[0]);
						
				if(R==0) {
							
					PanelRegistroPersona.PrincipalRegistro.setVisible(false);
					PanelRegistroPersona.RegistroPersona.setVisible(false);
					PanelRegistroPersona.EditarPersona.setVisible(true);
						
					PanelRegistroPersona.txtBS_ED_ELI.setText("");		
				}
			}
            //Si presionan eliminar
			if(e.getSource().equals(PanelRegistroPersona.btnELI)){}
            //Si presionan atras, regresa al menu principal de registro 
			if(e.getSource().equals(PanelRegistroPersona.btnAS)) {
						
				PanelRegistroPersona.PrincipalRegistro.setVisible(true);
				PanelRegistroPersona.RegistroPersona.setVisible(false);				
			}
            //Si seleccionan "Otro" en la lista desplegable en registrar persona   
		    if(PanelRegistroPersona.TIP.getSelectedItem().equals("Otro (Especifique)"))
		    {
                PanelRegistroPersona.txtEPF.setVisible(true);
                PanelRegistroPersona.lblEPF.setVisible(true);
                
		    }else{
		    	
                PanelRegistroPersona.txtEPF.setVisible(false);
                PanelRegistroPersona.lblEPF.setVisible(false);
		    }
		    //Si seleccionan "Otro" en la lista desplegable en editar persona  
		    if(PanelRegistroPersona.TIPe.getSelectedItem().equals("Otro (Especifique)")) {
		    	PanelRegistroPersona.txtEPFe.setVisible(true);
		    	PanelRegistroPersona.lblEPFe.setVisible(true);
		    }else {
		    	PanelRegistroPersona.txtEPFe.setVisible(false);  
		    	PanelRegistroPersona.lblEPFe.setVisible(false);
		    }


		    //--------------->Eventos de PANEL Editar una persona<---------------
		    if(e.getSource().equals(PanelRegistroPersona.btnRegistrare)){}

		    if(e.getSource().equals(PanelRegistroPersona.btnASe)) {
						
				PanelRegistroPersona.txtEPFe.setVisible(false);
				PanelRegistroPersona.txtEPF.setVisible(false);		
				PanelRegistroPersona.EditarPersona.setVisible(false);
				PanelRegistroPersona.RegistroPersona.setVisible(true);
			}

			//--------------->Eventos de PANEL asistencia<---------------
			if(e.getSource().equals(ModuloAsistencia.Aceptar)){}

            if(e.getSource().equals(ModuloAsistencia.BuscarAsistenciaButton)){
            	if(ModuloAsistencia.BuscarAsistencia.getText() == null || ModuloAsistencia.BuscarAsistencia.getText() == "" || 
					ModuloAsistencia.SelecEvent.getSelectedIndex() == 0){
                	JOptionPane.showMessageDialog(null, "Faltan campos por llenar para realizar la busqueda.");
			    }else{//Busqueda de la persona

			    } 
            }

			if(ModuloAsistencia.Si.isSelected() || ModuloAsistencia.No.isSelected()){ ModuloAsistencia.Aceptar.setEnabled(true);} 
			else {ModuloAsistencia.Aceptar.setEnabled(false);}

			//--------------->Eventos de PANEL Reportes<---------------
			//Seguridad para la realizacion de reportes
			if(Report.ListaEventos.getSelectedIndex()  == 0 || Report.TipoParticipante.getSelectedIndex()  == 0) {
				Report.Acreditacion.setEnabled(false);
				Report.Inscritos.setEnabled(false);
				Report.Certificados.setEnabled(false);
				Report.LibroEvento.setEnabled(false);
				Report.TotalEventos.setEnabled(false);
			} else 
				if(Report.ListaEventos.getSelectedIndex() > 0 && Report.TipoParticipante.getSelectedIndex() > 0){
					Report.Acreditacion.setEnabled(true);
					Report.Inscritos.setEnabled(true);
					Report.Certificados.setEnabled(true);
					Report.LibroEvento.setEnabled(true);
					Report.TotalEventos.setEnabled(true);
			}
			
			//Si presionan el boton de Acreditacion
			if(e.getSource().equals(Report.Acreditacion)) {
				
			}
			
			//Si presionan el boton de Inscritos
			if(e.getSource().equals(Report.Inscritos)) {

			}
			
			//Si presionan el boton de Certificados
			if(e.getSource().equals(Report.Certificados)) { JOptionPane.showMessageDialog(null, "Opcion no disponible para esta versión");}
			
			//Si presionan el boton de Libro por Evento
			if(e.getSource().equals(Report.LibroEvento)) {

			}
			
			//Si presionan el boton de Total de Eventos
			if(e.getSource().equals(Report.TotalEventos)) {

			}
	}

	public void VerificarBotonPresionado(){
		ImageIcon ImagenBlanco;
		
		if(!VP.ModoN) {
			if(VP.Bienvenida.isVisible()) {	ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoLuminoso/BotonHouseSelected.png"); this.BotonBlanco(VP.Home, ImagenBlanco);}
			if(PanelRegistroPersona.PrincipalRegistro.isVisible() || PanelRegistroPersona.RegistroEventos.isVisible() ||
					PanelRegistroPersona.EditarEvento.isVisible() || PanelRegistroPersona.RegistroPersona.isVisible() ||
					PanelRegistroPersona.EditarPersona.isVisible()){		ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoLuminoso/BotonRegisterSelected.png");this.BotonBlanco(VP.Registrar, ImagenBlanco);}
			if(ModuloAsistencia.Asistencia.isVisible()){ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoLuminoso/BotonConfirmSelected.png");this.BotonBlanco(VP.Home, ImagenBlanco);}
			if(Report.Reportes.isVisible()){ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoLuminoso/BotonReportesSelected.png"); this.BotonBlanco(VP.Home, ImagenBlanco);}
			if(ModuloAcercade.Acercade.isVisible()){ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoLuminoso/BotonAcercadeSelected.png");this.BotonBlanco(VP.Home, ImagenBlanco);}
		} else {
			if(VP.Bienvenida.isVisible()) {	ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoNocturno/BotonHouseDarkSelected.png");this.BotonBlanco(VP.Home, ImagenBlanco);}
			if(PanelRegistroPersona.PrincipalRegistro.isVisible() || PanelRegistroPersona.RegistroEventos.isVisible() ||
					PanelRegistroPersona.EditarEvento.isVisible() || PanelRegistroPersona.RegistroPersona.isVisible() ||
					PanelRegistroPersona.EditarPersona.isVisible()){		ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoNocturno/BotonRegisterDarkSelected.png");this.BotonBlanco(VP.Registrar, ImagenBlanco);}
			if(ModuloAsistencia.Asistencia.isVisible()){		ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoNocturno/BotonConfirmDarkSelected.png");this.BotonBlanco(VP.Home, ImagenBlanco);}
			if(Report.Reportes.isVisible()){ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoNocturno/BotonReportesDarkSelected.png");this.BotonBlanco(VP.Home, ImagenBlanco);}
			if(ModuloAcercade.Acercade.isVisible()){ImagenBlanco = new ImageIcon("control-de-eventos/src/img/IconosMenu/ModoNocturno/BotonAcercadeDarkSelected.png");this.BotonBlanco(VP.Home, ImagenBlanco);}
		}
	}
	
	//--------------->METODO PARA PERSONALIZAR BOTON<---------------
	public void PersonalizarBoton(JButton Boton, ImageIcon ImagenClaro, ImageIcon ImagenOpaco) {
		    
		Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
		     Boton.getHeight(), Image.SCALE_DEFAULT));
		Boton.setIcon(Icono);
		Boton.setOpaque(false);
		Boton.setContentAreaFilled(false);
		Boton.setBorderPainted(false);
				
		MouseListener OyentedeRatonAtras = new MouseListener() {
				
			public void mouseClicked(MouseEvent e) {}

			public void mouseEntered(MouseEvent e) {
						
				Icono = new ImageIcon(ImagenClaro.getImage().getScaledInstance(Boton.getWidth(), 
						Boton.getHeight(), Image.SCALE_DEFAULT));
					Boton.setIcon(Icono);
					Boton.setCursor(new Cursor(Cursor.HAND_CURSOR));
				}

				public void mouseExited(MouseEvent e) {
						
					Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
							Boton.getHeight(), Image.SCALE_DEFAULT));
					Boton.setIcon(Icono);
							
				}

				public void mousePressed(MouseEvent e) {}

				public void mouseReleased(MouseEvent e) {}
					
			};
				
			Boton.addMouseListener(OyentedeRatonAtras);
		    	
	    }

	//--------------->METODO PARA PERSONALIZAR BOTON del menu<---------------
	public void PersonalizarBoton(JButton Boton, ImageIcon ImagenClaro, ImageIcon ImagenOpaco, JPanel Panel,ImageIcon ImagenBlanco) {
	    
			Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
			Boton.getHeight(), Image.SCALE_DEFAULT));
			Boton.setOpaque(false);
			Boton.setContentAreaFilled(false);
			Boton.setBorderPainted(false);
		    Boton.setIcon(Icono);
			
			MouseListener OyentedeRatonAtras = new MouseListener() {
			
				public void mouseClicked(MouseEvent e) {
					Icono = new ImageIcon(ImagenBlanco.getImage().getScaledInstance(Boton.getWidth(), 
					Boton.getHeight(), Image.SCALE_DEFAULT));
			        Boton.setIcon(Icono);
				    Boton.setOpaque(false);
				    Boton.setContentAreaFilled(false);
				    Boton.setBorderPainted(false);
				    Boton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				    }

				public void mouseEntered(MouseEvent e) {
					
					if(!Panel.isVisible()) {
					Icono = new ImageIcon(ImagenClaro.getImage().getScaledInstance(Boton.getWidth(), 
							Boton.getHeight(), Image.SCALE_DEFAULT));
					Boton.setIcon(Icono);
					Boton.setCursor(new Cursor(Cursor.HAND_CURSOR));
					}else {Boton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));}
					
				}

				public void mouseExited(MouseEvent e) {
					
					if(!Panel.isVisible()){
					Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
							Boton.getHeight(), Image.SCALE_DEFAULT));
					Boton.setIcon(Icono);
					}
				}

				public void mousePressed(MouseEvent e) {}

				public void mouseReleased(MouseEvent e) {if(!Panel.isVisible()) {Boton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));}}
				
			};
			
			Boton.addMouseListener(OyentedeRatonAtras);
	    }	

    //--------------->Boton presionado<---------------
	public void BotonBlanco(JButton Boton, ImageIcon ImagenBlanco) {
		Icono = new ImageIcon(ImagenBlanco.getImage().getScaledInstance(Boton.getWidth(), 
		Boton.getHeight(), Image.SCALE_DEFAULT));
        Boton.setIcon(Icono);
	    Boton.setOpaque(false);
	    Boton.setContentAreaFilled(false);
	    Boton.setBorderPainted(false);
	    Boton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
 
}
*/