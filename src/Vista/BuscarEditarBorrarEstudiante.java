package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BuscarEditarBorrarEstudiante extends JPanel{

	public JPanel BEB; //Panel buscar, editar, borrar
	public JLabel Titulo, IngreseCI;
	public JTextField CI;
	public JTextArea Resultado;
	public JButton Buscar, BusquedaG, Editar, Borrar, Regresar;
	public JScrollPane scroll;
	ButtonsDesing BD;
	
	public BuscarEditarBorrarEstudiante() {
		
		//Inicio de componetes principales
		BD = new ButtonsDesing();
		InicioPrinciopales();
	}
	
	public void InicioPrinciopales(){
		
		//Panel para buscar, editar y borrar
		BEB = new JPanel();
		BEB.setLayout(null);
		BEB.setBackground(new Color(245,245,245));
		BEB.setBounds(175,60,785, 500);
		
		Labels();
		Inputs();
		Buttons();
	}
	
	public void Labels() {
		
		//Etiquetas
		Titulo = new JLabel("BUSCAR UN ESTUDIANTE");
		Titulo.setFont(new Font("RussellSquare", 1, 35));
		Titulo.setBounds(195,5, 500, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		IngreseCI = new JLabel("Ingrese la cedula del estudiante");
		IngreseCI.setFont(new Font("RussellSquare", 1, 20));
		IngreseCI.setBounds(100,75, 500, 75);
		IngreseCI.setForeground(new Color(0,0,0));
		
		BEB.add(Titulo);
		BEB.add(IngreseCI);
	}
	
	public void Inputs() {
		
		CI = new JTextField();
		CI.setBounds(100,140, 200, 27);
		BEB.add(CI);
		
		Resultado = new JTextArea();
		Resultado.setBounds(100,240, 600, 210);
		Resultado.setEditable(false);
		Resultado.setText("");
		BEB.add(Resultado);
		
		//Agregando una barra de scroll como tanto vertical y horizontal
		scroll = new JScrollPane(Resultado);
		scroll.setBounds(100,240, 600, 210);
		BEB.add(scroll);
	}
	
	public void Buttons() {
		
		//Botones
		Buscar = new JButton();
		BusquedaG = new JButton();
		Editar = new JButton();
		Borrar = new JButton();
		Regresar = new JButton();
		
		//Posicion y tamaño
		Buscar.setBounds(325, 132, 100, 38);
		BusquedaG.setBounds(470, 132, 100, 38);
		Editar.setBounds(470, 190, 100, 38);
		Borrar.setBounds(600, 190, 100, 38);
		Regresar.setBounds(25,10, 45, 45);
		
        //Enviando datos al metodo
        BD.AsignacionImagen(Buscar, "src/img/icons/Buttons/Light Mode/BotonBuscar.png", "src/img/icons/Buttons/Light Mode/BotonBuscarHover.png", "src/img/icons/Buttons/Light Mode/BotonBuscarPress.png");
        BD.AsignacionImagen(BusquedaG, "src/img/icons/Buttons/Light Mode/BotonBuscarG.png", "src/img/icons/Buttons/Light Mode/BotonBuscarHoverG.png", "src/img/icons/Buttons/Light Mode/BotonBuscarPressG.png");
        BD.AsignacionImagen(Editar, "src/img/icons/Buttons/Light Mode/BotonEditar.png", "src/img/icons/Buttons/Light Mode/BotonEditarHover.png", "src/img/icons/Buttons/Light Mode/BotonEditarPress.png");
        BD.AsignacionImagen(Borrar, "src/img/icons/Buttons/Light Mode/BotonBorrar.png", "src/img/icons/Buttons/Light Mode/BotonBorrarHover.png", "src/img/icons/Buttons/Light Mode/BotonBorrarPress.png");
        BD.AsignacionImagen(Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");
		
		//Se ocultan los botones editar y borrar
		Editar.setVisible(false);
		Borrar.setVisible(false);
		
		//Añadiendo al panel
		BEB.add(Buscar);
		BEB.add(BusquedaG);
		BEB.add(Editar);
		BEB.add(Borrar);
		BEB.add(Regresar);
	}
}
