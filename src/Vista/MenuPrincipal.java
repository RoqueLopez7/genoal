package Vista;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;

import Controlador.Controlador;

public class MenuPrincipal extends JPanel implements ActionListener{

    //Componentes
	public JPanel BarraFlotante, MenuLateral, Bienvenida;
	public JLabel bienvenida, Unerg, imgUnerg, UNERGbienvenida;
	public JCheckBox MDark;
	public JButton Home, Student, Algoritmo1, Algoritmo2, Maraton, Opciones, Salir;
	public JButton BotonNuevo, BotonAnterior; //Boton para verificar el boton presionado

	//Imagenes
    private ImageIcon ImagenClaro,ImagenOpaco, ImagenBlanco;
    private Icon Icono;
    
	//Vistas externas
	Vista V;
	Bienvenida B;
	Estudiante Stu;
	
	//Clase Controlador
	Controlador controlador;

    public MenuPrincipal(){
    	//Inicio de los componentes
    	InicioComponentesPrincipales(); 
    }
    //---------->Barra flotante y menu lateral<----------
	private void InicioComponentesPrincipales() {
		
		//Se declaran los paneles
		BarraFlotante = new JPanel();
		MenuLateral = new JPanel();
		
		//Se establece un layout nulo para poder trabajar con setBounds
		BarraFlotante.setLayout(null);
		MenuLateral.setLayout(null);
		
		//Se le declara un color
		BarraFlotante.setBackground(new Color(0,115,217));
		MenuLateral.setBackground(new Color(0,80,180));
		
		//Se le declara una posicion
		BarraFlotante.setBounds(0,0,960,60); //X = 0, Y = 0, Anchura = 1190, Altura = 70
		MenuLateral.setBounds(0,60,175,960); //X = 0, Y = 60, Anchura = 200, Altura = 1130
		
		//Se vuelven visibles ambos paneles
		BarraFlotante.setVisible(true);
		MenuLateral.setVisible(true);
	
		//Se a�aden a la ventana
		add(BarraFlotante);
		add(MenuLateral);
		
		//Para llamar a sus opciones y demas
		ComponentesAdicionalesBarraFlotante();
		
		OpcionesMenuLateral();
	}

	public void ComponentesAdicionalesBarraFlotante() {
		
		//Boton de modo oscuro
		MDark = new JCheckBox();
		MDark.setBounds( 20, 5, 40, 45);
		ImagenClaro = new ImageIcon("src/img/icons/YellowMoon.png");
		ImagenOpaco = new ImageIcon("src/img/icons/WhiteMoon.png");
		this.PersonalizarCheckBox(MDark, ImagenClaro, ImagenOpaco); 
		
		BarraFlotante.add(MDark);
		
		//Etiqueta de GENOAL
		Unerg = new JLabel("GENOAL");
		Unerg.setBounds(850, 0, 250, 55);
		Unerg.setFont(new Font("RussellSquare", 1, 20));
		Unerg.setForeground(new Color(255,255,255));
		
		//Se añade al panel de Barra flotante
		BarraFlotante.add(Unerg);
	}
	
	public void OpcionesMenuLateral(){
		
		//Se calcula un tama�o para los botones
		int alturaboton = (960 - 60)/14;

		//Se crean los botones, se les asigna un icono
		Home = new JButton();
		Student = new JButton();
		Algoritmo1 = new JButton();
		Algoritmo2 = new JButton();
		Maraton = new JButton();
		Opciones = new JButton();
		Salir = new JButton();
		
		//Instancia de la clase controlador
		//controlador = new Controlador(V, this, B, Stu);  
		
		//Se añaden caracteristicas a los botones
		Home.setBounds(0,0,175,alturaboton);//Posicion del boton
        ImagenClaro = new ImageIcon("src/img/icons/Light Mode/ButtonHomeHover.png"); //Dandole una imagen al ImageIcon
		ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonHome.png"); //Dandole una imagen al ImageIcon
		ImagenBlanco = new ImageIcon("src/img/icons/Light Mode/ButtonHomeStanding.png");//Dandole una imagen al ImageIcon
		this.PersonalizarBoton(Home, ImagenClaro, ImagenOpaco, ImagenBlanco); //Llamando al metodo
				
		Student.setBounds(0, alturaboton,175,alturaboton);
        ImagenClaro = new ImageIcon("src/img/icons/Light Mode/ButtonStudentHover.png");
		ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonStudent.png");
		ImagenBlanco = new ImageIcon("src/img/icons/Light Mode/ButtonStudentStanding.png");
		this.PersonalizarBoton(Student, ImagenClaro, ImagenOpaco, ImagenBlanco);
		
		Algoritmo1.setBounds(0,(alturaboton*2),175,alturaboton);
        ImagenClaro = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo1Hover.png");
		ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo1.png");
		ImagenBlanco = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo1Standing.png");
		this.PersonalizarBoton(Algoritmo1, ImagenClaro, ImagenOpaco, ImagenBlanco);
		
		Algoritmo2.setBounds(0,(alturaboton*3),175,alturaboton);
        ImagenClaro = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo2Hover.png");
		ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo2.png");
		ImagenBlanco = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo2Standing.png");
		this.PersonalizarBoton(Algoritmo2, ImagenClaro, ImagenOpaco, ImagenBlanco);
		
		Maraton.setBounds(0, (alturaboton*4),175,alturaboton);
        ImagenClaro = new ImageIcon("src/img/icons/Light Mode/ButtonMaratonHover.png");
		ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonMaraton.png");
		ImagenBlanco = new ImageIcon("src/img/icons/Light Mode/ButtonMaratonStanding.png");
		this.PersonalizarBoton(Maraton, ImagenClaro, ImagenOpaco, ImagenBlanco);
		
		Opciones.setBounds(0,(alturaboton*5),175,alturaboton);
        ImagenClaro = new ImageIcon("src/img/icons/Light Mode/ButtonSettingsHover.png");
		ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonSettings.png");
		ImagenBlanco = new ImageIcon("src/img/icons/Light Mode/ButtonSettingsStanding.png");
		this.PersonalizarBoton(Opciones, ImagenClaro, ImagenOpaco, ImagenBlanco);
		
		Salir.setBounds(0,(alturaboton*6),175,alturaboton);
        ImagenClaro = new ImageIcon("src/img/icons/Light Mode/ButtonExitHover.png");
		ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonExit.png");
		ImagenBlanco = new ImageIcon("src/img/icons/Light Mode/ButtonExitHover.png");
		this.PersonalizarBoton(Salir, ImagenClaro, ImagenOpaco,ImagenBlanco);
		
		//Los botones se añaden al panel de MenuLateral
		MenuLateral.add(Home);
		MenuLateral.add(Student);
		MenuLateral.add(Algoritmo1);
		MenuLateral.add(Algoritmo2);
		MenuLateral.add(Opciones);
		MenuLateral.add(Salir);
		MenuLateral.add(Maraton);
	}

	//--------------->METODO PARA PERSONALIZAR BOTON<---------------
	public void PersonalizarCheckBox(JCheckBox Box, ImageIcon ImagenClaro, ImageIcon ImagenOpaco) {
		 //Dandole valor al icono   
		Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(35, 
				35, Image.SCALE_DEFAULT));
		Box.setIcon(Icono); //Estableciendo el icono al checkbox
		Box.setOpaque(false); //Apagando la opacidad
		Box.setContentAreaFilled(false); //Apagando Area contenida
		Box.setBorderPainted(false); //Apagando los bordes pintados
				
		MouseListener OyentedeRatonAtras = new MouseListener() {
				
			public void mouseClicked(MouseEvent e) {}

			public void mouseEntered(MouseEvent e) {
					
				//Cambiandolo de color al pasar el mouse		
				Icono = new ImageIcon(ImagenClaro.getImage().getScaledInstance(35, 
						35, Image.SCALE_DEFAULT));
					Box.setIcon(Icono);
					Box.setCursor(new Cursor(Cursor.HAND_CURSOR));
				}

				public void mouseExited(MouseEvent e) {
					
					//Volviendo a su color normal	
					Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(35, 
							35, Image.SCALE_DEFAULT));
					Box.setIcon(Icono);		
				}

				public void mousePressed(MouseEvent e) {}

				public void mouseReleased(MouseEvent e) {}
					
			};
				
			Box.addMouseListener(OyentedeRatonAtras);
		    	
	    }
	
	//@Override
	public void actionPerformed(ActionEvent e) {}
	
	//--------------->METODO PARA PERSONALIZAR BOTON<---------------
	public void PersonalizarBoton(JButton Boton, ImageIcon ImagenClaro, ImageIcon ImagenOpaco, ImageIcon ImagenBlanco) {		
		
		//Dandole valor al icono
		Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
				Boton.getHeight(), Image.SCALE_DEFAULT));
		Boton.setIcon(Icono);
		Boton.setOpaque(false);
		Boton.setContentAreaFilled(false);
		Boton.setBorderPainted(false);		
		
		//Cadena que nos ayudara para el cambio de los botones
		String Imagen = "src/img/icons/Light Mode/";
		
		//Condicion para colocar a Home como boton principal
		if(BotonAnterior == null) {
			BotonAnterior = Home;
		    Icono = new ImageIcon(ImagenBlanco.getImage().getScaledInstance(Home.getWidth(), 
		        Home.getHeight(), Image.SCALE_DEFAULT));
		   Home.setIcon(Icono);
	    }
		
		MouseListener OyentedeRatonAtras = new MouseListener() {
				
			public void mouseClicked(MouseEvent e) {	

				//Metodo para cambiar a opaco a todos los botones
				   CambioFondo(ImagenOpaco);
				
				   //Declarando el fondo blanco al boton presionado
				   Icono = new ImageIcon(ImagenBlanco.getImage().getScaledInstance(BotonNuevo.getWidth(), 
				        BotonNuevo.getHeight(), Image.SCALE_DEFAULT));
				   BotonNuevo.setIcon(Icono); //Estableciendo el icono al boton
				   Boton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); //Estableciendo el cursor por defecto al boton presionado
				        
				   //Atributo necesario para el funcionamiento del cambio
				   BotonAnterior = Boton;     
			}

			public void mouseEntered(MouseEvent e) {
				//Si el boton anterior dejo de ser home, es que hay otro boton nuevo	
				if(BotonAnterior != null){BotonNuevo = Boton;}
				
				//Estableciendo el cursor por defecto
				if(BotonAnterior == null & Boton == Home){Boton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));}
				
				//Si los demas botones no estan siendo pasados, se quedan en su estado claro
				if(BotonAnterior != Boton) {
					Icono = new ImageIcon(ImagenClaro.getImage().getScaledInstance(Boton.getWidth(), 
							Boton.getHeight(), Image.SCALE_DEFAULT));
						Boton.setIcon(Icono); //
						Boton.setCursor(new Cursor(Cursor.HAND_CURSOR));
				}
			}

				public void mouseExited(MouseEvent e) {
                    
                    //Condicion para que los botones vuelvan a su color natural
					if(BotonAnterior != Boton) {
						Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(Boton.getWidth(), 
								Boton.getHeight(), Image.SCALE_DEFAULT));
						Boton.setIcon(Icono);	
					}
				}

				public void mousePressed(MouseEvent e) {}

				public void mouseReleased(MouseEvent e) {}
					
			};
				
			Boton.addMouseListener(OyentedeRatonAtras);
		    	
	    }
	//Metodo para cambiar de color al boton anteriormente tocado    
	void CambioFondo(ImageIcon ImagenOpaco){
		if(BotonAnterior == Home) {//verificando cual de los botones fue el anterior
			if(ImagenOpaco.getImage().equals("src/img/icons/Light Mode/ButtonHome.png")) {cambio(Home, ImagenOpaco);} else {ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonHome.png"); cambio(Home,ImagenOpaco);} 
		}
		if(BotonAnterior == Student) {
			if(ImagenOpaco.getImage().equals("src/img/icons/Light Mode/ButtonStudent.png")) {cambio(Student, ImagenOpaco);} else {ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonStudent.png"); cambio(Student,ImagenOpaco);} 
        }
		if(BotonAnterior == Algoritmo1) {			
			if(ImagenOpaco.getImage().equals("src/img/icons/Light Mode/ButtonAlgoritmo1.png")) {cambio(Algoritmo1, ImagenOpaco);} else {ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo1.png"); cambio(Algoritmo1,ImagenOpaco);} 
        }
		if(BotonAnterior == Algoritmo2) {
			if(ImagenOpaco.getImage().equals("src/img/icons/Light Mode/ButtonAlgoritmo2.png")) {cambio(Algoritmo2, ImagenOpaco);} else {ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonAlgoritmo2.png"); cambio(Algoritmo2,ImagenOpaco);} 
        }
		if(BotonAnterior == Maraton) {
			if(ImagenOpaco.getImage().equals("src/img/icons/Light Mode/ButtonMaraton.png")) {cambio(Maraton, ImagenOpaco);} else {ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonMaraton.png"); cambio(Maraton,ImagenOpaco);} 
        }
		if(BotonAnterior == Opciones) {
			if(ImagenOpaco.getImage().equals("src/img/icons/Light Mode/ButtonSettings.png")) {cambio(Opciones, ImagenOpaco);} else {ImagenOpaco = new ImageIcon("src/img/icons/Light Mode/ButtonSettings.png"); cambio(Opciones,ImagenOpaco);} 
        }
	}
	
	//Cambiando de color al boton anterior
	void cambio(JButton BotonCambiable, ImageIcon ImagenOpaco) {
		   Icono = new ImageIcon(ImagenOpaco.getImage().getScaledInstance(BotonCambiable.getWidth(), 
				   BotonCambiable.getHeight(), Image.SCALE_DEFAULT));
		   BotonCambiable.setIcon(Icono);	
	}
}