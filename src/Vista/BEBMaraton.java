package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class BEBMaraton extends JPanel{

	//Componentes
	public JPanel BuscarMa;
	public JLabel Titulo, Asignatura, Seccion, Unidad, Resultados;
	public JComboBox Asig, Uni, Sec, Day, Month, Year;
	public JButton Registrar, Buscar, Editar, RegistrarGanadores, Suspender, Regresar;
	public JTextArea ResultadosBusqueda;
	public JScrollPane scroll;
	
	//Atributos
	public String AsignaturaBM[] = {"Asignatura", "Algoritmo 1", "Algoritmo 2"};
	public String secciones[] = {"Secciones", "1","2", "3","4", "5","6", "7","8"};
	public String unidades[] = 	{"Unidades", "1", "2", "3", "4", "5"};

	ButtonsDesing BD;
	public BEBMaraton() {
	
		//Componentes principales para la ventana
		BD = new ButtonsDesing();
		ComponentesPrincipales();
	}

	public void ComponentesPrincipales(){

		BuscarMa = new JPanel();
		BuscarMa.setLayout(null);
		BuscarMa.setBackground(new Color(245,245,245));
		BuscarMa.setBounds(175,60,785, 500);
		
		//Componentes
		Labels();
		Lists();
		Buttons();
		TextArea();
	}

	public void Labels(){
		
		//Titulo
		Titulo = new JLabel();
		Titulo.setFont(new Font("RussellSquare", 1, 32));
		Titulo.setBounds(220,5, 450, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		//Asignatura
		Asignatura = new JLabel("Asignatura");
		Asignatura.setFont(new Font("RussellSquare", 1, 18));
		Asignatura.setBounds(110,100, 300, 75);
		Asignatura.setForeground(new Color(0,0,0));
		
		//Seccion
		Seccion = new JLabel("Seccion");
		Seccion.setFont(new Font("RussellSquare", 1, 18));
		Seccion.setBounds(110,200, 300, 75);
		Seccion.setForeground(new Color(0,0,0));
		
		//Unidad
		Unidad = new JLabel("Unidad");
		Unidad.setFont(new Font("RussellSquare", 1, 18));
		Unidad.setBounds(110,300, 300, 75);
		Unidad.setForeground(new Color(0,0,0));
				
		//Resultados
		Resultados = new JLabel("Resultados");
		Resultados.setFont(new Font("RussellSquare", 1, 22));
		Resultados.setBounds(525,80, 300, 75);
		Resultados.setForeground(new Color(0,0,0));
		
		//Añadiendo al panel
		BuscarMa.add(Titulo);
		BuscarMa.add(Asignatura);
		BuscarMa.add(Seccion);
		BuscarMa.add(Unidad);
		BuscarMa.add(Resultados);

	}

	public void Lists() {
		
		//Asignatura
		Asig = new JComboBox(AsignaturaBM);
		Asig.setBounds(110,150, 125, 25);
		
		//Seccion
		Sec = new JComboBox(secciones);
		Sec.setBounds(110,250, 125, 25);
		
		//Unidad
		Uni = new JComboBox(unidades);
		Uni.setBounds(110,350, 125, 25);
			
		//Añadiendo al panel
		BuscarMa.add(Asig);
		BuscarMa.add(Sec);
		BuscarMa.add(Uni);
	}
	
	public void Buttons() {
		
		//Botones
		Buscar = new JButton();
		Buscar.setBounds(280, 140, 100,40);
		
		RegistrarGanadores = new JButton();
		RegistrarGanadores.setBounds(525, 375, 150,40);
		RegistrarGanadores.setVisible(false);
		
		Editar = new JButton();
		Editar.setBounds(525, 375, 100,40);
		Editar.setVisible(false);
		
		Suspender = new JButton();
		Suspender.setBounds(525, 375, 100,40);
		Suspender.setVisible(false);
		
		Regresar = new JButton();
		Regresar.setBounds(25,10, 45, 45);
		
		//Asignacion de los botones
        BD.AsignacionImagen(RegistrarGanadores, "src/img/icons/Buttons/Light Mode/ButtonWinnersRegister.png", "src/img/icons/Buttons/Light Mode/ButtonWinnersRegisterHover.png", "src/img/icons/Buttons/Light Mode/ButtonWinnersRegisterHold.png");
        BD.AsignacionImagen(Buscar, "src/img/icons/Buttons/Light Mode/BotonBuscar.png", "src/img/icons/Buttons/Light Mode/BotonBuscarHover.png", "src/img/icons/Buttons/Light Mode/BotonBuscarPress.png");
        BD.AsignacionImagen(Editar, "src/img/icons/Buttons/Light Mode/BotonEditar.png", "src/img/icons/Buttons/Light Mode/BotonEditarHover.png", "src/img/icons/Buttons/Light Mode/BotonEditarPress.png");
        BD.AsignacionImagen(Suspender, "src/img/icons/Buttons/Light Mode/BotonBorrar.png", "src/img/icons/Buttons/Light Mode/BotonBorrarHover.png", "src/img/icons/Buttons/Light Mode/BotonBorrarPress.png");
        BD.AsignacionImagen(Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");

		BuscarMa.add(Buscar);
		BuscarMa.add(RegistrarGanadores);
		BuscarMa.add(Editar);
		BuscarMa.add(Suspender);
		BuscarMa.add(Regresar);
	}	

	public void TextArea() {
		
		//Resultados
		ResultadosBusqueda = new JTextArea();
		ResultadosBusqueda.setBounds(440, 140, 300, 200);
		ResultadosBusqueda.setEditable(false);
		BuscarMa.add(ResultadosBusqueda);
		
		//Agregando una barra de scroll como tanto vertical y horizontal
		scroll = new JScrollPane(ResultadosBusqueda);
		scroll.setBounds(440, 140, 300, 200);
		BuscarMa.add(scroll);
	}
}
	
