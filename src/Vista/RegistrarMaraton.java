package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RegistrarMaraton extends JPanel{

	//Componentes
	public JPanel RegistrarMa;
	public JLabel Titulo, Asignatura, Seccion, Unidad, Fecha;
	public JComboBox Asig, Uni, Sec, Day, Month, Year;
	public JButton Registrar, Regresar;
	
	//Atributos
	public String AsignaturaRM[] = {"Asignatura", "Algoritmo 1", "Algoritmo 2"};
	public String secciones[] = {"Secciones", "1","2", "3","4", "5", "6", "7", "8"};
	public String unidades[] = 	{"Unidades", "1", "2", "3", "4", "5"};
	public String year[] = {""}, month[] = {""}, day[] = {""};
	
	ButtonsDesing BD;
	public RegistrarMaraton() {
	
		//Componentes principales para la ventana
		BD = new ButtonsDesing();
		ComponentesPrincipales();
	}

	public void ComponentesPrincipales(){

		RegistrarMa = new JPanel();
		RegistrarMa.setLayout(null);
		RegistrarMa.setBackground(new Color(245,245,245));
		RegistrarMa.setBounds(175,60,785, 500);
		
		//Componentes
		Labels();
		Lists();
		Buttons();
	}

	public void Labels(){
		
		//Titulo
		Titulo = new JLabel("REGISTRAR MARATON");
		Titulo.setFont(new Font("RussellSquare", 1, 32));
		Titulo.setBounds(220,5, 450, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		//Asignatura
		Asignatura = new JLabel("Asignatura");
		Asignatura.setFont(new Font("RussellSquare", 1, 18));
		Asignatura.setBounds(110,100, 300, 75);
		Asignatura.setForeground(new Color(0,0,0));
		
		//Seccion
		Seccion = new JLabel("Seccion");
		Seccion.setFont(new Font("RussellSquare", 1, 18));
		Seccion.setBounds(330,100, 300, 75);
		Seccion.setForeground(new Color(0,0,0));
		
		//Unidad
		Unidad = new JLabel("Unidad");
		Unidad.setFont(new Font("RussellSquare", 1, 18));
		Unidad.setBounds(550,100, 300, 75);
		Unidad.setForeground(new Color(0,0,0));
				
		//Fecha
		Fecha = new JLabel("Fecha del comienzo");
		Fecha.setFont(new Font("RussellSquare", 1, 18));
		Fecha.setBounds(295,200, 300, 75);
		Fecha.setForeground(new Color(0,0,0));
		
		//Añadiendo al panel
		RegistrarMa.add(Titulo);
		RegistrarMa.add(Asignatura);
		RegistrarMa.add(Seccion);
		RegistrarMa.add(Unidad);
		RegistrarMa.add(Fecha);

	}

	public void Lists() {
		
		//Asignatura
		Asig = new JComboBox(AsignaturaRM);
		Asig.setBounds(110,150, 125, 25);
		
		//Seccion
		Sec = new JComboBox(secciones);
		Sec.setBounds(330,150, 125, 25);
		
		//Unidad
		Uni = new JComboBox(unidades);
		Uni.setBounds(550,150, 125, 25);
		
		//Fecha
		
			//Dia
			Day = new JComboBox(day);
			Day.setBounds(240,260, 70, 25);
			
			//Mes
			Month = new JComboBox(month);
			Month.setBounds(340,260, 70, 25);
			
			//Año
			Year = new JComboBox(year);
			Year.setBounds(440,260, 70, 25);
			
			//Asignar valores
			fecha();
			
		//Añadiendo al panel
		RegistrarMa.add(Asig);
		RegistrarMa.add(Sec);
		RegistrarMa.add(Uni);
		RegistrarMa.add(Day);
		RegistrarMa.add(Month);
		RegistrarMa.add(Year);
	}
	
	public void Buttons() {
		
		//Botones
		Registrar = new JButton();
		Registrar.setBounds(610, 380, 120,45);
		
		Regresar = new JButton();
		Regresar.setBounds(25,10, 45, 45);
		
		//Asignacion de los diseños
        BD.AsignacionImagen(Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");
		BD.AsignacionImagen(Registrar, "src/img/icons/Buttons/Light Mode/BotonRegistrar.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarPress.png");
		
		RegistrarMa.add(Registrar);
		RegistrarMa.add(Regresar);
	}
	
	public void fecha() {
		
		int annos = 2021, mes = 1, dia = 1;
		//Año
		if(Year.getSelectedItem().equals("")) {
			Year.removeItem("");
			Year.addItem("Año");
			do {
				String AnnoString = ""+annos+"";
				Year.addItem(AnnoString);
				annos = annos + 1;
			}while(annos <= 2050);
		}
		
		//Mes
		if(Month.getSelectedItem().equals("")){
			Month.removeItem("");
			Month.addItem("Mes");
			do {
				String MesString = ""+mes+"";
				Month.addItem(MesString);
				mes = mes + 1;
			}while(mes <= 12);
		}
		
		//Dia
		if(Day.getSelectedItem().equals("")){
			Day.removeItem("");
			Day.addItem("Día");
			do {
				String DiaString = ""+dia+"";
				Day.addItem(DiaString);
				dia = dia + 1;
			}while(dia <= 28);
		}
	}
}
