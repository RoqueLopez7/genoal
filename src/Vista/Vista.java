package Vista;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.JTextComponent;

import com.toedter.calendar.JDateChooser;

//import com.itextpdf.text.pdf.qrcode.Mode;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Date;
import java.sql.SQLException;
//import java.awt.event.KeyEvent;
//import java.awt.event.KeyListener;
import java.awt.Font;
import java.awt.Image;

import javax.accessibility.AccessibleContext;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import Controlador.Controlador;
import Modulo.Conexion;
import Modulo.ConexionMaraton;
import Modulo.ConexionNotas;
import Modulo.Modulo;

public class Vista extends JFrame implements ActionListener{

	//Componentes
	public JPanel Login, MenuPrincipal;
	public JLabel Bienvenida, Password, background, Mensaje;
	public JTextField clave;
	public JButton enviar;

	//Imagenes
	ImageIcon Icono, BgLogin, Frase, Welcome = new ImageIcon("src/img/background/Welcome.png");;
	
	//Controlador
	Controlador controlador;
	
	//Vistas externas
	MenuPrincipal MP;
	Bienvenida B;
	Estudiante Stu;
	RegistrarEstudiante RE;
	BuscarEditarBorrarEstudiante BEB;
	NotasPrincipal NP;
	IngresarNota IN;
	MaratonesPrincipal MaratonP;
	RegistrarMaraton RM;
	BEBMaraton BEBM;
	RegistrarGanadores RG;
	Opciones Op;
	CambiarContrasena CC;
	ButtonsDesing BD;
	Cargando C;
	Exportar Ex;
	Importar Im;
	
	//Conexión con la base de datos y modulo
	Conexion Co;
	ConexionNotas CoNotas;
	ConexionMaraton CoMaraton;
	Modulo Mo;
	JFileChooser Ch;
	
	//Atributos
	String mensaje,persona, SelectedSeccion = "", SelectedEstudiante = "";
	String [] Botones = {"Aceptar", "Cancelar"};
	int PosicionX, AlturaFrase, R = -1;//Variable para controlar los cambios en registrar persona
	int Contador = 0; //esto va al controlador
	boolean CambiarPassword, CambiarCorreo, Editar, EditarNota, algoritmo1 = false, algoritmo2 = false; //Editar esta por si le quieren editar los datos de un estudiante o su nota
    boolean FinalEncontrado = false;
	Boolean EditandoMaraton = false, SuspendiendoMaraton = false, Proceso; 
	private String Clave;
	long FechaLarga; //Valor de la fecha más larga
	java.sql.Date Fecha; //Valor de la fecha adaptado para una base de datos en SQL
	public int Incrementador = 0;
	
	public Vista(){

		//Componentes principales para la ventana
		BD = new ButtonsDesing();
		Co = new Conexion();
		CoNotas = new ConexionNotas();
		CoMaraton = new ConexionMaraton();
		Mo = new Modulo();
		
		if(Incrementador == 0) {  Incrementador = Incrementador + 10;}
		
		ComponentesPrincipales();
		
		//Para que el proceso termine al darle X 
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//Sumando el incrementador para la barra de carga
     	if(Incrementador == 75) {  
     		Incrementador = Incrementador + 25;
     		Login.setVisible(true);
     	}
	}

	//Caracteristicas para la ventana
	public void ComponentesPrincipales(){
		
		//Ventana principal
		this.setLayout(null); //Se establece el layout nulo
		this.setVisible(false); //Se vuelve visible
		this.setBounds(0,0, 960, 560); //Posicion y tamaño
		this.setTitle("GENOAL"); //Se le añade un titulo al programa
		this.setLocationRelativeTo(null); //Se coloca la ventana en el medio 
		this.setResizable(false); //Se le pone una restriccion para que no cambie el tamaño de la pantalla
		this.setUndecorated(true);

		//Se llama el metodo login
		Login();
	}
	
	//------------------>Login<------------------
	public void Login(){

		//Panel del login
		Login = new JPanel();
		Login.setLayout(null);
		Login.setBounds(0,0, 960, 560);
		
		add(Login);

		//Componentes necesarios
		Labels();//Etiquetas
		Inputs();//Campos de textos
		Button();//Boton
		
		//Fondo de pantalla del login
		background = new JLabel(Background()); //Declaracion del login
		background.setBounds(0,0,960,560); //Posicion y tamaño
		Login.add(background); //Se añade al panel login
	
		
	}	
		//------------------>Etiquetas del Login<------------------
		public void Labels(){
	
				//Bienvenida
				Bienvenida = new JLabel(Welcome);
				Bienvenida.setBounds(210,25,550,100); //Posicion y tamaño
				Login.add(Bienvenida);//Se añade al panel login
	
				//Ingrese la contraseña
				Password = new JLabel("Ingrese la contraseña");
				Password.setBounds(320,140,320,100); //Posicion y tamaño
				Password.setFont(new Font("Arial", 1, 30)); //Sele asigna una fuente y tamaño
				Password.setForeground(new Color(255,255,255)); //Se le añade un color al texto
				Login.add(Password); //Se añade al panel login
				
				//Sumando el incrementador para la barra de carga
     	        if(Incrementador == 10) {  Incrementador = Incrementador + 15; }
     	        
				//Mensaje Diario
				Mensaje = new JLabel(MensajeDiario());
				Mensaje.setBounds(PosicionX,375,550,AlturaFrase); //Posicion y tamaño
				Mensaje.setFont(new Font("Arial", 3, 17)); //Sele asigna una fuente y tamaño
				Mensaje.setForeground(new Color(255,255,255)); //Se le añade un color al texto
				Login.add(Mensaje); //Se añade al panel login
			}
		//------------------>Inputs del Login<------------------
		public void Inputs(){
	
				//Declarar el campo de texto
				clave = new JTextField();
				clave.setBounds(320,245, 310,25); //Posicion y tamaño
				clave.setHorizontalAlignment(0);
				Login.add(clave); //Se añade al panel login
				
				//clave.addKeyListener(controlador); //Se le coloca un oyente
			}
		//------------------>Boton del Login<------------------
		public void Button(){
	
				//Declarar el boton
				enviar = new JButton();
				enviar.setBounds(410,300,130,40); //Posicion y tamaño
				BD.AsignacionImagen(enviar, "src/img/icons/Buttons/Light Mode/ButtonSend.png", "src/img/icons/Buttons/Light Mode/ButtonSendHover.png", "src/img/icons/Buttons/Light Mode/ButtonSendHold.png");
				Login.add(enviar); //Se aña de al panel login

				//Instancia del Controlador
				//controlador = new Controlador(this, MP, B, Stu);
				enviar.addActionListener(this);
			}
		
		//------------------>Fondo del login<------------------
		public ImageIcon Background() {
				
				//Atributos necesarios
				int NAbg; 
				BgLogin = null;
				
				//Ciclo para verificar que el numero siempre sera mayor a 1
				do{NAbg = (int)(Math.random()*10);}while(NAbg == 0 || NAbg > 6); //Numero aleatorio
				
				//Asignación de una imagen de fondo
				for(int i = 1; i <= 6; i++) {
				    ProcedimientoEnCasoBackground(i, NAbg);					
				}
		     	
				return BgLogin;//Retorna la imagen
			
		}
		
		//------------------>Mensaje diario<------------------
		public ImageIcon MensajeDiario() {
				
				//Atributos necesarios
				int NA; 
				Frase = null;
				
				//Ciclo para verificar que el numero siempre sera mayor a 1
				do{NA = (int)(Math.random()*130);}while(NA == 0 || NA > 112); //Numero aleatorio
				
				//Asignación de la frase
				for(int i = 1; i <= 112; i++) {
				    ProcedimientoEnCaso(i, NA);					
				}
				
				//Imagenes que necesitan ser un poco mas altos
				if(NA == 14 || NA == 46 || NA == 55 || NA == 63 || NA == 72 || NA == 77 || NA == 79 
						|| NA == 81 || NA == 84 || NA == 85 || NA == 88) 
				{ AlturaFrase = 130;} else { AlturaFrase = 110;}
				
		     	//Sumando el incrementador para la barra de carga
		     	if(Incrementador <= 50) {  Incrementador = Incrementador + 25;}
		     	
				//Retorna la imagen
				return Frase;
			}
		
	public void ProcedimientoEnCasoBackground(int i, int NAbg) {
		
		//Asignacion de imagen de fondo
		if(NAbg == 1) {
			BgLogin = new ImageIcon("src/img/background/background-login.png");
		} else {
			BgLogin = new ImageIcon("src/img/background/background-login"+i+".png");
		}
	}
		
	public void ProcedimientoEnCaso(int i, int NA){
		
		//Si numero aleatorio es igual a iteracion
		if(NA == i) {
			Frase = new ImageIcon("src/img/background/frases/frase"+NA+".png");
			
			//Posicionamiento de la imagen
		    if(NA == 2) {
			    PosicionX = 210;
		    }else if (NA == 12 || NA == 23 || NA == 34 || NA == 44 || NA == 54 || NA == 64 || NA == 74 
						|| NA == 84 || NA == 94 || NA == 104) 
		    {
		    	PosicionX = 209;
		    } else if(NA == 48 || NA == 58 || NA == 68 || NA == 78 || NA == 88 || NA == 98 || NA == 108) {
		    	PosicionX = 208;
		    } else if(NA == 53 || NA == 57 || NA == 63 || NA == 67 || NA == 70 || NA == 73 || NA == 77 || NA == 83 || NA == 87 ||
				NA == 89 || NA == 93 || NA == 97 || NA == 99 || NA == 103 || NA == 107 || NA == 109) {
		    	PosicionX = 206;
		    } else if ( NA == 112) {
		    	PosicionX = 207;
		    } else {
		    	PosicionX = 205;
		    }
		}
		
     	//Sumando el incrementador para la barra de carga
     	if(Incrementador == 25) {  Incrementador = Incrementador + 25; }
	}
		
	//------------------>Agregar los demas paneles<------------------
	
	public void IniciarVistas() {
		//Se inician
		Stu = new Estudiante();
		B = new Bienvenida();
		RE = new RegistrarEstudiante();
		BEB = new BuscarEditarBorrarEstudiante();
		NP = new NotasPrincipal();
		IN = new IngresarNota();
		MaratonP = new MaratonesPrincipal();
		RM = new RegistrarMaraton();
		BEBM = new BEBMaraton();
		RG = new RegistrarGanadores();
		Op = new Opciones();
		CC = new CambiarContrasena();
		MP = new MenuPrincipal();
		Ex = new Exportar();
        Im = new Importar();
        
		//Añadiendo paneles a la ventana
		add(B.Bienvenida);
		add(Stu.Estudiante);
		add(RE.RegistroEstudiante);
		add(RE.PanelContacto);
		add(BEB.BEB);
		add(NP.NotasPrin);
		add(IN.InNota);
		add(MaratonP.MaratonPrincipal);
		add(RM.RegistrarMa); 
		add(BEBM.BuscarMa);
		add(RG.RegistrarGanadores);
		add(Op.Opciones);
		add(CC.CambiarContrasena);
		add(MP.BarraFlotante);
		add(MP.MenuLateral);
		add(Ex.Contenedor);
		add(Ex.Barra);
		add(Im.Barra);
		add(Im.Contenedor);
		
		//Deshabilitando visibilidad a los paneles
		Ex.Contenedor.setVisible(false);
		Ex.Barra.setVisible(false);
		Im.Contenedor.setVisible(false);
		Im.Barra.setVisible(false);
		
		//Activando el principal
		CambioPaneles(B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, NP.NotasPrin, BEB.BEB,
		RE.PanelContacto, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones,CC.CambiarContrasena);
		
		//MP.Home.addActionListener(this);
	}
	
	public void AgregarPaneles() {
		
		IniciarVistas();
		//Login.setVisible(false);
		MP.BarraFlotante.setVisible(true);
		MP.MenuLateral.setVisible(true);
		Contador = 0;
		
		Oyentes();
		//remove(Login);
	}
	
	public void Oyentes() {
		
		//Añadiendo los oyentes en vectores
		JButton BotonesPrincipales[] = {MP.Home, MP.Student, MP.Algoritmo1, MP.Algoritmo2, MP.Maraton, MP.Opciones, MP.Salir};
		JButton BotonsEstudiantes[] = {Stu.RegistrarEstudiante, Stu.BuscarEstudiante, Stu.EditarEstudiante, Stu.BorrarEstudiante,
				RE.Siguiente,RE.Atras,RE.Guardar, RE.Regresar, BEB.Buscar, BEB.BusquedaG, BEB.Editar, BEB.Borrar, BEB.Regresar};
		JButton BotonesNotas[] = {NP.RegistrarNota, NP.BuscarNota, NP.EditarNota, NP.BorrarNota, IN.Guardar, IN.Editar, IN.Borrar, IN.Regresar, IN.BuscarNota};
	    JButton BotonesMaraton[] = { MaratonP.RegistrarMaraton, MaratonP.BuscarMaraton, MaratonP.EditarMaraton, MaratonP.SuspenderMaraton,
				RM.Registrar, RM.Regresar, BEBM.RegistrarGanadores, BEBM.Buscar, BEBM.Editar, BEBM.Suspender, BEBM.Regresar,
				RG.Registrar, RG.Regresar};
		JButton BotonesOpciones[] = { Op.CambiarCorreo,	Op.CambiarPass, Op.Regresar, Op.Guardar, CC.Regresar, CC.Enviar,
     			CC.Guardar, Op.Enviar, Op.Exportar, Op.Importar, Ex.Cancelar, Ex.ExportarArchivo, Ex.Seleccionar,
     			Im.Cancelar, Im.ImportarArchivo, Im.Seleccionar};
		
		//Lista
		JComboBox ListasIngresarNota[] = {IN.Sec, IN.Uni, IN.Estu};
		JComboBox ListaRegistrarMaraton[] = {RG.Primer, RG.Segundo, RG.Tercer, RG.UnidadPremio1, RG.UnidadPremio2, RG.UnidadPremio3};
		
		//Menu Principal
		for(int i = 0; i <= 6; i++) { BotonesPrincipales[i].addActionListener(this);}

		//Estudiante
		for(int i = 0; i <= 12; i++) { BotonsEstudiantes[i].addActionListener(this);}

		//Notas
		for(int i = 0; i <= 8; i++) { BotonesNotas[i].addActionListener(this);}

		//Maraton - Registrar Maraton - Buscar Maraton - Registrar Ganadores
     	for(int i = 0; i <= 12; i++) { BotonesMaraton[i].addActionListener(this);}
		
		//Opciones
     	for(int i = 0; i <= 15; i++) { BotonesOpciones[i].addActionListener(this);}

		//Oyentes especiales	
     	for(int i = 0; i <= 5; i++) { 
     		
     		if(i <= 2) {
     			ListasIngresarNota[i].addActionListener(this); 				
     		}
     		
     		ListaRegistrarMaraton[i].addActionListener(this);
     	}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		//Clave de seguridad
		Clave = Co.ObtenerPass();
		
		//Si presiona enviar la contraseña para entrar
				if(e.getSource().equals(enviar)) {
				/*-----------------------------------------------------------
				*--------------------------->Login<--------------------------
				*-----------------------------------------------------------*/
					//Si la contraseña es la clave del desarrollador
					if(clave.getText().toString().equals("GENOAL2021") || clave.getText().toString().equals(Clave)) {
						JOptionPane.showMessageDialog(null, "Contraseña correcta");
						AgregarPaneles();
						Login.setVisible(false);
						MP.BarraFlotante.setVisible(true);
						MP.MenuLateral.setVisible(true);
						Contador = 0;
						remove(Login);
					} 
					else if(clave.getText().toString().equals("")){ //Si el campo esta vacio
						
						JOptionPane.showMessageDialog(null, "El campo está vacío, ingrese la contraseña.");
						
						}else {//Si se equivoca
							
						Contador = Contador + 1; //Contador para seguridad del programa
						
						if(Contador != 3) {//Mensaje de contraseña incorrecta
							JOptionPane.showMessageDialog(null, "Contraseña incorrecta.\nVuelva a intentar.");
						}
					}
				}
				
				//Seguridad y cierre del programa
				if(Contador == 3 && (!CambiarPassword || !CambiarCorreo)) { 
					JOptionPane.showMessageDialog(null, "Demasiados intentos, vuelva pronto.");
					System.exit(0);
				}
				
			/*-----------------------------------------------------------
			*---------------------->Menu Principal<----------------------
			*-----------------------------------------------------------*/
				if(e.getSource().equals(MP.Home)) {
					//Cambio de paneles
					CambioPaneles(B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, NP.NotasPrin, BEB.BEB,
					RE.PanelContacto, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
				}
				
				/*-----------------------------------------------------------
				*---------------------->Estudiante<--------------------------
				*-----------------------------------------------------------*/
				
				if(e.getSource().equals(MP.Student) || e.getSource().equals(RE.Regresar) || e.getSource().equals(BEB.Regresar)) {
					
					//Cambio de paneles
					CambioPaneles( Stu.Estudiante, B.Bienvenida, RE.RegistroEstudiante, NP.NotasPrin, BEB.BEB,
					RE.PanelContacto, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
		
					BEB.Titulo.setText("BUSCAR UN ESTUDIANTE");
					
					//Poniendo visible el boton de busqueda general
					if(BEB.Titulo.getText().equals("BUSCAR UN ESTUDIANTE")) { BEB.BusquedaG.setVisible(true);}
					
					//Limpia los campos y listas
					Limpiar();
				}
				
					//----------------->Registrar un Estudiante<-----------------
					if(e.getSource().equals(Stu.RegistrarEstudiante)) {RE.Titulo.setText("REGISTRAR UN ESTUDIANTE");}
					
					if(e.getSource().equals(Stu.RegistrarEstudiante) || e.getSource().equals(RE.Atras)) {
						//Cambio de paneles
						CambioPaneles(RE.RegistroEstudiante, B.Bienvenida, Stu.Estudiante, NP.NotasPrin, BEB.BEB,
						RE.PanelContacto, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);

						RE.RegistroEstudiante.add(RE.Titulo);
						RE.RegistroEstudiante.add(RE.SubTitulo);
						RE.RegistroEstudiante.add(RE.Regresar);
						
						//Editar = false;
					}
					
						//------------->Siguiente en el panel de Registrar un Estudiante<--------------
						if(e.getSource().equals(RE.Siguiente)) {

							//Cambio de paneles
							CambioPaneles(RE.PanelContacto, B.Bienvenida, Stu.Estudiante, NP.NotasPrin, BEB.BEB,
							RE.RegistroEstudiante, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
							
							RE.PanelContacto.add(RE.Titulo);
							RE.PanelContacto.add(RE.SubTitulo);
							RE.PanelContacto.add(RE.Regresar);
						}
						
						//----------------->Guardar datos del estudiante<-----------------
						if(e.getSource().equals(RE.Guardar)) {
							Mo.setNombres(RE.Nom.getText());
							Mo.setApellidos(RE.Ape.getText());
							Mo.setCedula(RE.CI.getText());
							Mo.setTelefono(RE.TLF.getText());
							Mo.setCorreo(RE.correo.getText());
							Mo.setAsignatura(RE.Asig.getSelectedIndex());							
							Mo.setFecha_Ingreso(Fecha(RE.date, RE.Calendario));
							Mo.setSeccion(RE.Sec.getSelectedIndex());
							Mo.setFacebook(RE.facebook.getText());
							Mo.setTwitter(RE.twitter.getText());
							Mo.setInstagram(RE.instagram.getText());
						    Mo.setTelegram(RE.telegram.getText());
						    Mo.setLoL(RE.lol.getText());
						    Mo.setWoW(RE.wow.getText());
						    
						    if(!Editar) {
						    	Co.Insercion(Mo);
						    }else {
						    	Co.Actualizacion(Mo);
						    }
						    
						    //Limpia campos y listas
						    Limpiar();
							
							JOptionPane.showMessageDialog(null, "La fecha ingresada es: "+ Mo.FechaInicial());
							
						  //Cambio de paneles
							CambioPaneles(RE.RegistroEstudiante, B.Bienvenida, Stu.Estudiante, NP.NotasPrin, BEB.BEB,
							RE.PanelContacto, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);

							RE.RegistroEstudiante.add(RE.Titulo);
							RE.RegistroEstudiante.add(RE.SubTitulo);
							RE.RegistroEstudiante.add(RE.Regresar);
						}
						
					//------------->Panel de Buscar Estudiante<-------------------------	
					if(e.getSource().equals(Stu.BuscarEstudiante)) {BEB.Titulo.setText("BUSCAR UN ESTUDIANTE");}
					
					if(e.getSource().equals(Stu.BuscarEstudiante) || e.getSource().equals(Stu.BorrarEstudiante) || e.getSource().equals(Stu.EditarEstudiante)){
						//Cambio de paneles
						CambioPaneles(BEB.BEB, B.Bienvenida, Stu.Estudiante, NP.NotasPrin, RE.PanelContacto,
						RE.RegistroEstudiante, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, 
						Op.Opciones, CC.CambiarContrasena);
						
						//Limpia campos y listas
						Limpiar();
					}
					
						//---------------->Buscar Estudiante<--------------
						if(e.getSource().equals(BEB.Buscar)) {
							
							Mo.setCedula(BEB.CI.getText());//Enviando la cedula al modulo
							
							Co.Busqueda(Mo, false);//Procediendo a buscar en la base de datos
							
							if(Co.cedula != 0) {
								BEB.Resultado.setText(Co.Datos);//Imprime el resultado en el area de texto
								
								//Mostrar los botones
								BEB.Editar.setVisible(true);
								BEB.Borrar.setVisible(true);
								Co.cedula = 0;
							}
							else
							{
								JOptionPane.showMessageDialog(null, "Estudiante no encontrado. \nVerifique la cedula ingresada.");
							}
						}
						
						//---------------->Busqueda General<--------------
						if(e.getSource().equals(BEB.BusquedaG)) {
							
							//BEB.BusquedaG.setEnabled(false);
							
							//Ocultar los botones
							BEB.Editar.setVisible(false);
							BEB.Borrar.setVisible(false);
							BEB.Resultado.setText("");
							
							Co.ConsultaG(Mo);//Procediendo a buscar en la base de datos
							
							if(Co.cedula != 0) {
								BEB.Resultado.append(Co.Datos);//Imprime el resultado en el area de texto
							}
							else
							{
								JOptionPane.showMessageDialog(null, "Estudiante no encontrado. \nVerifique la cedula ingresada.");
							}
						}
//						else {
//							BEB.BusquedaG.setEnabled(true);
//						}
						
						//---------------->Editar Estudiante<--------------
						if(e.getSource().equals(BEB.Editar)) {

							R = JOptionPane.showOptionDialog(null, "¿Esta seguro que desea editar los datos de éste estudiante?",
										"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
										null, Botones, Botones[0]);
							if(R == 0){
								
								RE.Titulo.setText("EDITAR UN ESTUDIANTE");
								RE.Titulo.setBounds(195,5, 500, 75);

								//Limpia los campos y listas
								Limpiar();
								
								//Cambio de paneles
								CambioPaneles(RE.RegistroEstudiante, B.Bienvenida, Stu.Estudiante, NP.NotasPrin, RE.PanelContacto,
										BEB.BEB, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, 
										Op.Opciones, CC.CambiarContrasena);
								
								try { //Para asegurarnos que tendrá conexion
									GuardarDatosEstudiante(Co.DatosPersonales, Co.seccion, 1);
								    GuardarDatosEstudiante(Co.ContactoEstudiante, 0, 2);
								    GuardarDatosEstudiante2(Co.AsignaturaYFecha);
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
								
								RE.RegistroEstudiante.add(RE.Regresar);
								Editar = true;
							}
						}
						
						//---------------->Borrar Estudiante<--------------
						if(e.getSource().equals(BEB.Borrar)) {
							R = JOptionPane.showOptionDialog(null, "¿Esta seguro que desea ELIMINAR los datos de éste estudiante de la base de datos?",
									"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
									null, Botones, Botones[0]);
						    if(R == 0){
							
						    	//Se llama el procedimiento para borrar al estudiante
							    Co.Borrar(Mo);
							    
							    //Limpia los campos y listas
							    Limpiar();
							    
							  //Cambio de paneles
								CambioPaneles(Stu.Estudiante, B.Bienvenida, RE.PanelContacto, NP.NotasPrin, BEB.BEB,
								RE.RegistroEstudiante, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
								
						    }
						}
					
					//---------------->Editar un Estudiante<----------------
					if(e.getSource().equals(Stu.EditarEstudiante)) 
					{
						BEB.Titulo.setText("BUSQUE AL ESTUDIANTE");
						RE.Titulo.setText("EDITAR UN ESTUDIANTE");
						
						BEB.BusquedaG.setVisible(false);
					}	
					
					//---------------->Borrar un Estudiante<----------------
					if(e.getSource().equals(Stu.BorrarEstudiante)) {
						BEB.Titulo.setText("BORRAR UN ESTUDIANTE");
						BEB.Editar.setVisible(false);
					}
					
				/*-----------------------------------------------------------
				*---------------------->Notas<-------------------------------
				*-----------------------------------------------------------*/
				if(e.getSource().equals(MP.Algoritmo1) || e.getSource().equals(MP.Algoritmo2) || e.getSource().equals(IN.Regresar)) {
					
					//Algoritmo 1 y 2
					if(e.getSource().equals(MP.Algoritmo1)) {
						algoritmo1 = true;
						algoritmo2 = false;
					}else if(e.getSource().equals(MP.Algoritmo2)){
						algoritmo2 = true;
						algoritmo1 = false;
					}
					
					//Colocando falso a Editar nota para que se muestre la ventana emergente correspondiente
					EditarNota = false;
					
					//Limpieando componentes
					Limpiar();
					
					//cambio del titulo
					if(e.getSource().equals(MP.Algoritmo2)) { NP.Titulo.setText("NOTAS - Algoritmo 2");} 
					else if(e.getSource().equals(MP.Algoritmo1)){NP.Titulo.setText("NOTAS - Algoritmo");}
					
					//Cambio de paneles
					CambioPaneles(NP.NotasPrin, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
					BEB.BEB, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
				}
					//--------------->Ingresar una Nota<---------------
					if(e.getSource().equals(NP.RegistrarNota) || e.getSource().equals(NP.BuscarNota) || 
							e.getSource().equals(NP.EditarNota) || e.getSource().equals(NP.BorrarNota)) {

						//Cambio del titulo
						if(e.getSource().equals(NP.RegistrarNota)) {IN.Titulo.setText("INGRESAR NOTA"); IN.Editar.setVisible(false);IN.Borrar.setVisible(false); IN.Guardar.setVisible(true);} 
						else if (e.getSource().equals(NP.BuscarNota)) {IN.Titulo.setText("BUSCAR NOTA"); IN.Editar.setVisible(false); IN.Borrar.setVisible(false); IN.Guardar.setVisible(false);}
						else if (e.getSource().equals(NP.EditarNota)) {IN.Titulo.setText("EDITAR NOTA");IN.Editar.setVisible(true);IN.Borrar.setVisible(false);}
						else if (e.getSource().equals(NP.BorrarNota)) {IN.Titulo.setText("BORRAR NOTA");IN.Editar.setVisible(false);IN.Borrar.setVisible(true); IN.Guardar.setVisible(false); IN.Borrar.setBounds(400, 280, 120, 35);}
						
						//Cambio de paneles
						CambioPaneles(IN.InNota, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
						BEB.BEB, NP.NotasPrin, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
					}
					
					    //---------->Busqueda interna del estudiante<----------
					   if(SelectedSeccion == "") { SelectedSeccion = "Seccion";}
					   
					   if(e.getSource().equals(IN.Sec)) {
						   
						   if(IN.Sec.getSelectedIndex() != 0 && IN.Sec.getSelectedItem() != SelectedSeccion) {
						   
							   //Seccion seleccionada 
						       SelectedSeccion = IN.Sec.getSelectedItem().toString();
						       
						       //Se eliminan las opciones disponibles
						       IN.Estu.removeAllItems();
						       IN.IdEstudiantes.removeAllItems();
						   
						       //Se envia la seccion al modulo
						       Mo.setSeccion(IN.Sec.getSelectedIndex());
						   
						       IN.Estu.addItem("Estudiante");
						       IN.IdEstudiantes.addItem("Id's");
						   
						       //Se procede a la busqueda automatica
						       if(algoritmo1 && !algoritmo2) {
						    	   CoNotas.BusquedaAutomatica(Mo, IN.Estu, IN.IdEstudiantes, 1);
						       } else if(!algoritmo1 && algoritmo2) {
						    	   CoNotas.BusquedaAutomatica(Mo, IN.Estu, IN.IdEstudiantes, 2);
						       }
						  }
					   }
					   
					   //---------->Llenando la lista id del estudiante<----------
					   if(IN.Estu.getSelectedIndex() != 0) {
					       IN.IdEstudiantes.setSelectedIndex((IN.Estu.getSelectedIndex()));
				       }
					   
					   //---------->Busqueda de la Nota<----------
					   if(e.getSource().equals(IN.BuscarNota)) {
						   
						   if(IN.Uni.getSelectedIndex() != 0 && IN.Sec.getSelectedIndex() != 0 && IN.Estu.getSelectedIndex() != 0){
							   
							   //Condicion para saber si esta en ingresar nota o en buscar
							   if(IN.Titulo.getText() == "INGRESAR NOTA") {
								   BusquedaNota(false, true);
							   }
							   else
							   {
								   BusquedaNota(false, false);	
							   }						       
						   }else {
							   JOptionPane.showMessageDialog(null, "Rellene todos los componentes para buscar la nota del estudiante.");
						   }
					   }
						//---------->Guardar Nota<----------
						if(e.getSource().equals(IN.Guardar)) {
							
							if(IN.Notas.isVisible()) {
								//Seguridad ante la insercion de la nota
							     R = JOptionPane.showOptionDialog(null, "¿Esta seguro de que la nota del estudiante sea "+IN.Notas.getSelectedIndex()+"?",
								    	"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
									    null, Botones, Botones[0]);
						
						            if(R == 0) {
						    	        //Estableciendo nota
						    	        Mo.setNotaAlgoritmo(IN.Notas.getSelectedIndex());
						    	        
						    	        //Asegurarse de que asignatura es
						    	        if(algoritmo1 && !algoritmo2) { CoNotas.Actualizar(Mo, 1, IN.Uni.getSelectedIndex(), CoNotas.idEstudiante, false, EditarNota); }
						    	        else if (algoritmo2 && !algoritmo1) { CoNotas.Actualizar(Mo, 2, IN.Uni.getSelectedIndex(), CoNotas.idEstudiante, false, EditarNota);}
						            }
							}else {
							    JOptionPane.showMessageDialog(null, "Verifique que los campos: Sección, Unidad y Estudiante no estén vacíos.");
						    }
						}
						
						//---------->Editar Nota<-----------
						if(e.getSource().equals(IN.Editar)) {
							
							if(IN.Uni.getSelectedIndex() != 0 && IN.Sec.getSelectedIndex() != 0 && IN.Estu.getSelectedIndex() != 0){
								   
							   BusquedaNota(true, false);
						   }else {
							   JOptionPane.showMessageDialog(null, "Rellene todos los componentes para buscar la nota del estudiante.");
						   }

						}
						
						//---------->Borrar Nota<-----------
						if(e.getSource().equals(IN.Borrar)) {
							
							if(IN.Uni.getSelectedIndex() != 0 && IN.Sec.getSelectedIndex() != 0 && IN.Estu.getSelectedIndex() != 0){
								
								if(IN.NotasEstudiante.getText() != "") {
							        R = JOptionPane.showOptionDialog(null, "¿Esta seguro que desea BORRAR esta nota?",
								    	    "¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
									         null, Botones, Botones[0]);
							    
							        if(R == 0) {
							        	//Establece la nota en 0 para anularla en cualquier operacion
							        	Mo.setNotaAlgoritmo(0);
							    	
							    	    //Se realiza la operacion dependiendo en que asignatura exactamente esté
							    	    if(algoritmo1 && !algoritmo2) { CoNotas.Actualizar(Mo, 1, IN.Uni.getSelectedIndex(), CoNotas.idEstudiante, true, EditarNota); }
						    	        else if (algoritmo2 && !algoritmo1) { CoNotas.Actualizar(Mo, 2, IN.Uni.getSelectedIndex(), CoNotas.idEstudiante, true, EditarNota);}
							        }									
								}
								else
								{
									JOptionPane.showMessageDialog(null,  "No se encuentra la nota que quiere borrar.\n Dele click al boton buscar y luego al boton borrar.");
								}

							}else {
								   JOptionPane.showMessageDialog(null, "Rellene todos los componentes para buscar la nota del estudiante.");
						    }
						}
					
				/*-----------------------------------------------------------
				*---------------------->Maraton<-------------------------------
				*-----------------------------------------------------------*/			
				if(e.getSource().equals(MP.Maraton) || e.getSource().equals(RM.Regresar) || e.getSource().equals(BEBM.Regresar)) {
			        
			        //Desactivando el encontrado de la conexion al Maraton
			        CoMaraton.Encontrado = false;
			        
					//Cambio de paneles
					CambioPaneles(MaratonP.MaratonPrincipal, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
					BEB.BEB, NP.NotasPrin, IN.InNota, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
					
					//Limpiar componentes
					Limpiar();
				}
				
					//-------------->Registrar Maraton<--------------
					if(e.getSource().equals(MaratonP.RegistrarMaraton)) {
						
						//Aplicar texto al titulo
						if(e.getSource().equals(MaratonP.RegistrarMaraton)) {RM.Titulo.setText("REGISTRAR MARATON");}
						//else if(e.getSource().equals(MaratonP.EditarMaraton)) {RM.Titulo.setText("EDITAR MARATON");}
						
						//Cambio de paneles
						CambioPaneles(RM.RegistrarMa, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
						BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, BEBM.BuscarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
					}
					
					//-------------->Boton de Registrar Maraton<--------------
					if(e.getSource().equals(RM.Registrar)) {
						if(RM.Asig.getSelectedIndex() != 0 && RM.Sec.getSelectedIndex() != 0 && RM.Uni.getSelectedIndex() != 0 &&
								RM.Day.getSelectedIndex() != 0 && RM.Month.getSelectedIndex() != 0 && RM.Year.getSelectedIndex() != 0) 
						{
						    //Enviando datos al modulo
							Mo.setAsignatura(RM.Asig.getSelectedIndex());
							Mo.setSeccion(RM.Sec.getSelectedIndex());
							Mo.setUnidad(RM.Uni.getSelectedIndex());
							//Mo.setFecha_Ingreso(RM.Day.getSelectedItem().toString(), RM.Month.getSelectedItem().toString(), RM.Year.getSelectedItem().toString());
							CoMaraton.RegistrarMaraton(Mo);
							
							//Limpiar componentes
							Limpiar();
					    }
						else
						{
							JOptionPane.showMessageDialog(null, "Verifique que los siguientes campos no estén vacios: Asignatura, Seccion, Unidad y Fecha de comienzo");
						}
					}
					
					
					//-------------->Buscar Maraton<--------------
					
					if(e.getSource().equals(MaratonP.BuscarMaraton) || e.getSource().equals(RG.Regresar) 
							|| e.getSource().equals(MaratonP.SuspenderMaraton) || 
							e.getSource().equals(MaratonP.EditarMaraton)){
						
						//Poniendo invisibles a los botones registrar ganadores, editar y borrar
						BEBM.RegistrarGanadores.setVisible(false);
				     	BEBM.Editar.setVisible(false);
				        BEBM.Suspender.setVisible(false);
				        
						//Aplicar texto al titulo
						if(e.getSource().equals(MaratonP.BuscarMaraton)){
							
							//Colocando el titulo							
							BEBM.Titulo.setText("BUSCAR MARATON");
							EditandoMaraton = false;
							SuspendiendoMaraton = false;
						}
						else if(e.getSource().equals(MaratonP.EditarMaraton)) {
							
							//Colocando el titulo
							BEBM.Titulo.setText("EDITAR MARATON");
							EditandoMaraton = true;
							SuspendiendoMaraton = false;
						}
						else if(e.getSource().equals(MaratonP.SuspenderMaraton)) {
							
							//Colocando el titulo
							BEBM.Titulo.setText("SUSPENDER MARATON"); 
							SuspendiendoMaraton = true;
							EditandoMaraton = false;
						}
                        
						//Cambio de paneles
						CambioPaneles(BEBM.BuscarMa, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
						BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
					}
					
						//-------------->Registrar Ganadores del Maraton<--------------
						if(e.getSource().equals(BEBM.RegistrarGanadores) || e.getSource().equals(BEBM.Editar)){
							
							//Aplicar texto al titulo
							if(e.getSource().equals(BEBM.RegistrarGanadores)) {RG.Titulo.setText("REGISTRAR GANADORES"); EditandoMaraton = false;}
							else if(e.getSource().equals(BEBM.Editar)) {RG.Titulo.setText("EDITAR GANADORES"); EditandoMaraton = true;}

							//Enviar los datos al modulo
							Mo.setAsignatura(BEBM.Asig.getSelectedIndex());
							Mo.setSeccion(BEBM.Sec.getSelectedIndex());
							Mo.setUnidad(BEBM.Uni.getSelectedIndex());
							
							//informe de cargando
							JOptionPane.showMessageDialog(null,  "Cargando...");
							
							int Asignatura = BEBM.Asig.getSelectedIndex();
                        	int Seccion = BEBM.Sec.getSelectedIndex();
							
							//Limpiando
							Limpiar();
							
							//Deshabilitando el boton de registrar
							RG.Registrar.setEnabled(false);
							
							//Habilitar la lista del estudiante n1
							RG.Primer.setEnabled(true);
							RG.UnidadPremio1.setEnabled(true);

							//Agregar estudiante e id
							RG.Primer.addItem("Estudiantes");
							RG.Segundo.addItem("Estudiantes");
							RG.Tercer.addItem("Estudiantes");
							RG.IdEstudiantes.addItem("Id");
							
							//Busqueda interna
							CoMaraton.BusquedaEstudiantes(Mo, Mo.getAsignatura(), Mo.getSeccion(), Mo.getUnidad(), RG.Primer, RG.Segundo, RG.Tercer, RG.IdEstudiantes); //Asignatura, Seccion y Unidad
							
							//Cambio de paneles
							CambioPaneles(RG.RegistrarGanadores, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
							BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, Op.Opciones, CC.CambiarContrasena);
                            
							//Asignacion de los botones
							BD.AsignacionImagen(RG.Registrar, "src/img/icons/Buttons/Light Mode/BotonGuardar.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHover.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHold.png");
					        BD.AsignacionImagen(RG.Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");

						}
						
						//BuscarMaraton
						if(e.getSource().equals(BEBM.Buscar)) {
							
							//Enviado datos al modulo
							Mo.setAsignatura(BEBM.Asig.getSelectedIndex());
							Mo.setSeccion(BEBM.Sec.getSelectedIndex());
							Mo.setUnidad(BEBM.Uni.getSelectedIndex());
							
							//Buscando los ganadores
							CoMaraton.BusquedaMaraton(Mo, true);					
		
							//JOptionPane.showMessageDialog(null, "Buscando...");
							
							//Aplicando el texto en el area de texto
							BEBM.ResultadosBusqueda.setText(CoMaraton.Datos);

							//Vector de los botones a interactuar
					        //JButton Botones[] = { BEBM.Registrar, BEBM.Editar, BEBM.Suspender};
					        
							//Aparecen los botones al encontrar el maraton
	                        if(CoMaraton.Encontrado == true) {
								
	                        	if(EditandoMaraton) {
	    							
	    							//Habilitando la visibilidad
	                        		//-----------------------------RegistrarGanadores, Editar, Suspender
	                        		HabilitandoBotonCorrespondiente(false, true, false);
			     	                
	                        	}
	                        	else if(SuspendiendoMaraton) {

	                        		//Habilitando la visibilidad
	                        		//-----------------------------RegistrarGanadores, Editar, Suspender
	                        		HabilitandoBotonCorrespondiente(false, false, true);
	                        		
	                        	}else {
	                        		
	                        		//Habilitando la visibilidad
	                        		//-----------------------------RegistrarGanadores, Editar, Suspender
	                        		HabilitandoBotonCorrespondiente(true, false, false);
	                        	}  
	                        }
						}
						    
						    //Buscando datos del estudiante en Registrar Ganadores
						    if(SelectedEstudiante == "") { SelectedEstudiante = "Estudiante";}
						   
						    if(e.getSource().equals(RG.Primer) && RG.RegistrarGanadores.isVisible()) {
							   
						    	//Enviando datos para verificar los datos de las listas
                                SeguridadEstudianteGanador(RG.Primer, SelectedEstudiante, RG.UnidadPremio1, 1, Mo.getSeccion());
						    }else 
						    	if(e.getSource().equals(RG.Segundo) && RG.RegistrarGanadores.isVisible()) {
						    	
						    	//Enviando datos para verificar los datos de las listas
                                SeguridadEstudianteGanador(RG.Segundo, SelectedEstudiante, RG.UnidadPremio2, 2, Mo.getSeccion());
						    }else 
						    	if(e.getSource().equals(RG.Tercer) && RG.RegistrarGanadores.isVisible()) {
						    		
						    	//Enviando datos para verificar los datos de las listas
                                SeguridadEstudianteGanador(RG.Tercer, SelectedEstudiante, RG.UnidadPremio3, 3, Mo.getSeccion());
						    }
						    
						    //Habilitar las nuevas listas luego del llenado de las anteriores
						    if(e.getSource().equals(RG.UnidadPremio1) && RG.RegistrarGanadores.isVisible()) {
						    	
						    	//Verificacion de las listas
						    	LlenadoListas(RG.UnidadPremio1, RG.Primer, RG.Segundo, RG.UnidadPremio2, RG.IdEstudiantes);
						    } else
							    if(e.getSource().equals(RG.UnidadPremio2) && RG.RegistrarGanadores.isVisible()) {
							    	
							    	//Verificacion de las listas
							    	LlenadoListas(RG.UnidadPremio2, RG.Segundo, RG.Tercer, RG.UnidadPremio3, RG.IdEstudiantes);
							    }
							    else
								    if(e.getSource().equals(RG.UnidadPremio3) && RG.RegistrarGanadores.isVisible()) {
								    	
								    	//Verificacion de las listas
								    	LlenadoListas(RG.UnidadPremio3, RG.Tercer, null, null, RG.IdEstudiantes);
								    }
						    
						    //Si registran los ganadores del maraton
						    if(e.getSource().equals(RG.Registrar)) {
						    	
						    	//Guardando en la base de datos
						    	CoMaraton.RegistrarGanadores(Mo);
						    	
						    	//Poniendo invisibles a los botones registrar ganadores, editar y borrar
								BEBM.RegistrarGanadores.setVisible(false);
						     	BEBM.Editar.setVisible(false);
						        BEBM.Suspender.setVisible(false);
						        
						      //Cambio de paneles
								CambioPaneles(BEBM.BuscarMa, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
								BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, RG.RegistrarGanadores, Op.Opciones, CC.CambiarContrasena);
						    	Limpiar();
						    }
						    
						//-------------->Suspender del Maraton<--------------
						if(e.getSource().equals(BEBM.Suspender)) {
							
							R = JOptionPane.showOptionDialog(null, "¿Esta seguro que desea SUSPENDER este maraton?",
									"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
									null, Botones, Botones[0]);
							
							//Suspendiendo el maraton - Borrandolo de la base de datos
							CoMaraton.SuspenderMaraton(CoMaraton.idMaraton);
						}
				/*-----------------------------------------------------------
				*--------------------->Opciones<-----------------------------
				*-----------------------------------------------------------*/		
				if(e.getSource().equals(MP.Opciones) || e.getSource().equals(Op.Regresar) || e.getSource().equals(CC.Regresar)) {

					InterfazOpciones();
					
					//Cambio de paneles
					CambioPaneles(Op.Opciones, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
					BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, CC.CambiarContrasena);
				}
				
				/*----------------------------------
				//*********Cambiar contraseña*************
				-----------------------------------*/
					
					if(e.getSource().equals(Op.CambiarPass)) {
						//Variable necesaria
						CambiarPassword = true;
						
						//Poniendo un titulo
						Op.Titulo.setText("Cambiar Contraseña");
						Op.Titulo.setFont(new Font("RussellSquare", 1, 40)); //Fuente de texto
						Op.Titulo.setBounds(210,5, 500, 75); //Posicion y tamaño
						
						//Cambio de paneles
						CambioPaneles(CC.CambiarContrasena, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
						BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, 
						 Op.Opciones);
						
						//Desactivando visibilidad
						Op.CambiarCorreo.setVisible(false);
						Op.CambiarPass.setVisible(false);
						Op.Exportar.setVisible(false);
						Op.Importar.setVisible(false);
						Op.Opciones.setVisible(false);
						
						//Activando visibilidad a los componente de cambiar correo electronico
						CC.InContrasena.setVisible(true);
						CC.Contrasena.setVisible(true);
						CC.Enviar.setVisible(true);
						CC.Regresar.setVisible(true);
					}
						
						if(e.getSource().equals(CC.Enviar) && CambiarPassword) {
				
							//Si la contraseña es la clave del desarrollador
							if(CC.Contrasena.getText().toString().equals("GENOAL2021") || CC.Contrasena.getText().toString().equals(Clave)) {
								
								JOptionPane.showMessageDialog(null, "Contraseña correcta");
								
								//Desactivando componentes
								Op.Contrasena.setVisible(false);
								Op.Enviar.setVisible(false);
								Op.InContrasena.setVisible(false);
								
								//Aplicando texto
								//CC.InContrasena.setText("Ingrese su nueva contraseña");
								
								//Activando componentes
								CC.InContrasena.setVisible(false);
							    CC.InContrasenaNueva.setVisible(true);
							    CC.Contrasena.setVisible(false);
								CC.NuevaContrasena.setVisible(true);
								CC.Enviar.setVisible(false);
								CC.Guardar.setVisible(true);
								CC.Regresar.setVisible(true);
								Contador = 0;
							} 
							else if(CC.Contrasena.getText().toString().equals("") && CambiarPassword){ //Si el campo esta vacio
								
								JOptionPane.showMessageDialog(null, "El campo está vacío, ingrese la contraseña.");
								
								}else {//Si se equivoca
									
								Contador = Contador + 1; //Contador para seguridad del programa
								
								if(Contador != 3) {//Mensaje de contraseña incorrecta

									JOptionPane.showMessageDialog(null, "Contraseña incorrecta.\nVuelva a intentar.");
									JOptionPane.showMessageDialog(null, "La contraseña es: "+Co.ObtenerPass());
								}
							}
						}
						
						//Si guarda contraseña y el campo esta vacio
						
						if(e.getSource().equals(CC.Guardar) && CC.NuevaContrasena.getText().toString().equals("") && CambiarPassword) 
						{ 
							JOptionPane.showMessageDialog(null, "El campo está vacío, ingrese su nueva contraseña.");
						}
						else 
							if(e.getSource().equals(CC.Guardar) && CambiarPassword) { //Si guarda y el campo no está vacio
							
							R = JOptionPane.showOptionDialog(null, "¿Está seguro que desea CAMBIAR la contraseña?",
									"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
									null, Botones, Botones[0]);
							
							//Si el cliente decide cmabiar la contraseña
							if(R == 0) {
								
								//Pasando la contraseña al modulo y luego a la base de datos
								Mo.setPass(CC.NuevaContrasena.getText().toString());
								
								//Cambiando contraseña
							    Co.CambiarPass(Mo);
							    
							    //Clave de seguridad
								Clave = Co.ObtenerPass();
								
								//Limpiando datos
							    Limpiar();
							    
								CambioPaneles(Op.Opciones, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
										BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, CC.CambiarContrasena);
								
								//Regresando a la interfaz principal de opciones 
								InterfazOpciones();
								
								//Regresando el contador a su estaod inicial
								Contador = 0;
							}
						}

				/*----------------------------------
				//**********Cambiar correo*************
				-----------------------------------*/
						
				if(e.getSource().equals(Op.CambiarCorreo)) {
					//Variable necesaria
					CambiarPassword = false;
					CambiarCorreo = true;
							
					//Poniendo un titulo
					Op.Titulo.setText("Cambiar Correo Electronico");
					Op.Titulo.setFont(new Font("RussellSquare", 1, 40));
					Op.Titulo.setBounds(150, 25, 500, 75);
							
				    //Desactivando visibilidad
				    Op.CambiarCorreo.setVisible(false);
				    Op.CambiarPass.setVisible(false);
							
					//Activando visibilidad a los componente de cambiar correo electronico
					Op.InContrasena.setVisible(true);
					Op.Contrasena.setVisible(true);
					Op.Enviar.setVisible(true);
					Op.Regresar.setVisible(true);

				}
				
				//Si presionan enviar la contraseña
				if(e.getSource().equals(Op.Enviar) && CambiarCorreo) {
					
					//Si la contraseña es la clave del desarrollador
					if(Op.Contrasena.getText().toString().equals("GENOAL2021") || Op.Contrasena.getText().toString().equals(Clave)) {
						
						JOptionPane.showMessageDialog(null, "Contraseña correcta");
						//Desactivando componentes
						Op.Contrasena.setVisible(false);
						Op.Enviar.setVisible(false);
						Op.InContrasena.setVisible(false);
						
						//Aplicando texto
						Op.InContrasena.setText("Ingrese su nuevo correo");
						
						//Activando componentes
						Op.InCorreo.setVisible(true);
						Op.Correo.setVisible(true);
						Op.Guardar.setVisible(true);
						Op.Regresar.setVisible(true);
						Contador = 0;
					} 
					else if(Op.Contrasena.getText().toString().equals("")){ //Si el campo esta vacio
						
						JOptionPane.showMessageDialog(null, "El campo está vacío, ingrese la contraseña.");
						
					}else {//Si se equivoca
							
						Contador = Contador + 1; //Contador para seguridad del programa
						
					    if(Contador != 3) {//Mensaje de contraseña incorrecta
							JOptionPane.showMessageDialog(null, "Contraseña incorrecta.\nVuelva a intentar.");
						}
					}
				}
						
				//Si guarda contraseña y el campo esta vacio
				
				if(e.getSource().equals(Op.Guardar) && Op.Correo.getText().toString().equals("") && CambiarCorreo) 
				{ 
					JOptionPane.showMessageDialog(null, "El campo está vacío, ingrese su nuevo correo electrico.");
				}
				else 
					if(e.getSource().equals(Op.Guardar) && CambiarCorreo) { //Si guarda y el campo no está vacio
					
					R = JOptionPane.showOptionDialog(null, "¿Está seguro que desea CAMBIAR el correo electronico?",
							"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
							null, Botones, Botones[0]);
					
					//Si el cliente decide cmabiar la contraseña
					if(R == 0) {
						
						//Pasando la contraseña al modulo y luego a la base de datos
						Mo.setEmail(Op.Correo.getText().toString());
						
						JOptionPane.showMessageDialog(null, "El nuevo correo electronico es: "+Mo.getEmail());
					    Co.CambiarEmail(Mo);
					    
					    Limpiar();
					
					    Op.InContrasena.setText("Ingrese la contraseña");
					    
						CambioPaneles(Op.Opciones, B.Bienvenida, Stu.Estudiante, RE.RegistroEstudiante, RE.PanelContacto,
								BEB.BEB, NP.NotasPrin, IN.InNota, MaratonP.MaratonPrincipal, RM.RegistrarMa, BEBM.BuscarMa, RG.RegistrarGanadores, CC.CambiarContrasena);
						
						//Regresando a la interfaz principal de opciones 
						InterfazOpciones();
						
						//Regresando a su estado inicial
						Contador = 0;
					}
				}
				
				/*---------------------------------------------
				//**********Exportar Base de Datos*************
				-----------------------------------------------*/
				
				if(e.getSource().equals(Op.Exportar)) {
					
					//Mostrando la ventana de Exportar
					Ex.Contenedor.setVisible(true); 
					Ex.Barra.setVisible(true);
					
				}
				
				//Seleccionar ruta de donde se guardará el archivo
				if(e.getSource().equals(Ex.Seleccionar)) {
					
					//Buscando lugar donde guardarlo
					Ch = new JFileChooser();
					Ch.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					
					int se = Ch.showSaveDialog(null);
					
					if(se == JFileChooser.APPROVE_OPTION) {
						
						String ruta = Ch.getSelectedFile().getPath();
						Ex.Campo.setText(ruta);
					}
				}
				
				//Exportar archivo - Backup
				if(e.getSource().equals(Ex.ExportarArchivo)) {
					
					String ruta = Ex.Campo.getText();
					String NombreArchivo = "\\Backup.sql";
					String Backus = "";
					
					if(ruta.trim().length() != 0) {
						
						try {
										
							//Comando SQL
							Backus = "C://wamp//bin//mysql//mysql5.5.24//bin//mysqldump --opt --host=localhost -u"+
							        Co.getUsuario()/*+" -p"+Co.getPass()*/+" -B "+Co.getBD()+" -r "+ 
									ruta + NombreArchivo;
						    
							//Componente para ejecutar el comando SQL
							Runtime rt = Runtime.getRuntime();
						    rt.exec(Backus);
						    
						    System.out.println(ruta);
							//JOptionPane.showMessageDialog(null, "La ruta es: "+ruta);
						    JOptionPane.showMessageDialog(null, "Exportado correctamente.");
						}catch(Exception Ex) {
							
							JOptionPane.showMessageDialog(null, Ex.getMessage());
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Seleccione una dirección válida.");
					}
				}
				
				//Si cancela la creacion del backup
				if(e.getSource().equals(Ex.Cancelar)) {
					
					Ex.Contenedor.setVisible(false);
					Ex.Barra.setVisible(false);
				}
				
				/*---------------------------------------------
				//**********Importar Base de Datos*************
				-----------------------------------------------*/
				
				if(e.getSource().equals(Op.Importar)) {
					
					//Mostrando la ventana de Exportar
					Im.Contenedor.setVisible(true); 
					Im.Barra.setVisible(true);
					
				}
				
				//Seleccionar ruta de donde se guardará el archivo
				if(e.getSource().equals(Im.Seleccionar)) {
					
					//Buscando lugar donde guardarlo
					Ch = new JFileChooser();
                    FileNameExtensionFilter Fil = new FileNameExtensionFilter("SQL", "sql");
                    Ch.setFileFilter(Fil);
                    
					int se = Ch.showOpenDialog(null);
					
					if(se == JFileChooser.APPROVE_OPTION) {
						
						String ruta = Ch.getSelectedFile().getPath();
						Im.Campo.setText(ruta);
					}
				}
				
				//Importar archivo - Backup
				if(e.getSource().equals(Im.ImportarArchivo)) {
					
					String ruta = Im.Campo.getText();
					String Backus = "";
					
					if(ruta.trim().length() != 0) {
						
						try {
										
							//Comando SQL
							
							/*Backus = "C://wamp//bin//mysql//mysql5.5.24//bin//mysqldump --opt --host=localhost -u"+
							        Co.getUsuario()/*+" -p"+Co.getPass()/+" -B "+Co.getBD()+" -r "+ 
									ruta;*/
						    
							Backus = "cmd /c mysql -u"+Co.getUsuario()+" "+Co.getBD()+" < "+ ruta;
							//Componente para ejecutar el comando SQL
							Runtime rt = Runtime.getRuntime();
						    rt.exec(Backus);
						    
						    System.out.println(ruta);
							//JOptionPane.showMessageDialog(null, "La ruta es: "+ruta);
						    JOptionPane.showMessageDialog(null, "Importado correctamente.");
						}catch(Exception Im) {
							
							JOptionPane.showMessageDialog(null, Im.getMessage());
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Seleccione una dirección válida.");
					}
				}
				
				//Si cancela la creacion del backup
				if(e.getSource().equals(Im.Cancelar)) {
					
					Im.Contenedor.setVisible(false);
					Im.Barra.setVisible(false);
				}
				
				/*-----------------------------------------------------------
				*---------------------->Salir<-------------------------------
				*-----------------------------------------------------------*/
				if(e.getSource().equals(MP.Salir)) {
					System.exit(0);
				}
						
			}
	
	
	public void CambioPaneles(JPanel PanelActivar, JPanel PD1, JPanel PD2, JPanel PD3, JPanel PD4, JPanel PD5, JPanel PD6,
			JPanel PD7, JPanel PD8, JPanel PD9, JPanel PD10, JPanel PD11, JPanel PD12) {
		
		//Declarando vector para meter los paneles
		JPanel Vector[] = { PD1,  PD2,  PD3,  PD4,  PD5, PD6, PD7, PD8,  PD9,  PD10,  PD11, PD12};
		
		//Recorriendo el vector
		for(int i = 0; i <= 11; i++) {	
			Vector[i].setVisible(false);
		}
		
		//Activando el panel a ver
		PanelActivar.setVisible(true);
	}
	
	public void Limpiar() {

		VaciandoCamposRE();
		VaciandoBusqueda();
		VaciandoNotas();
		VaciandoRegistrarMaraton();
		VaciandoBEBMaraton();
		VaciandoRegistrarGanadoresMaraton();
		VaciandoOpciones();
		
    }
	
	public void VaciandoCamposRE() {

		//Se crea el vector para los campos de texto y listas
		JTextField Campos[] = {RE.Nom, RE.Ape, RE.CI, RE.TLF, RE.correo, RE.facebook, RE.twitter, RE.instagram, RE.telegram, RE.lol, RE.wow};
		JComboBox Listas[] = {RE.Asig, RE.Sec};
		
		for(int i = 0; i <= 10; i++) {//Recorre el vector y vacia cada componente 
		   Campos[i].setText("");
		   if(i <= 1) {
			   Listas[i].setSelectedIndex(0);
		   }
		}
		
		RE.Calendario.setDate(Mo.FechaInicial());
	}
	
	public void VaciandoBusqueda() {
		
		//Limpiando campos y volviendo invisibles los botones necesarios
		BEB.CI.setText("");
		BEB.Resultado.setText("");
		BEB.Editar.setVisible(false);
		BEB.Borrar.setVisible(false);
	}
	
	public void VaciandoNotas() {
		
		//Listas
		JComboBox Listas[]= {IN.Sec, IN.Uni, IN.Estu, IN.Notas};
		
		//Recorre el vector y vacia cada componente 
		for(int i = 0; i <= 3; i++) {
			Listas[i].setSelectedIndex(0);
		}
		
		//Vaciando otros componentes
		IN.IdEstudiantes.removeAllItems();
		IN.Estu.removeAllItems();
		IN.NotasEstudiante.setText("");
		SelectedSeccion = "Seccion";
		
		//Agregando el item Estudiante a la lista
		IN.Estu.addItem("Estudiante");
		
		//Estableciendo invisibilidad
		IN.IngreseNota.setVisible(false);
		IN.Notas.setVisible(false);
	}
	
	public void VaciandoRegistrarMaraton() {
		
		//Listas
		JComboBox Listas[]= {RM.Asig, RM.Sec, RM.Uni, RM.Day, RM.Month, RM.Year};
		
		//Recorre el vector y vacia cada componente 
		for(int i = 0; i <= 5; i++) {
			Listas[i].setSelectedIndex(0);
		}
	}
	
	public void VaciandoBEBMaraton() {
		
		//Vector para las listas
		JComboBox Listas[] = { BEBM.Asig, BEBM.Sec, BEBM.Uni};
		
		//Recorre el vector y vacia cada componente 
		for(int i = 0; i <= 2; i++) {
			Listas[i].setSelectedIndex(0);
		}
		
		//Vaciando los resultados
		BEBM.ResultadosBusqueda.setText("");

	}
	
	public void VaciandoRegistrarGanadoresMaraton() {
		
		//Vector para las listas
		JComboBox Listas[] = { RG.Primer, RG.Segundo, RG.Tercer, RG.UnidadPremio1, RG.UnidadPremio2, RG.UnidadPremio3, RG.IdEstudiantes};
		
		//Recorre el vector y vacia cada componente 
		for(int i = 0; i <= 6; i++) {
			Listas[i].removeAllItems();
		}
		
		//Vaciando los resultados
		RG.DatosEstudiante.setText("");
	}
	
	public void GuardarDatosEstudiante(String Vector[], int entero, int i) throws SQLException {
   		//Llenar la primera parte de los datos del estudiante
   	    //-----------Datos Personales-----------
   	    if(i == 1) {
   		    RE.Nom.setText(Vector[0]);
   	        RE.Ape.setText(Vector[1]);
   	        RE.CI.setText(Vector[2]);
   	        RE.Sec.setSelectedIndex(entero);
   	    }
    	
   	    //-----------Contacto-----------
   	    if(i == 2) {
   		    RE.TLF.setText(Vector[0]);
   		    RE.facebook.setText(Vector[1]);
  		    RE.twitter.setText(Vector[2]);
   		    RE.instagram.setText(Vector[3]);
   		    RE.correo.setText(Vector[4]);
   		    RE.telegram.setText(Vector[5]);
   		    RE.lol.setText(Vector[6]);
  		    RE.wow.setText(Vector[7]);
  		    
   	    }
   	}      	  
    
	public void GuardarDatosEstudiante2(int Vector[]) {
		   	    
		//-----------Asignatura, Seccion y Fecha de ingreso-----------
    	RE.Asig.setSelectedIndex(Vector[0]);
    	/*RE.Day.setSelectedIndex();
	    RE.Month.setSelectedIndex();
	        RE.Year.setSelectedIndex();*/  
	}
	
	public void BusquedaNota(Boolean Editar, Boolean Ingresar) {
		   if(algoritmo1) { //Si estan en algoritmo 1							   
			    CoNotas.BusquedaNota(Mo, IN.Uni.getSelectedIndex(), 1, (int) IN.IdEstudiantes.getSelectedItem());
			    IN.NotasEstudiante.setText(CoNotas.Datos);
			    Buscar_O_Editar(Editar, Ingresar);
		    } else if(algoritmo2) { //Si estan en algoritmo 2
			    CoNotas.BusquedaNota(Mo, IN.Uni.getSelectedIndex(), 2, (int) IN.IdEstudiantes.getSelectedItem());
			    IN.NotasEstudiante.setText(CoNotas.Datos);
			    Buscar_O_Editar(Editar, Ingresar);
		    }
	       IN.NotasEstudiante.setEditable(false);	
	}
	
	//Metodo que sirve para verificar si es buscar nota o editar nota y disminuir la cantidad de lineas en la clase
	public void Buscar_O_Editar(Boolean Editar, Boolean Ingresar) {
		
		if(!Editar) {
			if(IN.Titulo.getText() == "BUSCAR NOTA") {
				IN.Editar.setVisible(false);
			}
        }
		else
		{
		    R = JOptionPane.showOptionDialog(null, "¿Esta seguro que desea editar esta nota?",
						"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
						null, Botones, Botones[0]);
			
		   if(R == 0) {
			   IN.Editar.setVisible(false);
			   IN.Borrar.setVisible(false);
			   IN.Notas.setVisible(true);
			   IN.IngreseNota.setVisible(true);
			   IN.Guardar.setVisible(true);
			   EditarNota = true;
		   }
		}
		
		if(Ingresar) {
			IN.Notas.setVisible(true);
		    IN.IngreseNota.setVisible(true);
		}
	}
	
	public void SeguridadEstudianteGanador(JComboBox ListaEstudiantes, String Estudiante, JComboBox UnidadPonderar, int ListaSeleccionada, int Seccion) {
		
        if(ListaEstudiantes.getSelectedIndex() != 0 && ListaEstudiantes.getSelectedItem() != Estudiante) {
			   
	       //Seccion seleccionada 
           Estudiante = ListaEstudiantes.getSelectedItem().toString();
       
           //Se eliminan las opciones disponibles   
           UnidadPonderar.removeAllItems();
           UnidadPonderar.addItem("Unidad");
   
           //Se procede a la busqueda automatica de las notas disponibles
    	   
		   if(Mo.getAsignatura() == 1) {
			   
	           //Metodo para asegurarse que la lista de id le sigue el paso a la lista de estudiantes
			   AumentarListaID(ListaEstudiantes, RG.IdEstudiantes);
			   
			   //Busqueda automatica de las notas posibles para ponderar el premio			   
		       CoMaraton.BusquedaAutomatica(1, (int) RG.IdEstudiantes.getSelectedItem(), UnidadPonderar, ListaSeleccionada);
    	       
		       //Metodo para mostrar los resultados del estudiante en el campo resultado
		       CoMaraton.Busqueda((int) RG.IdEstudiantes.getSelectedItem(), 1);
			   
		       //Estableciendo los datos del estudiante
		       RG.DatosEstudiante.setText(CoMaraton.Datos);
		   } 
		   else 
			   if(Mo.getAsignatura() == 2) {
        	   
        	   //Metodo para asegurarse que la lista de id le sigue el paso a la lista de estudiantes
			   AumentarListaID(ListaEstudiantes, RG.IdEstudiantes);
			   
			   //Busqueda automatica de las notas posibles para ponderar el premio
			   CoMaraton.BusquedaAutomatica(2, (int) RG.IdEstudiantes.getSelectedItem(), UnidadPonderar, ListaSeleccionada);
    	       
			   //Metodo para mostrar los resultados del estudiante en el campo resultado
			   CoMaraton.Busqueda((int) RG.IdEstudiantes.getSelectedItem(), 2);

			   //Estableciendo los datos del estudiante
			   RG.DatosEstudiante.setText(CoMaraton.Datos);			   
           }
        }
        else {
        	//Mensaje de advertencia
        	JOptionPane.showMessageDialog(null, "La información de la lista es: "+ListaEstudiantes.getSelectedItem().toString());
        }
      }
	
	public void AumentarListaID(JComboBox ListaEstudiantes, JComboBox ListaID) {
		
		//---------->Llenando la lista id del estudiante<----------
	    if(ListaEstudiantes.getSelectedIndex() != 0) {
	        ListaID.setSelectedIndex(ListaEstudiantes.getSelectedIndex());
	    }
	}
	
	public void LlenadoListas(JComboBox UnidadPremiada, JComboBox EstudianteGanador, JComboBox SiguienteEstudiante, JComboBox SiguienteUnidad, JComboBox ListaID) {
		
		//Si unidad es distinto a 0, entonces se pregunta si esta seguro
    	if(UnidadPremiada.getSelectedIndex() != 0 && UnidadPremiada.getSelectedItem() != null) {
    		
    		//Seguridad ante la insercion de la nota
	        R = JOptionPane.showOptionDialog(null, "¿Esta seguro de que la unidad a ponderar sea la unidad "+UnidadPremiada.getSelectedItem()+"?",
		    	"¡Advertencia!", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE,
			    null, Botones, Botones[0]);

            if(R == 0) { //Si aceptan, entonces
            	
            	JOptionPane.showMessageDialog( null , "El id del estudiante es "+RG.IdEstudiantes.getSelectedItem());
            	if(SiguienteEstudiante == null && SiguienteUnidad == null) { //Si estan llenando la ultima lista
            		//Enviando el id de los ganadores al modulo
        	    	Mo.setIdGanador3((int) RG.IdEstudiantes.getSelectedItem());
            		
                	JOptionPane.showMessageDialog( null , "El id del estudiante es "+RG.IdEstudiantes.getSelectedItem());
                	
        	    	//Desactivando la lista para que no editen
            		EstudianteGanador.setEnabled(false);
            	    UnidadPremiada.setEnabled(false);
            	    
            	    if(RG.Tercer.getSelectedIndex() != 0 && RG.UnidadPremio3.getSelectedItem() != null) {
            	    	
            	    	//Activando el boton para guardar
            	        RG.Registrar.setEnabled(true);
            	    }
            	       	
            	    //Borrando el id de la lista de id de estudiantes
            		RG.IdEstudiantes.removeItemAt(RG.IdEstudiantes.getSelectedIndex());        	    	
            	}
            	else //Aun quedan listas por llenar
            	{
            		
            		//Borrando el ganador seleccionado de la siguiente lista y su id de la lista de ids
            		SiguienteEstudiante.removeItemAt(EstudianteGanador.getSelectedIndex());
            		
            		//borrando la tercera lista solo cuando se llene la primera
            		if(EstudianteGanador.equals(RG.Primer)) {
            			RG.Tercer.removeItemAt(EstudianteGanador.getSelectedIndex());
            			
            			//Enviando el id de los ganadores al modulo
            			Mo.setIdGanador1((int) RG.IdEstudiantes.getSelectedItem());
            		}
            		
            	    //Condicion para enviar el id del estudiante
            	    if(EstudianteGanador.equals(RG.Segundo)) { 
            	    	
            	    	//Enviando el id de los ganadores al modulo
            	    	Mo.setIdGanador2((int) RG.IdEstudiantes.getSelectedItem());
            	    }
            	    
            	    //Borrando el id de la lista de id de estudiantes
            		RG.IdEstudiantes.removeItemAt(RG.IdEstudiantes.getSelectedIndex());

            		//Desactivando la lista para que no editen
            		EstudianteGanador.setEnabled(false);
            	    UnidadPremiada.setEnabled(false);
            	    
            	    //Activando las siguientes listas
            	    SiguienteEstudiante.setEnabled(true);
            	    SiguienteUnidad.setEnabled(true);
            	}
            }
    	}
	}
	
	public void HabilitandoBotonCorrespondiente(Boolean Registrar, Boolean Editar, Boolean Suspender) {
		
		//Botones
        BEBM.RegistrarGanadores.setVisible(Registrar);
        BEBM.Editar.setVisible(Editar);
        BEBM.Suspender.setVisible(Suspender);
	}
	
	public void VaciandoOpciones() {

		//Se crea el vector para los campos de texto y listas
		JTextField Campos[] = {Op.Contrasena, Op.Correo, CC.Contrasena, CC.NuevaContrasena};
		
		for(int i = 0; i <= 3; i++) {//Recorre el vector y vacia cada componente 
		   Campos[i].setText("");
		}
	}
	
	public void InterfazOpciones() {
		
		//Variable necesaria
		CambiarPassword = false;
		CambiarCorreo = true;
		
		Op.Titulo.setText("OPCIONES");
		Op.Titulo.setBounds(270,25, 500, 75);
		Op.Titulo.setFont(new Font("RussellSquare", 1, 60));
		
		Op.InContrasena.setVisible(false);
		Op.InCorreo.setVisible(false);
		Op.Contrasena.setVisible(false);
		Op.Enviar.setVisible(false);
		Op.Regresar.setVisible(false);
		Op.Correo.setVisible(false);
		Op.Guardar.setVisible(false);
		
		Op.CambiarCorreo.setVisible(true);
		Op.CambiarPass.setVisible(true);
		Op.Exportar.setVisible(true);
		Op.Importar.setVisible(true);
		
		CC.Contrasena.setText("");
		CC.NuevaContrasena.setText("");
		CC.InContrasenaNueva.setVisible(false);
		CC.NuevaContrasena.setVisible(false);
		
		Limpiar();
	}
	
	public Date Fecha(java.util.Date Fecha, JDateChooser Calendar) {
		
		//Declaracion del resultado actual del horario
		Fecha = Calendar.getDate();
		
		FechaLarga = Fecha.getTime();
		
		this.Fecha = new java.sql.Date(FechaLarga);
		
		JOptionPane.showMessageDialog(null, "La fecha obtenida es: "+this.Fecha);
     	
		return this.Fecha;
	}
}