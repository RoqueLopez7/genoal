package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Opciones extends JPanel{

	//Componentes
	JPanel Opciones;
	JLabel Titulo, InContrasena, InCorreo;
	JButton CambiarPass, CambiarCorreo, Enviar, Guardar, Regresar, Importar, Exportar;
	JTextField Contrasena, Correo;
	
	ButtonsDesing BD;
	
	public Opciones() {
		
		//Inicio de componentes
		BD = new ButtonsDesing();
		InicioComponentes();
	}
	
	public void InicioComponentes() {
		
		//Panel
		Opciones = new JPanel();
		Opciones.setLayout(null);
		Opciones.setBounds(175,60,785, 500);
		Opciones.setBackground(new Color(245,245,245));
		
		//Componentes del menu de opciones
		Label();
		ButtonsOpciones();
		//Componentes del correo electronico
		Labels();
		Inputs();
		ButtonsCorreo();
	}
	
	public void Label() {
		
		//Titulo
		Titulo = new JLabel();
		Titulo.setForeground(new Color(0,0,0));
		
		Opciones.add(Titulo);
	}
	
	public void ButtonsOpciones() {
		
		//Cambiar Contraseña
		CambiarPass = new JButton();
		CambiarPass.setBounds(150,175, 200, 70);
		
		//Cambiar Correo Electronico
		CambiarCorreo = new JButton();
		CambiarCorreo.setBounds(500,175, 200, 70);
		
		//Cambiar Contraseña
		Importar = new JButton();
		Importar.setBounds(150,325, 200, 70);
				
		//Cambiar Correo Electronico
		Exportar = new JButton();
		Exportar.setBounds(500,325, 200, 70);
		
		//Diseño de los botones
        BD.AsignacionImagen(CambiarPass, "src/img/icons/Buttons/Light Mode/ChangePassword.png", "src/img/icons/Buttons/Light Mode/ChangePasswordHover.png", "src/img/icons/Buttons/Light Mode/ChangePasswordHold.png");
		BD.AsignacionImagen(CambiarCorreo, "src/img/icons/Buttons/Light Mode/ChangeEmail.png", "src/img/icons/Buttons/Light Mode/ChangeEmailHover.png", "src/img/icons/Buttons/Light Mode/ChangeEmailHold.png");
		BD.AsignacionImagen(Importar, "src/img/icons/Buttons/Light Mode/BotonImportar.png", "src/img/icons/Buttons/Light Mode/BotonImportarHover.png", "src/img/icons/Buttons/Light Mode/BotonImportarPress.png");
		BD.AsignacionImagen(Exportar, "src/img/icons/Buttons/Light Mode/BotonExportar.png", "src/img/icons/Buttons/Light Mode/BotonExportarHover.png", "src/img/icons/Buttons/Light Mode/BotonExportarPress.png");

		//Añadiendo al panel
		Opciones.add(CambiarPass);
		Opciones.add(CambiarCorreo);
		Opciones.add(Importar);
		Opciones.add(Exportar);
	}

	public void Labels() {
		
		InContrasena = new JLabel();
		InContrasena.setText("Ingrese la Contraseña");
		InContrasena.setFont(new Font("RussellSquare", 1, 25));
		InContrasena.setBounds(280,150, 300, 75);
		InContrasena.setForeground(new Color(0,0,0));
		InContrasena.setVisible(false);
		
		InCorreo = new JLabel("Ingrese su nuevo correo electronico");
		InCorreo.setFont(new Font("RussellSquare", 1, 22));
		InCorreo.setBounds(215,150, 400, 75);
		InCorreo.setForeground(new Color(0,0,0));
		InCorreo.setVisible(false);
		
		Opciones.add(InContrasena);
		Opciones.add(InCorreo);
	}
	
	public void Inputs() {
		
		Contrasena = new JTextField();
		Contrasena.setBounds(220,210, 350, 25);
		Contrasena.setHorizontalAlignment(0);
		Contrasena.setVisible(false);
		
		Correo = new JTextField();
		Correo.setBounds(220,210, 350, 25);
		Correo.setHorizontalAlignment(0);
		Correo.setVisible(false);
		
		Opciones.add(Contrasena);
		Opciones.add(Correo);
	}
	
	public void ButtonsCorreo() {
		
		//Botones
		Enviar = new JButton();
		Enviar.setBounds(330,250, 130,40);
		Enviar.setVisible(false);
		
		Guardar = new JButton();
		Guardar.setBounds(330,250, 130,40);
		Guardar.setVisible(false);
		
		Regresar = new JButton();
		Regresar.setBounds(25,10, 45, 45);
		Regresar.setVisible(false);
		
		
		//Diseño de los botones
        BD.AsignacionImagen(Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");
		BD.AsignacionImagen(Guardar, "src/img/icons/Buttons/Light Mode/BotonGuardar.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHover.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHold.png");
		BD.AsignacionImagen(Enviar, "src/img/icons/Buttons/Light Mode/ButtonSend.png", "src/img/icons/Buttons/Light Mode/ButtonSendHover.png", "src/img/icons/Buttons/Light Mode/ButtonSendHold.png");

        Opciones.add(Enviar);
		Opciones.add(Guardar);
		Opciones.add(Regresar);
	}
}
