package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MaratonesPrincipal extends JPanel{

	//Componentes
	public JPanel MaratonPrincipal;
	public JLabel Titulo, Imagen;
	public JButton RegistrarMaraton, BuscarMaraton, EditarMaraton, SuspenderMaraton;
	
	ButtonsDesing BD;
	
	public MaratonesPrincipal() {
		
		//Componentes principales para la ventana
        BD = new ButtonsDesing();
		ComponentesPrincipales();
	}

	public void ComponentesPrincipales(){

		MaratonPrincipal = new JPanel();
		MaratonPrincipal.setLayout(null);
		MaratonPrincipal.setBackground(new Color(245,245,245));
		MaratonPrincipal.setBounds(175,60,785, 500);
		
		//Componentes
		Labels();
		Buttons();
	}

	public void Labels(){
		
		//Titulo
		Titulo = new JLabel("MARATONES");
		Titulo.setFont(new Font("RussellSquare", 1, 32));
		Titulo.setBounds(220,5, 330, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		MaratonPrincipal.add(Titulo);
	}

	public void Buttons(){

		//Botones
		RegistrarMaraton = new JButton();
		BuscarMaraton = new JButton();
		EditarMaraton = new JButton();
		SuspenderMaraton = new JButton();
		
		//Caracteristicas de los botones
		RegistrarMaraton.setBounds(525, 90, 200,70);
		BuscarMaraton.setBounds(525, 180, 200,70);
		EditarMaraton.setBounds(525, 270, 200,70);
		SuspenderMaraton.setBounds(525, 360, 200,70);
		
		//Asignacion del diseño de los botones
        BD.AsignacionImagen(RegistrarMaraton, "src/img/icons/Buttons/Light Mode/BotonRegistrar.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarPress.png");
        BD.AsignacionImagen(BuscarMaraton, "src/img/icons/Buttons/Light Mode/BotonBuscar.png", "src/img/icons/Buttons/Light Mode/BotonBuscarHover.png", "src/img/icons/Buttons/Light Mode/BotonBuscarPress.png");
        BD.AsignacionImagen(EditarMaraton, "src/img/icons/Buttons/Light Mode/BotonEditar.png", "src/img/icons/Buttons/Light Mode/BotonEditarHover.png", "src/img/icons/Buttons/Light Mode/BotonEditarPress.png");
        BD.AsignacionImagen(SuspenderMaraton, "src/img/icons/Buttons/Light Mode/BotonBorrar.png", "src/img/icons/Buttons/Light Mode/BotonBorrarHover.png", "src/img/icons/Buttons/Light Mode/BotonBorrarPress.png");
		
		
		//Añadiendo al panel
		MaratonPrincipal.add(RegistrarMaraton);
		MaratonPrincipal.add(BuscarMaraton);
		MaratonPrincipal.add(EditarMaraton);
		MaratonPrincipal.add(SuspenderMaraton);
	}
}
