package Vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.toedter.calendar.JDateChooser;

public class RegistrarEstudiante extends JPanel implements ActionListener{
	
	//Componentes
	public JPanel RegistroEstudiante, PanelContacto;
	public JLabel Titulo, SubTitulo, Nombres,Apellidos,Cedula,Telefono,Asignatura, FechaIngreso, Seccion; //etiquetas de los datos personales
	private JLabel Facebook,Instagram, Correo, Telegram, Twitter, LoL, WoW; //Etiquetas de los contactos
	public JTextField  Nom, Ape, CI, TLF; //Campos de texto para los datos personales
	public JTextField facebook, instagram, correo, telegram, twitter, lol, wow; //Campos de texto para los contactos
	public JButton Siguiente, Atras, Guardar, Regresar;
	public JComboBox Asig, Sec;
	public ImageIcon ImagenNormal, ImagenHover, ImagenHold, Icono;
	
	public Object ComponentesStrings[] = { Nom, Ape, CI, TLF, facebook, instagram, correo, telegram, twitter, lol, wow};
	public JTextField[] CamposTexto = new JTextField[11];
	public JDateChooser Calendario;
	 
	//Atributos
	public String AsignaturaRE[] = {"Asignatura", "Algoritmo 1", "Algoritmo 2"};
	public String year[] = {""}, month[] = {""}, day[] = {""};
	public String secciones[] = {"Secciones", "1","2", "3","4", "5", "6", "7", "8"};
	
	public java.util.Date date; 
	
	Estudiante E;
	ButtonsDesing BD;
	public RegistrarEstudiante() {
		
		BD = new ButtonsDesing();
		InicioPanel();
		PanelContacto();

	}
	
	public void InicioPanel() {
		
		RegistroEstudiante = new JPanel();
		RegistroEstudiante.setLayout(null);
		RegistroEstudiante.setBackground(new Color(245,245,245));
		RegistroEstudiante.setBounds(175,60,785, 500);
				
		Labels();
		Inputs();
		Lists();
		Buttons();
	}
	
	public void Labels() {
		
		//Titulo
		Titulo = new JLabel();
		//Titulo.setText("REGISTRAR UN ESTUDIANTE");
		Titulo.setFont(new Font("RussellSquare", 1, 35));
		Titulo.setBounds(150,5, 500, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		//SubTitulo
		SubTitulo = new JLabel("Ingrese los siguientes datos");
		SubTitulo.setFont(new Font("RussellSquare", 1, 25));
		SubTitulo.setBounds(225,45, 500, 75);
		SubTitulo.setForeground(new Color(0,0,0));
		
		//Añadiendolos a su panel correspondiente
		RegistroEstudiante.add(Titulo);
		RegistroEstudiante.add(SubTitulo);
		
		//------------------>Panel de datos personales<------------------
		
		Nombres = new JLabel("Nombres");
		Nombres.setFont(new Font("RussellSquare", 1, 20));
		Nombres.setBounds(100,110, 500, 75);
		Nombres.setForeground(new Color(0,0,0));
		
		Apellidos = new JLabel("Apellidos");
		Apellidos.setFont(new Font("RussellSquare", 1, 20));
		Apellidos.setBounds(300,110, 500, 75);
		Apellidos.setForeground(new Color(0,0,0));
		
		Cedula = new JLabel("Cedula");
		Cedula.setFont(new Font("RussellSquare", 1, 20));
		Cedula.setBounds(500,110, 500, 75);
		Cedula.setForeground(new Color(0,0,0));
		
		Telefono = new JLabel("Telefono");
		Telefono.setFont(new Font("RussellSquare", 1, 18));
		Telefono.setBounds(100,220, 500, 75);
		Telefono.setForeground(new Color(0,0,0));
		
		Correo = new JLabel("Correo Electronico");
		Correo.setFont(new Font("RussellSquare", 1, 18));
		Correo.setBounds(300,220, 500, 75);
		Correo.setForeground(new Color(0,0,0));
		
		Asignatura = new JLabel("Asignatura");
		Asignatura.setFont(new Font("RussellSquare", 1, 18));
		Asignatura.setBounds(100,330, 500, 75);
		Asignatura.setForeground(new Color(0,0,0));
		
		FechaIngreso = new JLabel("Fecha de Ingreso");
		FechaIngreso.setFont(new Font("RussellSquare", 1, 18));
		FechaIngreso.setBounds(500,220, 500, 75);
		FechaIngreso.setForeground(new Color(0,0,0));
		
		Seccion = new JLabel("Sección");
		Seccion.setFont(new Font("RussellSquare", 1, 18));
		Seccion.setBounds(430, 330, 500, 75);
		Seccion.setForeground(new Color(0,0,0));
		
		//Añadiendoos al panel
		RegistroEstudiante.add(Nombres);
		RegistroEstudiante.add(Apellidos);
		RegistroEstudiante.add(Cedula);
		RegistroEstudiante.add(Telefono);
		RegistroEstudiante.add(Asignatura);
		RegistroEstudiante.add(FechaIngreso);
		RegistroEstudiante.add(Correo);
		RegistroEstudiante.add(Seccion);
	}
	
	public void Inputs() {
		
		//Campos de texto de registrar datos personales
		Nom = new JTextField();
		Nom.setBounds(100,170, 125, 25);
		
		Ape = new JTextField();
		Ape.setBounds(300,170, 125, 25);

		CI = new JTextField();
		CI.setBounds(500,170, 125, 25);

		TLF = new JTextField();
		TLF.setBounds(100,280, 125, 25);
		
		correo = new JTextField();
		correo.setBounds(300,280, 125, 25);
		
		//Se añade al panel
		RegistroEstudiante.add(Nom);
		RegistroEstudiante.add(Ape); 
		RegistroEstudiante.add(CI);
		RegistroEstudiante.add(TLF);
		RegistroEstudiante.add(correo);
	}
	
	public void Lists() {
		
		//Listas desplegables, asignatura y fecha
		Asig = new JComboBox(AsignaturaRE);
		Asig.setBounds(100,390, 125, 25);
		
		//Metodo de fecha
		fecha();
		
		//Seccion
		Sec = new JComboBox(secciones);
		Sec.setBounds(430,390, 125, 25);
		
		RegistroEstudiante.add(Asig);
		RegistroEstudiante.add(Sec);
	}
	
	public void Buttons() {
		
		//Botones
		Siguiente = new JButton();
		Regresar = new JButton();
		
		//Posicion
		Regresar.setBounds(25,10, 45, 45);
		Siguiente.setBounds(610, 380, 120,45);
		
		//Llamando para asignar los botones
		BD.AsignacionImagen(Siguiente, "src/img/icons/Buttons/Light Mode/BotonNext.png", "src/img/icons/Buttons/Light Mode/BotonNextHover.png", "src/img/icons/Buttons/Light Mode/BotonNextHold.png");
        BD.AsignacionImagen(Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");
		
        //Añadiendolos a su panel correspondiente
		RegistroEstudiante.add(Siguiente);
		RegistroEstudiante.add(Regresar);
	}

	public void fecha() {
		
		Calendario = new JDateChooser();
		Calendario.setBounds(500,280, 150, 25);
		RegistroEstudiante.add(Calendario);
		
		/*long d = date.getTime();
		java.sql.Date Fecha = new java.sql. Date(d);*/
	}
	
	public void PanelContacto(){
		
		//Panel
		PanelContacto = new JPanel();
		PanelContacto.setLayout(null);
		PanelContacto.setBackground(new Color(245,245,245));
		PanelContacto.setBounds(175,60,785, 500);
		
		LabelsContacto();
		InputsContacto();
		ButtonsContacto();
	}
	
	public void LabelsContacto() {
		
		//---------------->Etiquetas de Contacto<----------------
		Facebook = new JLabel("Facebook"); 
		Facebook.setFont(new Font("RussellSquare", 1, 20));
		Facebook.setBounds(100,110, 500, 75);
		Facebook.setForeground(new Color(0,0,0));
		
		Instagram = new JLabel("Instagram");
		Instagram.setFont(new Font("RussellSquare", 1, 20));
		Instagram.setBounds(300,110, 500, 75);
		Instagram.setForeground(new Color(0,0,0));
		
		Telegram = new JLabel("Telegram");
		Telegram.setFont(new Font("RussellSquare", 1, 18));
		Telegram.setBounds(500,110, 500, 75);
		Telegram.setForeground(new Color(0,0,0));
		
		Twitter = new JLabel("Twitter");
		Twitter.setFont(new Font("RussellSquare", 1, 18));
		Twitter.setBounds(100,220, 500, 75);
		Twitter.setForeground(new Color(0,0,0));
		
		LoL = new JLabel("League of Legends");
		LoL.setFont(new Font("RussellSquare", 1, 18));
		LoL.setBounds(300,220, 500, 75);
		LoL.setForeground(new Color(0,0,0));
		
		WoW = new JLabel("World of Warcraft");
		WoW.setFont(new Font("RussellSquare", 1, 18));
		WoW.setBounds(500,220, 500, 75);
		WoW.setForeground(new Color(0,0,0));
		
		//Añadiendo al panel
		PanelContacto.add(Facebook);
		PanelContacto.add(Instagram);
		PanelContacto.add(Telegram);
		PanelContacto.add(Twitter);
		PanelContacto.add(LoL);
		PanelContacto.add(WoW);
	}
	
	public void InputsContacto() {
		
		//Campos de contacto
		facebook = new JTextField(); 
		facebook.setBounds(100,170, 125, 25);
		
		instagram = new JTextField(); 
		instagram.setBounds(300,170, 125, 25);

		telegram = new JTextField();
		telegram.setBounds(500,170, 125, 25); 
		
		twitter = new JTextField();
		twitter.setBounds(100,280, 125, 25); 
		
		lol = new JTextField();
		lol.setBounds(300,280, 125, 25); 
		
		wow = new JTextField();
		wow.setBounds(500,280, 125, 25); 
		
		//Enviando al metodo para luego guardarlos por si se quiere editar estos datos del estudiante
		/*GuardarCamposTexto(facebook);
		GuardarCamposTexto(instagram);
		GuardarCamposTexto(twitter);
		GuardarCamposTexto(telegram);
		GuardarCamposTexto(lol);
		GuardarCamposTexto(wow);*/
		
		//Añadiendo al panel
		PanelContacto.add(facebook);
		PanelContacto.add(instagram);
		PanelContacto.add(telegram);  
		PanelContacto.add(twitter);
		PanelContacto.add(lol);
		PanelContacto.add(wow);
	}
	
	public void ButtonsContacto() {
		 
		//Botones y su posicion
		Atras = new JButton();
		Atras.setBounds(75, 380, 120,45);

		Guardar = new JButton();		
		Guardar.setBounds(610, 380, 120,45);

		//Llamando para la asignación de una imagen
		BD.AsignacionImagen(Atras, "src/img/icons/Buttons/Light Mode/BotonBack.png", "src/img/icons/Buttons/Light Mode/BotonBackHover.png", "src/img/icons/Buttons/Light Mode/BotonBackHold.png");
		BD.AsignacionImagen(Guardar, "src/img/icons/Buttons/Light Mode/BotonGuardar.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHover.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHold.png");
		
		//Añadiendolos a su panel correspondiente
		PanelContacto.add(Atras);
		PanelContacto.add(Guardar);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource().equals(Siguiente)) {
			JOptionPane.showMessageDialog(null, "Siguiente panel");
		}
	}
}
