package Vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPanel;

public class Exportar extends JFrame implements ActionListener{

	//Componentes de la ventana
	JPanel Barra, Contenedor;
	JLabel Titulo, Exportar;
	JButton Seleccionar, ExportarArchivo, Cancelar;
	JTextField Campo;
		
	public Exportar() {
		
		//Llamando metodos
		Ventana();
		IniciarComponentes();
		
		//Termina el proceso al darle a la equis
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void Ventana() {
		
		//Caracteristicas de las ventanas
		this.setLayout(null); //Colocando la posición libre
		this.setBounds(0, 0, 450, 300); //Tamaño de la ventana
		//this.setTitle("Exportar Base de Datos"); //Titulo de la ventana
		this.setLocationRelativeTo(null);
		this.setResizable(false); //No se puede cambiar el tamaño de la ventana
		//this.setBackground(new Color(245,245,245));
		this.setUndecorated(true);
		this.setVisible(true); //Activando la visibilidad
	}
	
	public void IniciarComponentes() {
		
		//Llamando a los metodos que contienen sus componentes
		Panels();
		Labels();
		Buttons();
		TextField();
	}
	

	//------------------>Componentes de la ventana<------------------
	
	public void Panels() {
		
		//declarando los paneles
		//Panel del titulo
		Barra = new JPanel();
		Barra.setBounds(0, 0, 450, 50);
		Barra.setBackground(new Color(0,115,217));
		Barra.setLayout(null);
		Barra.setBorder(BorderFactory.createLineBorder(new Color(0,0,0,50), 2));
		
		add(Barra);
		
		//Panel que tendrá los componentes para la exportación
		Contenedor = new JPanel();
		Contenedor.setBounds(0, 50, 450, 250);
		Contenedor.setBackground(new Color(245,245,245));
		Contenedor.setLayout(null);
		Contenedor.setBorder(BorderFactory.createLineBorder(new Color(0,0,0,50), 2));
		
		add(Contenedor);
	}
	
	public void Labels() {
		
		//subtitulo
		Exportar = new JLabel("Destino donde se guardará el respaldo");
		Exportar.setFont(new Font("RussellSquare", 1, 19));
		Exportar.setBounds(60, 13, 400, 30);
		Exportar.setForeground(new Color(0,0,0));
		
		Contenedor.add(Exportar);
		
		//Titulo de la ventana
		Titulo = new JLabel("EXPORTAR RESPALDO DE LA BASE DE DATOS");
		Titulo.setFont(new Font("RussellSquare", 1, 18));
		Titulo.setBounds(32, 12, 400, 30);
		Titulo.setForeground(new Color(255, 255, 255));
		
		Barra.add(Titulo);
	}
	
	public void Buttons() {
		
		//Boton para seleccionar el destino donde se guardará el respaldo
		Seleccionar = new JButton("Seleccionar Destino");
		Seleccionar.setBounds(250, 75, 150, 40);
		Seleccionar.addActionListener(this);
		
		Contenedor.add(Seleccionar);
		
		//Boton para exportar
		ExportarArchivo = new JButton("Exportar");
		ExportarArchivo.setBounds(140, 165, 150, 50);
		ExportarArchivo.addActionListener(this);
		
		Contenedor.add(ExportarArchivo);
		
		//Boton para regresar
		Cancelar = new JButton();
		Cancelar.setBounds(10, 10, 35, 35);
		
		Contenedor.add(Cancelar);
	}
	
	public void TextField() {
		
		//campo de texto donde se verá la dirección donde se guardará  el archivo
		Campo = new JTextField();
		Campo.setBounds(40, 80, 160, 30);
		//Campo.setEditable(false);
		
		Contenedor.add(Campo);
	}

	
	//------------------>Acciones de los botones<------------------

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
