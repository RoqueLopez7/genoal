package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Bienvenida extends JPanel{

	//Componentes
	public JPanel Bienvenida;
	JLabel MensajeBienvenido;
	
	public Bienvenida() {
		
		//Inicio de los componentes
		InicioPanel();
	}
	
	public void InicioPanel(){
		
		//Inicio del panel de bienvenida
		Bienvenida = new JPanel();
		Bienvenida.setBackground(new Color(245,245,245));
		Bienvenida.setBounds(175,60,785, 500); 
		Bienvenida.setLayout(null);

		//Componentes para el panel
		Labels();
	}
	
	public void Labels() {
		
		//Bienvenido
		MensajeBienvenido = new JLabel("BIENVENIDO");
		MensajeBienvenido.setFont(new Font("RussellSquare", 1, 40));
		MensajeBienvenido.setBounds(290,5, 300, 75);
		MensajeBienvenido.setForeground(new Color(0,0,0));
		
		Bienvenida.add(MensajeBienvenido);
	}
}
