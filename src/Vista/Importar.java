package Vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPanel;

public class Importar extends JFrame implements ActionListener{

	//Componentes de la ventana
	JPanel Barra, Contenedor;
	JLabel Titulo, Importar;
	JButton Seleccionar, ImportarArchivo, Cancelar;
	JTextField Campo;
		
	public Importar() {
		
		//Llamando metodos
		Ventana();
		IniciarComponentes();
		
		//Termina el proceso al darle a la equis
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void Ventana() {
		
		//Caracteristicas de las ventanas
		this.setLayout(null); //Colocando la posición libre
		this.setBounds(0, 0, 450, 300); //Tamaño de la ventana
		//this.setTitle("Exportar Base de Datos"); //Titulo de la ventana
		this.setLocationRelativeTo(null);
		this.setResizable(false); //No se puede cambiar el tamaño de la ventana
		//this.setBackground(new Color(245,245,245));
		this.setUndecorated(true);
		this.setVisible(true); //Activando la visibilidad
	}
	
	public void IniciarComponentes() {
		
		//Llamando a los metodos que contienen sus componentes
		Panels();
		Labels();
		Buttons();
		TextField();
	}
	

	//------------------>Componentes de la ventana<------------------
	
	public void Panels() {
		
		//declarando los paneles
		//Panel del titulo
		Barra = new JPanel();
		Barra.setBounds(0, 0, 450, 50);
		Barra.setBackground(new Color(0,115,217));
		Barra.setLayout(null);
		Barra.setBorder(BorderFactory.createLineBorder(new Color(0,0,0,50), 2));
		
		add(Barra);
		
		//Panel que tendrá los componentes para la exportación
		Contenedor = new JPanel();
		Contenedor.setBounds(0, 50, 450, 250);
		Contenedor.setBackground(new Color(245,245,245));
		Contenedor.setLayout(null);
		Contenedor.setBorder(BorderFactory.createLineBorder(new Color(0,0,0,50), 2));
		
		add(Contenedor);
	}
	
	public void Labels() {
		
		//subtitulo
		Importar = new JLabel("Destino del respaldo");
		Importar.setFont(new Font("RussellSquare", 1, 19));
		Importar.setBounds(60, 13, 400, 30);
		Importar.setForeground(new Color(0,0,0));
		
		Contenedor.add(Importar);
		
		//Titulo de la ventana
		Titulo = new JLabel("IMPORTAR RESPALDO DE LA BASE DE DATOS");
		Titulo.setFont(new Font("RussellSquare", 1, 18));
		Titulo.setBounds(32, 12, 400, 30);
		Titulo.setForeground(new Color(255, 255, 255));
		
		Barra.add(Titulo);
	}
	
	public void Buttons() {
		
		//Boton para seleccionar el destino donde se guardará el respaldo
		Seleccionar = new JButton("Seleccionar Destino");
		Seleccionar.setBounds(250, 75, 150, 40);
		Seleccionar.addActionListener(this);
		
		Contenedor.add(Seleccionar);
		
		//Boton para exportar
		ImportarArchivo = new JButton("Importar");
		ImportarArchivo.setBounds(140, 165, 150, 50);
		ImportarArchivo.addActionListener(this);
		
		Contenedor.add(ImportarArchivo);
		
		//Boton para regresar
		Cancelar = new JButton();
		Cancelar.setBounds(10, 10, 35, 35);
		
		Contenedor.add(Cancelar);
	}
	
	public void TextField() {
		
		//campo de texto donde se verá la dirección donde se guardará  el archivo
		Campo = new JTextField();
		Campo.setBounds(40, 80, 160, 30);
		//Campo.setEditable(false);
		
		Contenedor.add(Campo);
	}

	
	//------------------>Acciones de los botones<------------------

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
