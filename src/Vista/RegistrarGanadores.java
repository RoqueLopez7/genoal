package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class RegistrarGanadores extends JPanel{

	//Componentes
	public JPanel RegistrarGanadores;
	public JLabel Titulo, PrimerLugar, SegundoLugar, TercerLugar, Datosestudiante, UnidadPonderar;
	public JComboBox Primer, Segundo, Tercer, IdEstudiantes, UnidadPremio1, UnidadPremio2, UnidadPremio3;
	public JButton Registrar, Regresar;
	public JTextArea DatosEstudiante;
	public JScrollPane scroll;
	
	//Atributos
	public String PrimerL[] = {""};
	public String SegundoL[] = {""};
	public String TercerL[] = 	{""};
	
	ButtonsDesing BD;
	
	public RegistrarGanadores() {
	
		//Componentes principales para la ventana
		ComponentesPrincipales();
	}

	public void ComponentesPrincipales(){

		RegistrarGanadores = new JPanel();
		RegistrarGanadores.setLayout(null);
		RegistrarGanadores.setBackground(new Color(245,245,245));
		RegistrarGanadores.setBounds(175,60,785, 500);
		
		//Componentes
		Labels();
		Lists();
		Buttons();
		TextArea();
	}

	public void Labels(){
		
		//Titulo
		Titulo = new JLabel();
		Titulo.setFont(new Font("RussellSquare", 1, 32));
		Titulo.setBounds(220,5, 450, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		//PrimerLugar
		PrimerLugar = new JLabel("Primer Lugar");
		PrimerLugar.setFont(new Font("RussellSquare", 1, 18));
		PrimerLugar.setBounds(110,100, 300, 75);
		PrimerLugar.setForeground(new Color(0,0,0));
		
		//SegundoLugar
		SegundoLugar = new JLabel("Segundo Lugar");
		SegundoLugar.setFont(new Font("RussellSquare", 1, 18));
		SegundoLugar.setBounds(110,200, 300, 75);
		SegundoLugar.setForeground(new Color(0,0,0));
		
		//TercerLugar
		TercerLugar = new JLabel("Tercer Lugar");
		TercerLugar.setFont(new Font("RussellSquare", 1, 18));
		TercerLugar.setBounds(110,300, 300, 75);
		TercerLugar.setForeground(new Color(0,0,0));
				
		//Datos del estudiante
		Datosestudiante = new JLabel("Datos del Estudiante");
		Datosestudiante.setFont(new Font("RussellSquare", 1, 18));
		Datosestudiante.setBounds(500,100, 300, 75);
		Datosestudiante.setForeground(new Color(0,0,0));
		
		//Datos del estudiante
		UnidadPonderar = new JLabel("Unidad a Ponderar");
		UnidadPonderar.setFont(new Font("RussellSquare", 1, 16));
		UnidadPonderar.setBounds(320,100, 300, 75);
		UnidadPonderar.setForeground(new Color(0,0,0));
		
		//Añadiendo al panel
		RegistrarGanadores.add(Titulo);
		RegistrarGanadores.add(PrimerLugar);
		RegistrarGanadores.add(SegundoLugar);
		RegistrarGanadores.add(TercerLugar);
		RegistrarGanadores.add(Datosestudiante);
		RegistrarGanadores.add(UnidadPonderar);
	}

	public void Lists() {
		
		//Primer Lugar
		Primer = new JComboBox();
		Primer.setBounds(110,150, 200, 25);
		
		//Segundo
		Segundo = new JComboBox();
		Segundo.setBounds(110,250, 200, 25);
		Segundo.setEnabled(false);
		
		//Tercer
		Tercer = new JComboBox();
		Tercer.setBounds(110,350, 200, 25);
		Tercer.setEnabled(false);
		
		//Lista necesaria para la base de datos
		IdEstudiantes = new JComboBox();
		IdEstudiantes.setBounds(30, 20, 10, 35);
		IdEstudiantes.setVisible(false);
		
		//Unidad en la que se agregará el premio de 50, 30 o 20 puntos
		UnidadPremio1 = new JComboBox();
		UnidadPremio1.setBounds(330,150, 75, 25);
		
		UnidadPremio2 = new JComboBox();
		UnidadPremio2.setBounds(330,250, 75, 25);
		UnidadPremio2.setEnabled(false);
		
		UnidadPremio3 = new JComboBox();
		UnidadPremio3.setBounds(330,350, 75, 25);
		UnidadPremio3.setEnabled(false);
		
		//Añadiendo al panel
		RegistrarGanadores.add(Primer);
		RegistrarGanadores.add(Segundo);
		RegistrarGanadores.add(Tercer);
		RegistrarGanadores.add(UnidadPremio1);
		RegistrarGanadores.add(UnidadPremio2);
		RegistrarGanadores.add(UnidadPremio3);
	}
	
	public void Buttons() {
		
		//Botones
		Registrar = new JButton();
		Registrar.setBounds(610, 380, 120,45);
		Registrar.setEnabled(false);
		
		Regresar = new JButton();
		Regresar.setBounds(25,10, 45, 45);
		
		RegistrarGanadores.add(Registrar);
		RegistrarGanadores.add(Regresar);
	}
	
	public void TextArea() {
		
		//Resultados
		DatosEstudiante = new JTextArea();
		DatosEstudiante.setBounds(450, 155, 300, 200);
		DatosEstudiante.setEditable(false);
		RegistrarGanadores.add(DatosEstudiante);
		
		//Agregando una barra de scroll como tanto vertical y horizontal
		scroll = new JScrollPane(DatosEstudiante);
		scroll.setBounds(450, 155, 300, 200);
		RegistrarGanadores.add(scroll);
	}
	
}
