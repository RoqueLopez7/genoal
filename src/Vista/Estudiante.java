package Vista;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
//import java.awt.event.KeyEvent;
//import java.awt.event.KeyListener;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import Controlador.Controlador;

public class Estudiante extends JPanel /*implements ActionListener*/{

	//Componentes
	public JPanel Estudiante;
	private JLabel Titulo, ImagenEstudiante;
	public JButton RegistrarEstudiante, BuscarEstudiante, EditarEstudiante, BorrarEstudiante;
	
	Vista V;
	ButtonsDesing BD;
	ImageIcon Icono, ImagenNormal, ImagenHover, ImagenHold;
	
    public Estudiante(){

		//Componentes principales para la ventana
		BD = new ButtonsDesing();
		ComponentesPrincipales();
		
	}

	public void ComponentesPrincipales(){

		Estudiante = new JPanel();
		Estudiante.setLayout(null);
		Estudiante.setBackground(new Color(245,245,245));
		Estudiante.setBounds(175,60,785, 500);
		
		//Componentes
		Labels();
		Buttons();
	}

	public void Labels(){
		
		//Titulo
		Titulo = new JLabel("ESTUDIANTE");
		Titulo.setFont(new Font("RussellSquare", 1, 40));
		Titulo.setBounds(290,5, 300, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		Estudiante.add(Titulo);
	}

	public void Buttons(){

		//Botones
		RegistrarEstudiante = new JButton();
		BuscarEstudiante = new JButton();
		EditarEstudiante = new JButton();
		BorrarEstudiante = new JButton();
		
		//Caracteristicas de los botones
		RegistrarEstudiante.setBounds(525, 90, 200,65);
		BuscarEstudiante.setBounds(525, 180, 200,65);
		EditarEstudiante.setBounds(525, 270, 200,65);
		BorrarEstudiante.setBounds(525, 360, 200,65);
		
        //Enviando datos al metodo
        BD.AsignacionImagen(RegistrarEstudiante, "src/img/icons/Buttons/Light Mode/BotonRegistrar.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarPress.png");
        BD.AsignacionImagen(BuscarEstudiante, "src/img/icons/Buttons/Light Mode/BotonBuscar.png", "src/img/icons/Buttons/Light Mode/BotonBuscarHover.png", "src/img/icons/Buttons/Light Mode/BotonBuscarPress.png");
        BD.AsignacionImagen(EditarEstudiante, "src/img/icons/Buttons/Light Mode/BotonEditar.png", "src/img/icons/Buttons/Light Mode/BotonEditarHover.png", "src/img/icons/Buttons/Light Mode/BotonEditarPress.png");
        BD.AsignacionImagen(BorrarEstudiante, "src/img/icons/Buttons/Light Mode/BotonBorrar.png", "src/img/icons/Buttons/Light Mode/BotonBorrarHover.png", "src/img/icons/Buttons/Light Mode/BotonBorrarPress.png");
		
		//Añadiendo al panel
		Estudiante.add(RegistrarEstudiante);
		Estudiante.add(BuscarEstudiante);
		Estudiante.add(EditarEstudiante);
		Estudiante.add(BorrarEstudiante);
	}
}