package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CambiarContrasena extends JPanel{

	//Componentes
	public JPanel CambiarContrasena;
	public JLabel Titulo, ChangePass, InContrasena,InContrasenaNueva;
	public JButton CambiarPass, CambiarCorreo, Enviar, Guardar, Regresar;
	public JTextField Contrasena, NuevaContrasena;
	
	ButtonsDesing BD;
	public CambiarContrasena() {
		
		//Inicio de componentes
		BD = new ButtonsDesing();
		InicioComponentes();
	}
	
	public void InicioComponentes() {
		
		CambiarContrasena = new JPanel();
		CambiarContrasena.setLayout(null);
		CambiarContrasena.setBackground(new Color(245,245,245));//245
		CambiarContrasena.setBounds(175,60,785, 500);
		
		//Componentes
		Labels();
		Inputs();
		ButtonsContrasena();
	}
	
	public void Labels() {
		
		//Titulo de Cambiar Contraseña
		ChangePass = new JLabel();
		ChangePass.setText("Cambiar Contraseña"); //Texto del label
		ChangePass.setFont(new Font("RussellSquare", 1, 40)); //Fuente de texto
		ChangePass.setBounds(210,5, 500, 75); //Posicion y tamaño
		ChangePass.setForeground(new Color(0,0,0)); //Color de texto
		ChangePass.setVisible(true); //Visibilidad
		
		//Subtitulo 1, para verificar que es el usuario principal
		InContrasena = new JLabel();
		InContrasena.setText("Ingrese la Contraseña");
		InContrasena.setFont(new Font("RussellSquare", 1, 25));
		InContrasena.setBounds(280,150, 300, 75);
		InContrasena.setForeground(new Color(0,0,0));
		InContrasena.setVisible(true);
		
		//Subtitulo 2, para cambiar la contraseña
		InContrasenaNueva = new JLabel("Ingrese su nueva contraseña");
		InContrasenaNueva.setFont(new Font("RussellSquare", 1, 22));
		InContrasenaNueva.setBounds(250,150, 400, 75);
		InContrasenaNueva.setForeground(new Color(0,0,0));
		InContrasenaNueva.setVisible(false);
		
		//Agregar los componentes al panel
		CambiarContrasena.add(InContrasena);
		CambiarContrasena.add(InContrasenaNueva);
		CambiarContrasena.add(ChangePass);
	}
	
	public void Inputs() {
		
		//Input para la contraseña de verificacion
		Contrasena = new JTextField();
		Contrasena.setBounds(220,210, 350, 25);
		Contrasena.setHorizontalAlignment(0);
		Contrasena.setVisible(true);
		
		//Input para la nueva contraseña
		NuevaContrasena = new JTextField();
		NuevaContrasena.setBounds(220,210, 350, 25);
		NuevaContrasena.setHorizontalAlignment(0);
		NuevaContrasena.setVisible(true);
		
		//Agregar componentes al panel
		CambiarContrasena.add(Contrasena);
		CambiarContrasena.add(NuevaContrasena);
	}
	
	public void ButtonsContrasena() {
		
		//Boton de enviar lo escrito en el input
		Enviar = new JButton();
		Enviar.setBounds(330,250, 130,40);
		Enviar.setVisible(true);
		
		//Boton de guardar la nueva contraseña
		Guardar = new JButton();
		Guardar.setBounds(330,250, 130,40);
		Guardar.setVisible(false);
		
		//Boton para regresar al menu de opciones
		Regresar = new JButton();
		Regresar.setBounds(25,10, 45, 45);
		Regresar.setVisible(false);
		
		//Diseño de los botones
        BD.AsignacionImagen(Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");
		BD.AsignacionImagen(Guardar, "src/img/icons/Buttons/Light Mode/BotonGuardar.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHover.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHold.png");
		BD.AsignacionImagen(Enviar, "src/img/icons/Buttons/Light Mode/ButtonSend.png", "src/img/icons/Buttons/Light Mode/ButtonSendHover.png", "src/img/icons/Buttons/Light Mode/ButtonSendHold.png");

		//Agregar componentes al panel
		CambiarContrasena.add(Enviar);
		CambiarContrasena.add(Guardar);
		CambiarContrasena.add(Regresar);
	}
}
