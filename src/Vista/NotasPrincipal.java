package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class NotasPrincipal extends JPanel {
	
	//Componentes
	public JPanel NotasPrin;
	public JLabel Titulo, ImagenNotas;
	public JButton RegistrarNota, BuscarNota, EditarNota, BorrarNota;
	
	ButtonsDesing BD;
    public NotasPrincipal(){

		//Componentes principales para la ventana
    	BD = new ButtonsDesing();
		ComponentesPrincipales();
	}

	public void ComponentesPrincipales(){

		NotasPrin = new JPanel();
		NotasPrin.setLayout(null);
		NotasPrin.setBackground(new Color(245,245,245));
		NotasPrin.setBounds(175,60,785, 500);
		
		//Componentes
		Labels();
		Buttons();
	}

	public void Labels(){
		
		//Titulo
		Titulo = new JLabel();
		Titulo.setFont(new Font("RussellSquare", 1, 32));
		Titulo.setBounds(220,5, 330, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		NotasPrin.add(Titulo);
	}

	public void Buttons(){

		//Botones
		RegistrarNota = new JButton();
		BuscarNota = new JButton();
		EditarNota = new JButton();
		BorrarNota = new JButton();
		
		//Caracteristicas de los botones
		RegistrarNota.setBounds(525, 90, 200,65);
		BuscarNota.setBounds(525, 180, 200,65);
		EditarNota.setBounds(525, 270, 200,65);
		BorrarNota.setBounds(525, 360, 200,65);
		
		//Asignacion del diseño de los botones
        BD.AsignacionImagen(RegistrarNota, "src/img/icons/Buttons/Light Mode/BotonRegistrar.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegistrarPress.png");
        BD.AsignacionImagen(BuscarNota, "src/img/icons/Buttons/Light Mode/BotonBuscar.png", "src/img/icons/Buttons/Light Mode/BotonBuscarHover.png", "src/img/icons/Buttons/Light Mode/BotonBuscarPress.png");
        BD.AsignacionImagen(EditarNota, "src/img/icons/Buttons/Light Mode/BotonEditar.png", "src/img/icons/Buttons/Light Mode/BotonEditarHover.png", "src/img/icons/Buttons/Light Mode/BotonEditarPress.png");
        BD.AsignacionImagen(BorrarNota, "src/img/icons/Buttons/Light Mode/BotonBorrar.png", "src/img/icons/Buttons/Light Mode/BotonBorrarHover.png", "src/img/icons/Buttons/Light Mode/BotonBorrarPress.png");
			
		//Añadiendo al panel
		NotasPrin.add(RegistrarNota);
		NotasPrin.add(BuscarNota);
		NotasPrin.add(EditarNota);
		NotasPrin.add(BorrarNota);
	}
}
