package Vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class IngresarNota extends JPanel{

	//Componentes
	public JPanel InNota;
	public JLabel Titulo, Unidad, Seccion, Estudiante, IngreseNota;	
	public JTextArea NotasEstudiante;
	public JComboBox Sec, Uni, Estu, IdEstudiantes, Notas;
	public JButton Guardar, Regresar, Editar, Borrar, BuscarNota;
	public JScrollPane scroll;
	
	//Atributos
	public String secciones[] = {"Secciones","1","2","3","4","5","6","7","8"}, unidades[] = {"Unidades","1","2","3","4","5"}, estudiantes[] = { "Estudiante"}, NotasPosibles[] = {" "};
	
	ButtonsDesing BD;
	
	public IngresarNota() {
		
		//Inicio del panel
		BD = new ButtonsDesing();
		InicioPanel();
	}
	
	public void InicioPanel(){
		
		//Declaracion
		InNota = new JPanel();
		InNota.setLayout(null);
		InNota.setBackground(new Color(245,245,245));
		InNota.setBounds(175,60,785, 500);
		
		//llamado a los componentes
		Labels();
		Inputs();
		Lists();
		Buttons();
	}
	
	public void Labels() {
		
		//Titulo
		Titulo = new JLabel();
		Titulo.setFont(new Font("RussellSquare", 1, 40));
		Titulo.setBounds(230,5, 400, 75);
		Titulo.setForeground(new Color(0,0,0));
		
		//Seccion
		Seccion = new JLabel("Seccion");
		Seccion.setFont(new Font("RussellSquare", 1, 18));
		Seccion.setBounds(110,100, 300, 75);
		Seccion.setForeground(new Color(0,0,0));
		
		//Unidad
		Unidad = new JLabel("Unidad");
		Unidad.setFont(new Font("RussellSquare", 1, 18));
		Unidad.setBounds(330,100, 300, 75);
		Unidad.setForeground(new Color(0,0,0));
		
		//Estudiante
		Estudiante = new JLabel("Estudiante");
		Estudiante.setFont(new Font("RussellSquare", 1, 18));
		Estudiante.setBounds(550,100, 300, 75);
		Estudiante.setForeground(new Color(0,0,0));
		
		//Ingrese Nota
		IngreseNota = new JLabel("Ingrese la nota del estudiante");
		IngreseNota.setFont(new Font("RussellSquare", 1, 18));
		IngreseNota.setBounds(400,198, 300, 75);
		IngreseNota.setForeground(new Color(0,0,0));
		IngreseNota.setVisible(false);
		
		//Añadiendo al panel
		InNota.add(Titulo);
		InNota.add(Seccion);
		InNota.add(Unidad);
		InNota.add(Estudiante);
		InNota.add(IngreseNota);
	}
	
	public void Inputs() {
		
		//Nota del Estudiante
		NotasEstudiante = new JTextArea();
		NotasEstudiante.setBounds(110, 230, 250, 150);
		NotasEstudiante.setBackground(Color.WHITE);
		InNota.add(NotasEstudiante);
		
		//Agregando una barra de scroll como tanto vertical y horizontal
		scroll = new JScrollPane(NotasEstudiante);
		scroll.setBounds(110,230, 250, 150);
		InNota.add(scroll);
	}
	
	public void Lists() {
		
		//Declaracion, tamaño y posicion de las listas
		Sec = new JComboBox(secciones);
		Sec.setBounds(110,150, 125, 25);
		
		Uni = new JComboBox(unidades);
		Uni.setBounds(330,150, 125, 25);
		
		Estu = new JComboBox(estudiantes);
		Estu.setBounds(550,150, 200, 25);
		
		Notas = new JComboBox();
		Notas.setBounds(400, 260, 115, 25);
		for(int i = 0; i <= 100; i++) { Notas.addItem(i); } //Lenado de la lista
		Notas.setVisible(false);
		
		//Lista necesaria para la base de datos
		IdEstudiantes = new JComboBox();
		IdEstudiantes.setBounds(30, 20, 10, 35);
		//for(int i = 0; i <= 500; i++) { IdEstudiantes.addItem(0); } //Lenado de la lista
		IdEstudiantes.setVisible(false);
		
		//Añadiendo al panel
		InNota.add(Sec);
		InNota.add(Uni);
		InNota.add(Estu);
		InNota.add(Notas);
	}

	public void Buttons(){
		
		//Guardar
		Guardar = new JButton();
		Guardar.setBounds(610, 375, 120,45);
		
		//Guardar
		BuscarNota = new JButton();
		BuscarNota.setBounds(610, 320, 120,45);
		
		//Regresar
		Regresar = new JButton();
		Regresar.setBounds(25,10, 45, 45);
		
		//Editar
		Editar = new JButton();
		Editar.setBounds(380, 280, 120,35);
		
		//Borrar
		Borrar = new JButton();
		Borrar.setBounds(530, 280, 120, 35);

		//Diseño de los botones
        BD.AsignacionImagen(Regresar, "src/img/icons/Buttons/Light Mode/BotonRegresar.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHover.png", "src/img/icons/Buttons/Light Mode/BotonRegresarHold.png");
		BD.AsignacionImagen(Guardar, "src/img/icons/Buttons/Light Mode/BotonGuardar.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHover.png", "src/img/icons/Buttons/Light Mode/BotonGuardarHold.png");
        BD.AsignacionImagen(Editar, "src/img/icons/Buttons/Light Mode/BotonEditar.png", "src/img/icons/Buttons/Light Mode/BotonEditarHover.png", "src/img/icons/Buttons/Light Mode/BotonEditarPress.png");
        BD.AsignacionImagen(Borrar, "src/img/icons/Buttons/Light Mode/BotonBorrar.png", "src/img/icons/Buttons/Light Mode/BotonBorrarHover.png", "src/img/icons/Buttons/Light Mode/BotonBorrarPress.png");
		BD.AsignacionImagen(BuscarNota, "src/img/icons/Buttons/Light Mode/BotonBuscar.png", "src/img/icons/Buttons/Light Mode/BotonBuscarHover.png", "src/img/icons/Buttons/Light Mode/BotonBuscarPress.png");
        
		//Añaden al panel
		InNota.add(Guardar);
		InNota.add(Regresar);
		InNota.add(Editar);
		InNota.add(Borrar);
		InNota.add(BuscarNota);
	}
}
