package Vista;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class ButtonsDesing {
	
	ImageIcon ImagenNormal, ImagenHover, ImagenHold, Icono;
	
	public void AsignacionImagen(JButton Boton, String NormalImage, String HoverImage, String HoldImage){
		//Añandiendole la textura al boton
        ImagenNormal = new ImageIcon(NormalImage); //Imagen por defecto
		ImagenHover = new ImageIcon(HoverImage);   //Imagen cuando se pase el mouse por encima 
		ImagenHold = new ImageIcon(HoldImage);     //Imagen presionado
		this.Botones(Boton, ImagenNormal, ImagenHover, ImagenHold); //Llamada del metodo
	}
	
    public void Botones(JButton Boton,ImageIcon BotonNormal, ImageIcon BotonHover, ImageIcon BotonHold) {
		
		//Dandole valor al icono
		Icono = new ImageIcon(BotonNormal.getImage().getScaledInstance(Boton.getWidth(), 
				Boton.getHeight(), Image.SCALE_DEFAULT));
		Boton.setIcon(Icono);
		Boton.setOpaque(false);
		Boton.setContentAreaFilled(false);
		Boton.setBorderPainted(false);	
		
		MouseListener OyentedeRatonAtras = new MouseListener() {
			
			public void mouseClicked(MouseEvent e) {	

			}

			public void mouseEntered(MouseEvent e) {

					Icono = new ImageIcon(BotonHover.getImage().getScaledInstance(Boton.getWidth(), 
							Boton.getHeight(), Image.SCALE_DEFAULT));
						Boton.setIcon(Icono); //
						Boton.setCursor(new Cursor(Cursor.HAND_CURSOR));

			}

				public void mouseExited(MouseEvent e) {
                    
                    //para que los botones vuelvan a su color natural

						Icono = new ImageIcon(BotonNormal.getImage().getScaledInstance(Boton.getWidth(), 
								Boton.getHeight(), Image.SCALE_DEFAULT));
						Boton.setIcon(Icono);	
				}

				public void mousePressed(MouseEvent e) {
					
					 //Declarando el fondo blanco al boton presionado
					   Icono = new ImageIcon(BotonHold.getImage().getScaledInstance(Boton.getWidth(), 
					        Boton.getHeight(), Image.SCALE_DEFAULT));
					   Boton.setIcon(Icono); //Estableciendo el icono al boton
					   Boton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); //Estableciendo el cursor por defecto al boton presionado   
				}

				public void mouseReleased(MouseEvent e) {
                    //para que los botones vuelvan a su color natural

						Icono = new ImageIcon(BotonNormal.getImage().getScaledInstance(Boton.getWidth(), 
								Boton.getHeight(), Image.SCALE_DEFAULT));
						Boton.setIcon(Icono);	
				}
					
			};
				
			Boton.addMouseListener(OyentedeRatonAtras);
	}
}
