package Vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import javax.swing.Timer;

import Principal.Principal;

public class Cargando extends JFrame{

	JButton Boton; 
	JProgressBar Barra;
	JToggleButton BotonON;
	
	//Ventana de la vista principal
	Vista V;
	
	public Cargando() {
		
		//Caracteristicas de la ventana
		CaracteristicasVentana();
		
		//Demas componentes
		Componentes();
		VentanaCarga();
	}
	
	
	//------------------>Caracteristicas de la Ventana<------------------
		public void CaracteristicasVentana() {
			
			//Ventana principal
			this.setLayout(null); //Se establece el layout nulo
			this.setVisible(false); //Se vuelve visible
			this.setBounds(0,0, 550, 300); //Posicion y tamaño
			//this.setTitle("GENOAL"); //Se le añade un titulo al programa
			this.setLocationRelativeTo(null); //Se coloca la ventana en el medio 
			this.setResizable(false); //Se le pone una restriccion para que no cambie el tamaño de la pantalla
			this.setUndecorated(true);
			this.setVisible(true); //Se vuelve visible
		}
	
	//------------------>Splash Screen/Ventana de carga<------------------
	public void VentanaCarga() {		
		
		//Iniciando la ventana principal
		V = new Vista();
		
		if(Barra.getValue() < 100) {
			Barra.setValue(Barra.getValue() + V.Incrementador);
		}
		
		if(V.Incrementador == 100){
			this.dispose();
			V.setVisible(true);
		}
	}

	public void Componentes() {
		
		//Barra de progreso
		Barra = new JProgressBar();
		Barra.setLayout(null);
		Barra.setBounds(75, 200, 400, 30);
		Barra.setMaximum(100);
		Barra.setMinimum(0);
		
		//Añadiendolo a la ventana
		add(Barra);
		
//		BotonON = new JToggleButton("ON");
//		BotonON.setBounds(200, 25, 100, 50);
//		//BotonON.addActionListener(this);
//		
//		add(BotonON);
	}
}
