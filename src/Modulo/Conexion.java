package Modulo;
//Librerias necesarias para ejecutar comandos MySQL
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import Vista.RegistrarEstudiante;
//Conexion a otras clases
import Vista.Vista;

public class Conexion {
	
	//Caracteristicas necesarias para conectar con la base de datos
		Connection conexion; //Conectar con la base de datos
		Statement stt; //Para el use de la base de datos
		PreparedStatement PSControl_Estudiante = null, PSEstudiante = null, PSAsignatura = null, PSContacto = null,  Mistatement; //Para las instruccion SQL
		PreparedStatement PSMaraton = null, PSNotas_A1 = null, PSNotas_A2 = null, PSSeguridad = null;
		
		public Vista Ventana;//Conectando a la vista principal
		RegistrarEstudiante RE; //Conectando a la vista de Registrar estudiante.
		
		//Atributos necesarios para el metodo CRUD
		public String Datos, columnaNotas, Resultado, ResultadoFinal = "-";
		public int cedula, cedulaBor, N, seccion; //CedulaBor es una variable exclusiva para el segmento borrar y N es para llenar la tabla de notas, seccion es para enviarlo a registrar estudiante.
		public int idEstudiante = 0, idContacto = 0, idAsignatura = 0, idNotas = 0; //Variables para tomar los id de dichas tablas
		private ResultSet Estudiante, Contacto, Asignatura, Notas_A1, Notas_A2; //Un ResultSet para cada tabla
		
		//Vectores para tener los datos del estudiante al buscarlo
		public String DatosPersonales[] = { "Hola", "","",""};
		public String ContactoEstudiante[] = { "hola", " " , "" , "","" , "" , "", ""};
		public int DE2[] = { 0, 0, 0, 0, 0 }; //Datos del estudiante enteros
		public int AsignaturaYFecha[] = { 0, 0 , 0, 0 };

		int ayuda = 0; //DE PRUEBA
		
		//Variable para la clave
		public String Clave = "";
		
		//Variables para el funcionamiento de exportar
		public static String usuario = "root";
		public static String password = "";
		public static String BD = "genoal";
		
		public Conexion() {

		}
		
		//Metodo para la insercion
		public void Insercion(Modulo Mode) {
            //Ingresar datos de un estudiante
			String InStudent = "INSERT INTO estudiante (`nombres`, `apellidos`, `cedula`, `id_contacto`, `Fecha_Ingreso`, `Seccion`, `id_asignatura`) VALUES (?, ?, ?, ?, ?, ?, ?);";
			String InContact = "INSERT INTO contacto (`Telefono`, `Facebook`, `Twitter`, `Instagram`, `Correo`, `Telegram`, `LoL`, `WoW`) values(?, ?, ?, ?, ?, ?, ?, ?);";
			//String InStudentControl = ;(`nombres`, `apellidos`, `cedula`, `id_contacto`, `Fecha_Ingreso`, `seccion`, `id_asignatura`) VALUES ('Carlos', 'Suarez', '25555555', '8', '2021-03-24', '1', '2');
			try {
				
			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
				
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//Contacto del estudiante
			PSContacto = conexion.prepareStatement(InContact);
			PSContacto.setString(1, Mode.getTelefono());
			PSContacto.setString(2, Mode.getFacebook());
			PSContacto.setString(3, Mode.getTwitter());
			PSContacto.setString(4, Mode.getInstagram());
			PSContacto.setString(5, Mode.getCorreo());
			PSContacto.setString(6, Mode.getTelegram());
			PSContacto.setString(7, Mode.getLoL());
			PSContacto.setString(8, Mode.getWoW());
			PSContacto.execute();
			
			//Datos del Estudiante
			PSEstudiante = conexion.prepareStatement(InStudent);
			PSEstudiante.setString(1, Mode.getNombres());
			PSEstudiante.setString(2, Mode.getApellidos());
			PSEstudiante.setString(3, Mode.getCedula());
			PSEstudiante.setInt(4, BusquedaIDContacto(Mode));
			PSEstudiante.setDate(5, Mode.getFecha_Ingreso());
			PSEstudiante.setInt(6, Mode.getSeccion());
			PSEstudiante.setInt(7, Mode.getAsignatura());
			PSEstudiante.execute();

			//Comenzar las notas del estudiante en 0
			if(Mode.getAsignatura() == 1) {
				idNotas = AperturaNotas(PSNotas_A1, 1, Mode);
				columnaNotas = "id_Notas_A1";
			} 
			else if(Mode.getAsignatura() == 2) {
				idNotas = AperturaNotas(PSNotas_A2, 2, Mode);
				columnaNotas = "id_Notas_A2";
			}
			
			//Inserción para la tabla principal
			PSControl_Estudiante = conexion.prepareStatement("INSERT INTO control_estudiantes (`id_Estudiante`,`"+columnaNotas+"`) VALUES (?, ?)");
			PSControl_Estudiante.setInt(1, BusquedaIDEstudiante(Mode));
			PSControl_Estudiante.setInt(2, idNotas);
			PSControl_Estudiante.execute();
			
			//Informe sobre el proceso concluido
			JOptionPane.showMessageDialog(null,"Registro concluido con éxito.");
			
			} catch(SQLException e) {

				//Informe sobre que no conecta con la base de datos
				JOptionPane.showMessageDialog(null,"NO CONECTA CON LA BASE DE DATOS");
				
				//Para informar sobre donde o que ocasiona el error
				e.printStackTrace();
			}
		}
		
		//Metodo para la busqueda
       
		public void Busqueda(Modulo Mode, Boolean General) {
		//Conectar con la base de datos
		
			try {

				//1. Conectar con la base de datos
//				
//				Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
//				
//				//2. Creacion del Statement
//				
//				stt = conexion.createStatement();
				
				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				//3. Crear el PreparedStatement
				
				//En el parentesis se debe ingresar una sentencia sql
				PSEstudiante = conexion.prepareStatement("SELECT * FROM estudiante WHERE cedula = ?;");
				
				//Se busca en todas las tablas los datos del estudiante por su id
				PSContacto = conexion.prepareStatement("SELECT * FROM contacto WHERE id = ?;");
				PSAsignatura = conexion.prepareStatement("SELECT * FROM asignatura WHERE id = ?;");
				PSNotas_A1 = conexion.prepareStatement("SELECT * FROM notas_algoritmo1 WHERE id_estudiante = ?;");
				PSNotas_A2 = conexion.prepareStatement("SELECT * FROM notas_algoritmo2 WHERE id_estudiante = ?;");
				
				//Ingresar los campos de ?
				PSEstudiante.setString(1, Mode.getCedula());
				PSContacto.setInt(1, BusquedaIDEstudiante(Mode));
				PSAsignatura.setInt(1, BusquedaIDAsignatura(Mode));
				PSNotas_A1.setInt(1, BusquedaIDEstudiante(Mode));
				PSNotas_A2.setInt(1, BusquedaIDEstudiante(Mode));
				
				//Se crean los ResultSets
				
				Estudiante = PSEstudiante.executeQuery(); 
				Contacto = PSContacto.executeQuery();
				Asignatura = PSAsignatura.executeQuery();
				Notas_A1 = PSNotas_A1.executeQuery();
				Notas_A2 = PSNotas_A2.executeQuery();
				
				//Recorrer el ResultSet
				
				//Primero de la tabla estudiante
				while(Estudiante.next()) {
					//Si la cadena de datos está vacio
					if(Datos == null) { Datos = ""; }
					
					    Datos = "-----------------------------------------------------------------------------\n"+
							    "----------Datos Personales----------\n"+
							    "Nombre y Apellidos: "+Estudiante.getString("nombres") +" "+Estudiante.getString("apellidos")+".\n"+"Cedula: "+Estudiante.getString("cedula")+".\n"+
							    "Fecha de Ingreso: "+Estudiante.getString("Fecha_Ingreso")+".\n";
					
					    cedula = Estudiante.getInt("cedula"); //Obteniendo la cedula para mas tarde
					    cedulaBor = Estudiante.getInt("cedula");
	            	    
					    if(!General) {
					    	JOptionPane.showMessageDialog(null, "Se encontró al estudiante.");
					    }
					    
					    DatosPersonales[0] = Estudiante.getString("nombres");
					    DatosPersonales[1] = Estudiante.getString("apellidos");
					    DatosPersonales[2] = Estudiante.getString("cedula");
					    
					    seccion = Estudiante.getInt("seccion");
					}
				
				//Ahora con la tabla contacto
				//JOptionPane.showMessageDialog(null, "El resultset de contacto es "+Contacto.next());
				 //if(Contacto.next())
				 do{
					//Si la cadena Datos está vacío
					if(Datos == null) { Datos = ""; }
					//JOptionPane.showMessageDialog(null, "El resultset de contacto es "+Contacto.next());
					    //Guardar todos los datos de la persona para ser retornado
					    //CambiarContactosNoTiene(Contacto); //Verificar si tiene contacto o no para que se imprima bien
					  //Llenando los campos de notas de la tabla de algoritmo N
			        	Datos = Datos + "----------Contacto----------\n";
			        	//do
			        	if(Contacto.next()){
			        		String MetodosContacto[] = {"Telefono", "Facebook", "Twitter", "Instagram", "Correo", "Telegram", "LoL", "WoW"};
			        		for (int i = 0; i <= 7; i++) { //Recorre los metodos para contactarse en la base de datos
			        			if(Contacto.getString(MetodosContacto[i]).equals("")) { //Si no tiene un contacto, aparece como NO TIENE
			        				Datos = Datos + MetodosContacto[i] + ": NO TIENE. \n";
			        			} 
			        			else //Si le pusieron algun dato
			        			{
			        				if(i == 6) {
			        					Datos = Datos + "League of Legends: " + Contacto.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
			        				}
			        				else if (i == 7) {
			        					Datos = Datos + "World of Warcraft: " + Contacto.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
			        				}
			        				else {
			        					Datos = Datos + MetodosContacto[i] + ": " + Contacto.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
			        				}
			        			}
			        		}
			        	}
				 }while(Contacto.next());
				 
				//Ahora con la tabla asignatura 
				while(Asignatura.next()) {
					Datos = Datos +	"----------Asignatura----------\n"+
						    "Asignatura: "+Asignatura.getString("Asignatura")+".\n"+
							"Seccion: "+ seccion+".\n";
					//Ajustando el imprimir las notas
					if(Asignatura.getString("Asignatura").equals("Algoritmo 1")) {
						RestosdelTexto(Notas_A1, 1);
					}
					else //Si vio algoritmo 1 con gabriel y ahora esta cursando algoritmo 2
					{
						CambiarNotasNoTiene(Notas_A1, 1); //Imprimir las notas de algoritmo 1
		    		    RestosdelTexto(Notas_A2, 2); //Imprimir las notas de algoritmo 2
					}
					
					AsignaturaYFecha[0] = Asignatura.getInt("id");
						
				}
				
				Resultado = Datos;
			}catch(SQLException e) {
				
				//Por si no agarra
				JOptionPane.showMessageDialog(null, "Hubo un problema en la consulta.\nVerifique la cedula del estudiante.");
				
				e.printStackTrace();
			}
		}
		
		//Metodo para la consulta general
		public void ConsultaG(Modulo Mode) {
			
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				//En el parentesis se debe ingresar una sentencia sql
				PreparedStatement PSIdEstudiante = conexion.prepareStatement("SELECT id, cedula FROM Estudiante");
				ResultSet IDEstudiante = PSIdEstudiante.executeQuery();
				
				JComboBox ids = new JComboBox();
				JComboBox ci = new JComboBox();
				
				ids.removeAllItems();
				ci.removeAllItems();
				//Recorrer el ResultSet
				while(IDEstudiante.next()) {
					
					ids.addItem(IDEstudiante.getInt("id"));
					ci.addItem(IDEstudiante.getString("cedula"));
				}
				
				for(int i = 0; i <= (ids.getItemCount() - 1); i++) {
					//Pasando la cedula al modulo
					//JOptionPane.showMessageDialog(null, "cedula del estudiante N: "+i+", cedula: "+ci.getItemAt(i));
					Mode.setCedula(ci.getItemAt(i).toString());

					//Llamando al metodo de buscar estudiante
					Busqueda(Mode, true);
					
					ResultadoFinal = ResultadoFinal + Resultado;
				}
				
				Datos = ResultadoFinal;
				
			}catch(SQLException e){
				
				//Indica donde esta el problema
				e.printStackTrace();
				
				//Mensaje de advertencia
				JOptionPane.showMessageDialog(null, "Hubo un problema en la consulta.\nVerifique la cosulta interna.");
			}
		}
		
		//Metodo para la actualizacion
		public void Actualizacion(Modulo Mode) {
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				//Se establece el comando SQL
				
				//Estudiante
				PSEstudiante = conexion.prepareStatement("UPDATE estudiante set nombres = ?, apellidos = ?, cedula = ?, Fecha_Ingreso = ?, Seccion = ? where id = ?");
				PSEstudiante.setString(1, Mode.getNombres());
				PSEstudiante.setString(2, Mode.getApellidos());
				PSEstudiante.setString(3, Mode.getCedula());
				PSEstudiante.setDate(4, Mode.getFecha_Ingreso());
				PSEstudiante.setInt(5, Mode.getSeccion());
				PSEstudiante.setInt(6, BusquedaIDEstudiante(Mode));
				PSEstudiante.execute();
				
				//Contacto
				PSContacto = conexion.prepareStatement("UPDATE contacto set Telefono = ?, Facebook = ?, Twitter = ?, Instagram = ?, Correo = ?, Telegram = ?, LoL = ?, WoW = ? where id = ?");
		        PSContacto.setString(1,  Mode.getTelefono());
		        PSContacto.setString(2, Mode.getFacebook());
		        PSContacto.setString(3, Mode.getTwitter());
		        PSContacto.setString(4, Mode.getInstagram());
		        PSContacto.setString(5, Mode.getCorreo());
		        PSContacto.setString(6, Mode.getTelegram());
		        PSContacto.setString(7, Mode.getLoL());
		        PSContacto.setString(8, Mode.getWoW());
		        PSContacto.setInt(9, BusquedaIDContacto(Mode));
				PSContacto.execute();
				
				JOptionPane.showMessageDialog(null, "Datos actualizados.");
			}catch(SQLException e) {
				//Mostrar donde esta el error y mensaje de ayuda por si ocurre algo
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Hubo un problema en la actualizacion de los datos.\nVerifica los datos o la conexion de la base de datos.");
			}
		}
		
		//Metodo para la eliminacion
		public void Borrar(Modulo Mode) {
			
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				//Eliminacion
				//Tabla Contacto
				PSContacto = conexion.prepareStatement("DELETE FROM contacto WHERE id = ?;");
				PSContacto.setInt(1, BusquedaIDEstudiante(Mode));
				PSContacto.execute();
				
				//Tabla Notas de algoritmo 1
				PSNotas_A1 = conexion.prepareStatement("DELETE FROM notas_algoritmo1 where id = ?;");
				PSNotas_A1.setInt(1, BusquedaIDNotas(Mode, 1));
				PSNotas_A1.execute();
				
				//Tabla Notas de algoritmo 2
				PSNotas_A2 = conexion.prepareStatement("DELETE FROM notas_Algoritmo2 where id = ?;");
				PSNotas_A2.setInt(1, BusquedaIDNotas(Mode, 2));
				PSNotas_A2.execute();
				
				//Tabla principal
				PSControl_Estudiante = conexion.prepareStatement("DELETE FROM control_estudiantes where id_Estudiante = ?");
				PSControl_Estudiante.setInt(1, BusquedaIDEstudiante(Mode));
				PSControl_Estudiante.execute();
				
				//Tabla estudiante
				PSEstudiante = conexion.prepareStatement("DELETE FROM estudiante WHERE cedula = ?;");
				PSEstudiante.setString(1, Mode.getCedula());
				PSEstudiante.execute();

				//Mensaje que el proceso está concluido
				JOptionPane.showMessageDialog(null, "Proceso Concluido.\nEstudiante eliminado de la base de datos.");
			}catch(SQLException e) {
				//Mensaje de advertencia
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,"Hubo un problema en la actualizacion de los datos.\nVerifica la cedula ingresada o la conexion de la base de datos.");
			}
		}
		/*
		//metodo para generar el reporte
		public void Reporte() {
			//Creamos componente de tipo document
			Document documento = new Document(PageSize.LETTER, 50, 50, 50, 50);
			
			try {//Creamos el try
				
				//Creamos un par de metadatos
				documento.addAuthor("Roque Lopez");
				documento.addTitle("Reporte PDF");
			    
				String ruta = System.getProperty("user.home"); //Creamos la variable el cual tendrá el acceso a la ruta
				PdfWriter.getInstance(documento, new FileOutputStream(ruta+"/Desktop/Reporte.pdf")); //Creamos el PDFWriter para indicar donde se guardará el pdf 
			    
				documento.open(); //Abrimos el documento
			      
				PdfPTable tablachofer1 = new PdfPTable(4); //Se crea la tabla que será plasmada en el pdf
				PdfPTable tablachofer2 = new PdfPTable(4);
				PdfPTable tablachofer3 = new PdfPTable(1);
				PdfPTable tablaestado = new PdfPTable(2);
				PdfPTable tablamunicipio = new PdfPTable(2);
				PdfPTable tablavehiculo = new PdfPTable(4);
				
				
				//Colocamos el nombre a cada columna de la tabla chofer
				tablachofer1.addCell("Código.");
				tablachofer1.addCell("Cedula.");
				tablachofer1.addCell("Nombres y Apellidos.");
				tablachofer1.addCell("Dirección.");
				tablachofer2.addCell("Telefono");
				tablachofer2.addCell("Correo Electronico.");
				tablachofer2.addCell("ID Estado");
				tablachofer2.addCell("ID Municipio");
				tablachofer3.addCell("ID Vehiculo");
				
				//Colocamos el nombre a cada columna de la tabla estado
				tablaestado.addCell("Código.");
				tablaestado.addCell("Estado.");
				
				//Colocamos el nombre a cada columna de la tabla municipio
				tablamunicipio.addCell("Código");
				tablamunicipio.addCell("Municipio");
				
				//Colocamos el nombre para las columnas de la tabla vehiculo
				tablavehiculo.addCell("Código");
				tablavehiculo.addCell("Placa del Vehiculo.");
				tablavehiculo.addCell("Modelo del Vehiculo.");
				tablavehiculo.addCell("Marca del Vehiculo.");
				
				//Creamos un parrafo vacio para que haya un espacio entre las tablas
				Paragraph vacio = new Paragraph();
			    vacio.add("\n");
			    
			    //Linea separadora
			    Paragraph Separador = new Paragraph();
			    Separador.add("——————————————————————————————————————");
			    
			    //Identificadores para las tablas
				Paragraph TChofer =new Paragraph("TABLA CHOFER",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));
				Paragraph TVehiculo =new Paragraph("TABLA VEHICULO",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));
				Paragraph TEstado =new Paragraph("TABLA ESTADO",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));
				Paragraph TMunicipio =new Paragraph("TABLA MUNICIPIO",FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD, BaseColor.BLACK));

				//Conectar con la base de datos
				try {
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
					
					stt = conexion.createStatement();
					
					//Usar una base de datos
					
					String use = "use urbantaxi;"; //Instruccion SQL
					
					stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
					//Antes de todo se añade el titulo
				    documento.add(TChofer); documento.add(vacio);
				    
					//----------------------->TABLA DE CHOFER<---------------------------
				
					 	PSChofer = conexion.prepareStatement("Select * from chofer;");
						
						ResultSet rs = PSChofer.executeQuery();
						
						if(rs.next()) { //Si hay datos en la base de datos
							
							do {
								//Se coloca para añadir los valores de la base de datos en la tabla de pdf
								tablachofer1.addCell(rs.getString(1)); //Se añade el texto a la primera columna
								tablachofer1.addCell(rs.getString(2)); //Se añade el texto a la segunda columna
								tablachofer1.addCell(rs.getString(3));
								tablachofer1.addCell(rs.getString(4));
								tablachofer2.addCell(rs.getString(5));
								tablachofer2.addCell(rs.getString(6));
								tablachofer2.addCell(rs.getString(7));
								tablachofer2.addCell(rs.getString(8));
								tablachofer3.addCell(rs.getString(9));	
																				
							}while(rs.next());

							//Se añade todo al documento
							documento.add(tablachofer1); documento.add(vacio);
							documento.add(tablachofer2); documento.add(vacio);
							documento.add(tablachofer3); documento.add(vacio);
							documento.add(vacio); documento.add(Separador); documento.add(vacio);
						}
					
					//----------------------->TABLA DE VEHICULO<---------------------------
					documento.add(TVehiculo); documento.add(vacio);

					PSVehiculo = conexion.prepareStatement("Select * from vehiculo");
					
					rs = PSVehiculo.executeQuery();
					
					if(rs.next()) { //Si hay datos en la base de datos
						
						do {
							//Se coloca para añadir los valores de la base de datos en la tabla de pdf
							tablavehiculo.addCell(rs.getString(1)); //Se añade el texto a la primera columna
							tablavehiculo.addCell(rs.getString(2)); //Se añade el texto a la segunda columna
							tablavehiculo.addCell(rs.getString(3)); //Se añade el texto a la tercera columna y así
							tablavehiculo.addCell(rs.getString(4));
						}while(rs.next());
						
						//Se añade todo al documento
						documento.add(tablavehiculo);
						documento.add(vacio); documento.add(Separador); documento.add(vacio);
						
						//----------------------->TABLA DE ESTADO<---------------------------

					    documento.add(TEstado); documento.add(vacio);

						PSEstado = conexion.prepareStatement("Select * from estado;");
						
						rs = PSEstado.executeQuery();
						
						if(rs.next()) { //Si hay datos en la base de datos
							
							do {
								//Se coloca para añadir los valores de la base de datos en la tabla de pdf
								tablaestado.addCell(rs.getString(1)); //Se añade el texto a la primera columna
								tablaestado.addCell(rs.getString(2)); //Se añade el texto a la segunda columna
							}while(rs.next());
							
							//Se añade todo al documento
							documento.add(tablaestado);
							documento.add(vacio); documento.add(Separador); documento.add(vacio);
						}
						
						//----------------------->TABLA DE MUNICIPIO<---------------------------
						
					    documento.add(TMunicipio); documento.add(vacio);

						PSMunicipio = conexion.prepareStatement("Select * from municipio;");
						
						rs = PSMunicipio.executeQuery();
						
						if(rs.next()) { //Si hay datos en la base de datos
							
							do {
								//Se coloca para añadir los valores de la base de datos en la tabla de pdf
								tablamunicipio.addCell(rs.getString(1)); //Se añade el texto a la primera columna
								tablamunicipio.addCell(rs.getString(2)); //Se añade el texto a la segunda columna
							}while(rs.next());
							
							//Se añade todo al documento
							documento.add(tablamunicipio);
							documento.add(vacio); documento.add(Separador); documento.add(vacio);
						}
					}
				}catch(DocumentException | SQLException e) {}
				//Cerramos el documento
				documento.close();
				JOptionPane.showMessageDialog(null,"Reporte creado con éxito\n en el escritorio."); //Mensaje de verificacion que ya se hizo el pdf
			}catch(DocumentException | HeadlessException | FileNotFoundException a) {
				
			}
			
			
		}*/

		//Metodo para devolver el id del vehiculo
		public int BusquedaIDContacto(Modulo Mode) {
			
			//Variables necesarias para buscar el id del estudiante
			String Correo = Mode.getCorreo();
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				Mistatement = conexion.prepareStatement("Select id from contacto where correo like ?;");
				Mistatement.setString(1,Correo);

				ResultSet rs = Mistatement.executeQuery();
				if(rs.next()) { //Si hay datos en la base de datos
					do {
						idContacto = rs.getInt("id"); 
					}while(rs.next());
				}
			}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del vehiculo"); e.printStackTrace();}
			return idContacto;
		}
			
		public int BusquedaIDEstudiante(Modulo Mode) {
			
			//Variables necesarias para buscar el id del vehiculo
			String ci = Mode.getCedula();
			try {
				
				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
				    //2. Creacion del Statement
				
				    stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				Mistatement = conexion.prepareStatement("Select id from estudiante where cedula =?");
				Mistatement.setString(1,ci);
				ResultSet rs = Mistatement.executeQuery();
				if(rs.next()) { //Si hay datos en la base de datos
					do {
						idEstudiante = rs.getInt("id"); 
					}while(rs.next());
				}
			}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del estudiante"); e.printStackTrace();}
			return idEstudiante;
		}

		public int BusquedaIDAsignatura(Modulo Mode) {
			
			//Variables necesarias para buscar el id del vehiculo
			int id_estudiante = BusquedaIDEstudiante(Mode);
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				Mistatement = conexion.prepareStatement("Select id_asignatura from estudiante where id =?");
				Mistatement.setInt(1,id_estudiante);
				ResultSet rs = Mistatement.executeQuery();
				if(rs.next()) { //Si hay datos en la base de datos
					do {
						idAsignatura = rs.getInt("id_asignatura"); 
					}while(rs.next());
				}
			}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del estudiante"); e.printStackTrace();}
			return idAsignatura;
		}
		
		public int AperturaNotas(PreparedStatement Notas, int N, Modulo Mode) throws SQLException {
					
			Notas = conexion.prepareStatement("INSERT INTO notas_algoritmo"+N+" (`Nota1`, `Nota2`, `Nota3`, `Nota4`,`Nota5`,`Promedio`, `id_Estudiante`, `seccion`, `Estado`) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for(int i = 1; i <= 5; i++) {
				Notas.setInt(i, 0);
			}
			Notas.setFloat(6, 0);
			Notas.setInt(7, BusquedaIDEstudiante(Mode));
			Notas.setInt(8, Mode.getSeccion());
			Notas.setString(9, "Cursando");
			Notas.execute();
			
			return BusquedaIDNotas(Mode, N);
		}
		
        public int BusquedaIDNotas(Modulo Mode, int N) {
			
			//Variables necesarias para buscar el id del vehiculo
			int id_estudiante = BusquedaIDEstudiante(Mode);
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				Mistatement = conexion.prepareStatement("Select id from notas_algoritmo"+N+" where id_estudiante =?");
				Mistatement.setInt(1,id_estudiante);
				ResultSet rs = Mistatement.executeQuery();
				if(rs.next()) { //Si hay datos en la base de datos
					do {
						idNotas = rs.getInt("id"); 
					}while(rs.next());
				}
			}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del estudiante"); e.printStackTrace();}
			return idNotas;
		}
        
        public void RestosdelTexto(ResultSet cursando, int N) throws SQLException {
        	
        	if(N == 1) {
        		
        		CambiarNotasNoTiene(cursando, N); //Cambiando la impresion de 0 a No Tiene
        		//Llenando el campo de las notas de algoritmo 2
    		    Datos = Datos + "----------Notas de Algoritmo 2---------- \n";
    		    
    		    //Como no ha llegado a algoritmo 2 aun, se le pone como no tiene
    		    for(int i = 1; i <= 5; i++) { Datos = Datos + "Nota "+i+": NO TIENE. \n";}

    		    //Barra final de las notas
    			Datos = Datos + "-----------------------------------------------------------------------------\n";
        	}
        	else 
        	{
        	    CambiarNotasNoTiene(cursando, N); //Cambiando la impresion de 0 a No Tiene
     		    Datos = Datos + "-----------------------------------------------------------------------------\n";
        	}

        }
        
        public void CambiarNotasNoTiene(ResultSet Columna, int N) throws SQLException {
    		//Llenando los campos de notas de la tabla de algoritmo N
        	if(Columna.next()) 
        	{
        		Datos = Datos + "----------Notas de Algoritmo "+N+"----------\n";
        		for (int i = 1; i <= 5; i++) { //Recorre las 5 notas en la base de datos
        			if(Columna.getInt("Nota"+i) == 0) { //Si la nota es 0, la coloca como no tiene la nota
        				Datos = Datos + "Nota "+i+": NO TIENE. \n";
        			} 
        			else //Si le pusieron una nota
        			{
        				Datos = Datos + "Nota "+i+": "+Columna.getInt("Nota"+i)+". \n"; //Imprimiendo el valor de la nota
        			}
        		}
        	}

        }
        
        public void CambiarContactosNoTiene(ResultSet Columna) throws SQLException {
    		//Llenando los campos de notas de la tabla de algoritmo N
        	Datos = Datos + "----------Contacto----------\n";
        	//do
        	if(Columna.next()){
        		String MetodosContacto[] = {"Telefono", "Facebook", "Twitter", "Instagram", "Correo", "Telegram", "LoL", "WoW"};
        		for (int i = 0; i <= 7; i++) { //Recorre los metodos para contactarse en la base de datos
        			if(Columna.getString(MetodosContacto[i]).equals("")) { //Si no tiene un contacto, aparece como NO TIENE
        				Datos = Datos + MetodosContacto[i] + ": NO TIENE. \n";
        			} 
        			else //Si le pusieron algun dato
        			{
        				if(i == 6) {
        					Datos = Datos + "League of Legends: " + Columna.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
        				}
        				else if (i == 7) {
        					Datos = Datos + "World of Warcraft: " + Columna.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
        				}
        				else {
        					Datos = Datos + MetodosContacto[i] + ": " + Columna.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
        				}
        			}
        		}
			    ContactoEstudiante[0] = Columna.getString("Telefono");
			    ContactoEstudiante[1] = Columna.getString("Facebook");
			    ContactoEstudiante[2] = Columna.getString("Twitter");
			    ContactoEstudiante[3] = Columna.getString("Instagram");
			    ContactoEstudiante[4] = Columna.getString("Correo");
			    ContactoEstudiante[5] = Columna.getString("Telegram");
			    ContactoEstudiante[6] = Columna.getString("LoL");
			    ContactoEstudiante[7] = Columna.getString("WoW");
        	}//while(Columna.next()); 

        	//JOptionPane.showMessageDialog(null,  "La lista va por "+ayuda);
        }
        
        public void CambiarPass(Modulo Mode) {
        	
        	try {
				
    			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

    			if(conexion == null) {
    				
    				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
    			
    				//2. Creacion del Statement
    				
    				stt = conexion.createStatement();
    			}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				//Se establece el comando SQL
				
				//Estudiante
				PSMaraton = conexion.prepareStatement("UPDATE Seguridad set Contrasena = ? where id = 1");
				PSMaraton.setString(1, Mode.getPass());
				PSMaraton.execute();
				
				JOptionPane.showMessageDialog(null, "Contraseña actualizada.");
			}catch(SQLException e) {
				//Mostrar donde esta el error y mensaje de ayuda por si ocurre algo
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Hubo un problema en la actualizacion de los datos.\nVerifica los datos o la conexion de la base de datos.");
			}
        }
        
        public String ObtenerPass() {
			
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				Mistatement = conexion.prepareStatement("Select contrasena from seguridad where id = 1;");

				//JOptionPane.showMessageDialog(null, "Buscando la contraseña en la base de datos");
				ResultSet rs = Mistatement.executeQuery();
				if(rs.next()) { //Si hay datos en la base de datos
						
					//JOptionPane.showMessageDialog(null, "La clave es: "+rs.getString("Contrasena"));
					Clave = rs.getString("Contrasena");
				}
			}catch(SQLException e) { 
				
				JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del estudiante"); e.printStackTrace();
			}
			return Clave;
		}
        
        public void CambiarEmail(Modulo Mode) {
        	
        	try {
				
    			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

    			if(conexion == null) {
    				
    				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
    			
    				//2. Creacion del Statement
    				
    				stt = conexion.createStatement();
    			}
				
				//Usar una base de datos
				
				String use = "use genoal;"; //Instruccion SQL
				
				stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
				//Se establece el comando SQL
				
				//Estudiante
				PSSeguridad = conexion.prepareStatement("UPDATE Seguridad set Email = ? where id = 1");
				PSSeguridad.setString(1, Mode.getEmail());
				PSSeguridad.execute();
				
				JOptionPane.showMessageDialog(null, "Correo Electronico actualizado.");
			}catch(SQLException e) {
				//Mostrar donde esta el error y mensaje de ayuda por si ocurre algo
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Hubo un problema en la actualizacion de los datos.\nVerifica los datos o la conexion de la base de datos.");
			}
        }
        
        //-----------Getters para la exportancion de la base de datos----------------
        
        public static String getUsuario() {
        	return usuario;
        }
        
        public static String getPass() {
        	return password;
        }
        
        public static String getBD() {
        	return BD;
        }
}
