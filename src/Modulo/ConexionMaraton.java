package Modulo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
import javax.swing.JComboBox;

import Vista.BEBMaraton;
import Vista.IngresarNota;
import Vista.Vista;

public class ConexionMaraton {
	//Caracteristicas necesarias para conectar con la base de datos
	Connection conexion; //Conectar con la base de datos
	Statement stt; //Para el use de la base de datos
	PreparedStatement PSAlgoritmo = null, PSAlgoritmo2 = null, PSAsignatura = null, PSContacto = null,  Mistatement; //Para las instruccion SQL
	PreparedStatement PSEstudiante = null, PSMaraton = null, PSNotas_A = null, PSNotas_A2 = null, PSSeguridad = null;
	ResultSet Maraton, AlgoritmoResultSet, Contacto, Estudiante, Notas_A;
	
	public Vista Ventana;//Conectando a la vista principal
	BEBMaraton BEBM; //Conectando a la vista de Registrar estudiante.
	IngresarNota IN;		
	
	//Variables imporantes para el desarrollo de la conexion
	public int N, iteracion = 0, idEstudiante = 0, Cedula1, Cedula2, Cedula3; //N es para diferenciar entre las unidades, si es la unidad 1 o la 2 y así
	public String Datos; //Datos es para almacenar toda la información que se requiera en una sola variable.
	public String GanadorOro, GanadorPlata, GanadorBronce;
	public Boolean Encontrado = false;
	public int IdGanador1, IdGanador2, IdGanador3, ite = 1, idMaraton;
	
	public ConexionMaraton() {
		
	}
	
	public void RegistrarMaraton(Modulo Mode) {
		//Ingresar datos de un estudiante
		String InMaraton = "INSERT INTO maraton (`Fecha_Inicio`, `id_asignatura`, `Seccion`, `Unidad`, `id_Ganador_Oro`, `id_Ganador_Plata`, `id_Ganador_Bronce`) VALUES (?, ?, ?, ?, ?, ?, ?);";

		try {
			
		//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

		if(conexion == null) {
				
			conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
		
			//2. Creacion del Statement
				
			stt = conexion.createStatement();
		}
		
		//Usar una base de datos
		
		String use = "use genoal;"; //Instruccion SQL
		
		stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
		
		//Datos a ingresar
		PSMaraton = conexion.prepareStatement(InMaraton);
		PSMaraton.setDate(1, Mode.getFecha_Ingreso());
		PSMaraton.setInt(2, Mode.getAsignatura());
		PSMaraton.setInt(3, Mode.getSeccion());
		PSMaraton.setInt(4, Mode.getUnidad());
		PSMaraton.setInt(5, 0);
		PSMaraton.setInt(6, 0);
		PSMaraton.setInt(7, 0);
		PSMaraton.execute();
		
		//Informe sobre el proceso concluido
		JOptionPane.showMessageDialog(null,"Maraton registrado con éxito.");
		
		}catch(SQLException e) {
			//Informe sobre que no conecta con la base de datos
			JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
			
			//Para informar sobre donde o que ocasiona el error
			e.printStackTrace();
		}
	}
	//Metodo para la insercion
	public void Actualizar(Modulo Mode, int Algoritmo, int Unidad, int IDestudiante, boolean borrar, boolean actualizar) {
	    //Ingresar datos de un estudiante
		String InNotasAlgoritmo = "UPDATE notas_algoritmo"+Algoritmo+" SET Nota"+Unidad+" = ? WHERE id_estudiante ="+IDestudiante+";";
		
		try {
					
			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
				
			//Usar una base de datos
				
			String use = "use genoal;"; //Instruccion SQL
				
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use

		    if(!borrar && !actualizar) {
		        //Informe sobre que la inserción se cumplió con éxito
		        JOptionPane.showMessageDialog(null,"Nota ingresada con éxito.");
		    }else if(actualizar){ 
		    	//Informe sobre que se borro la nota con éxito
		        JOptionPane.showMessageDialog(null,"Nota actualizada con éxito.");	
		    }else{
		        //Informe sobre que se borro la nota con éxito
		        JOptionPane.showMessageDialog(null,"Nota borrada con éxito.");		    	
		    }
		}catch(SQLException e){
			//Informe sobre que no conecta con la base de datos
			JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
			
			//Para informar sobre donde o que ocasiona el error
			e.printStackTrace();
		}	
	}		
	public void BusquedaEstudiantes(Modulo Mode, int Algoritmo, int Seccion, int Unidad, JComboBox Lista, JComboBox Lista2, JComboBox Lista3, JComboBox ListaID) {
		//Conectar con la base de datos
		
		try {

			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
			
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//3. Crear el PreparedStatement
			
			PSAlgoritmo = conexion.prepareStatement("SELECT id_estudiante FROM notas_algoritmo"+Algoritmo+" WHERE Seccion ="+Seccion+" AND Estado like 'Cursando';");
			PSAlgoritmo.execute();
			
			//Se crean los ResultSets			
			//Algoritmo1 = PSAlgoritmo1.executeQuery();
			AlgoritmoResultSet = PSAlgoritmo.executeQuery();
			//Estudiante = PSEstudiante.executeQuery();
			
			//la tabla notas de algoritmo 1
			iteracion = 0;
			
			while(AlgoritmoResultSet.next()) {
				
				//Buscar a los estudiantes
				PSEstudiante = conexion.prepareStatement("SELECT id, nombres, apellidos, cedula FROM estudiante WHERE id ="+AlgoritmoResultSet.getInt("id_estudiante"));
				
				//iniciando el resultset del estudiante
				Estudiante = PSEstudiante.executeQuery();
				
				//Buscando con el resultset
				if(Estudiante.next()) {
					
					//Datos 
					Lista.addItem((iteracion + 1)+" "+Estudiante.getString("nombres")+" "+Estudiante.getString("apellidos"));
					
					Lista2.addItem((iteracion + 1)+" "+Estudiante.getString("nombres")+" "+Estudiante.getString("apellidos"));
					
					Lista3.addItem((iteracion + 1)+" "+Estudiante.getString("nombres")+" "+Estudiante.getString("apellidos"));
					
					//Numero de identificación de cada estudiante
					ListaID.addItem(Estudiante.getInt("id"));	
					
				    //Incremento de la iteracion
				    iteracion = iteracion + 1;
					
				}
			}
	    }catch(SQLException e){
			//Informe sobre que no conecta con la base de datos
			JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
			
			//Para informar sobre donde o que ocasiona el error
			e.printStackTrace();
		}
	}
	
	public void BusquedaMaraton(Modulo Mode, Boolean Buscando) {
		//Conectar con la base de datos
		
		try {

			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
					
			//Usar una base de datos
					
			String use = "use genoal;"; //Instruccion SQL
					
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//3. Crear el PreparedStatement
			PSMaraton = conexion.prepareStatement("SELECT id, Fecha_Inicio, id_ganador_oro, id_ganador_plata, id_ganador_bronce FROM maraton WHERE id_asignatura = ? AND seccion = ? AND unidad = ?;");
			PSMaraton.setInt(1, Mode.getAsignatura());
			PSMaraton.setInt(2, Mode.getSeccion());
			PSMaraton.setInt(3, Mode.getUnidad());
					
			//Se crean los ResultSets			
		    Maraton = PSMaraton.executeQuery();
			
		    //Si se encuentra el maraton
		    if(Maraton.next()) {
		    	
		    	//Si la variable esta vacia
		    	if(Datos == null) { Datos = ""; }
		    	
		    	//Asegurarse si hay nombres en las columnas
		    	if(Maraton.getInt("id_Ganador_oro") == 0) {  
		    		
		    		GanadorOro = "SIN GANADOR";
		    		GanadorPlata = "SIN GANADOR";
		    		GanadorBronce = "SIN GANADOR";
		    	} else {
		    		
		    		GanadorOro = BuscarGanador(Mode, Maraton.getInt("id_ganador_oro"), Buscando);
		    		IdGanador1 = Maraton.getInt("id_Ganador_oro");
		    		
		    		GanadorPlata = BuscarGanador(Mode, Maraton.getInt("id_ganador_plata"), Buscando);
		    		IdGanador2 = Maraton.getInt("id_Ganador_plata");
		    		
		    		GanadorBronce = BuscarGanador(Mode, Maraton.getInt("id_ganador_bronce"), Buscando);
		    		IdGanador3 = Maraton.getInt("id_Ganador_bronce");
		    	}
		    	
		    	//Asignando el texto a la variable Datos
			    Datos = "-------------------------------------------------------------------\n"+
								    "----------Maraton----------\n"+
								    "Fecha del evento: "+Maraton.getDate("Fecha_inicio")+".\n"+
								    "Ganador con Medalla de Oro: \n*"+GanadorOro+".\n"+
								    "Ganador con Medalla de Plata: \n*"+GanadorPlata+".\n"+
								    "Ganador con Medalla de Bronce: \n*"+GanadorBronce+".\n"+
						"-------------------------------------------------------------------\n";
			    
			    //Informe para avisar que se encontró el maraton
			   JOptionPane.showMessageDialog(null, "Maraton Encontrado");
			   
			   Encontrado = true;
			   
			   //Numero de identificacion
			   idMaraton = Maraton.getInt("id");
			   
		    }else {
		    	JOptionPane.showMessageDialog(null, "Maraton no encontrado.");
		    }
		    
	    }catch(SQLException e){
					//Informe sobre que no conecta con la base de datos
					JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
					
					//Para informar sobre donde o que ocasiona el error
					e.printStackTrace();
		}
	}
	
	//Metodo para buscar al estudiante por la seccion
	public String BuscarGanador(Modulo Mode, int IdGanador, Boolean Buscando) throws SQLException{
		
		//Variables necesarias para buscar el id del vehiculo
		String NombreApellido = "SIN GANADOR.";
		try {
			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
			
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			Mistatement = conexion.prepareStatement("Select nombres, apellidos, cedula from estudiante where id = ?");
			Mistatement.setInt(1, IdGanador);
			ResultSet rs = Mistatement.executeQuery();
						
			if(rs.next()) { //Si hay datos en la base de datos	
				
				do {

					if(Buscando) {
						
						//JOptionPane.showMessageDialog(null, "El id del ganador es: "+IdGanador);   
					    NombreApellido = rs.getString("nombres")+" "+rs.getString("Apellidos")+".\n"+"C.I: "+rs.getInt("Cedula");
					}
					else {
						
						//JOptionPane.showMessageDialog(null, "El id del ganador es: "+IdGanador);   
					    NombreApellido = rs.getString("nombres")+" "+rs.getString("Apellidos");
					}
					
					ite = ite + 1;//Aumentar contador

					//JOptionPane.showMessageDialog(null,  "La iteracion va por "+ite);
				}while(rs.next());
			}
		}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del estudiante"); e.printStackTrace();}
		//return idNotas;
		return NombreApellido;
	}
	
	public void BusquedaAutomatica(int Algoritmo, int IDEstudiante, JComboBox UnidadPonderar, int ListaSeleccionada) {
		//Conectar con la base de datos
		
		try {

			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
			
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//3. Crear el PreparedStatement
			
			PSAlgoritmo = conexion.prepareStatement("SELECT Nota1, Nota2, Nota3, Nota4, Nota5 FROM notas_algoritmo"+Algoritmo+" WHERE id_estudiante = ?;");
			PSAlgoritmo.setInt(1, IDEstudiante);
			//PSAlgoritmo.execute();
			
			//Se crean los ResultSets
			AlgoritmoResultSet = PSAlgoritmo.executeQuery();
			
			if(AlgoritmoResultSet.next()) {
				//Empieza la busqueda de las notas menores a 50
				do { //Si hay datos en la base de datos
					//Vector donde se guardan las notas del estudiante
					int Notas[] = {AlgoritmoResultSet.getInt("Nota1"), AlgoritmoResultSet.getInt("Nota2"), AlgoritmoResultSet.getInt("Nota3"), AlgoritmoResultSet.getInt("Nota4"), AlgoritmoResultSet.getInt("Nota5")};
						
					//Se recorre el vector
					for(int i = 0; i <= 4; i++) {
							
						if(ListaSeleccionada == 1) {
								
							if(Notas[i] <= 50) { //Se verifica si las notas son menores a 50
								UnidadPonderar.addItem(i+1); //En caso cierto, se agrega la unidad
							}
						}else if(ListaSeleccionada == 2) {
								
							if(Notas[i] <= 70) { //Se verifica si las notas son menores a 50
								UnidadPonderar.addItem(i+1); //En caso cierto, se agrega la unidad
							}
						}else if(ListaSeleccionada == 3) {
								
							if(Notas[i] <= 80) { //Se verifica si las notas son menores a 50
								UnidadPonderar.addItem(i+1); //En caso cierto, se agrega la unidad
							}
					    }	
					}
				}while(AlgoritmoResultSet.next());
			}			
	    }catch(SQLException e){
			//Informe sobre que no conecta con la base de datos
			JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
			
			//Para informar sobre donde o que ocasiona el error
			e.printStackTrace();
		}
	}
	
	public void Busqueda(int IDEstudiante, int Algoritmo) {
	//Conectar con la base de datos
	
		try {

			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
			
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//3. Crear el PreparedStatement
			
			//En el parentesis se debe ingresar una sentencia sql
			PSEstudiante = conexion.prepareStatement("SELECT * FROM estudiante WHERE id ="+IDEstudiante+";");
			
			//Se busca en todas las tablas los datos del estudiante por su id
			PSContacto = conexion.prepareStatement("SELECT * FROM contacto WHERE id = "+IDEstudiante+";");
			PSNotas_A = conexion.prepareStatement("SELECT * FROM notas_algoritmo"+Algoritmo+" WHERE id_estudiante = "+IDEstudiante+";");
			
			//Se crean los ResultSets
			
			Estudiante = PSEstudiante.executeQuery(); 
			Contacto = PSContacto.executeQuery();
			Notas_A = PSNotas_A.executeQuery();
			
			//Recorrer el ResultSet
			
			//Primero de la tabla estudiante
			if(Estudiante.next()) {
				//Si la cadena de datos está vacio
				if(Datos == null) { Datos = ""; }
				
				    Datos = "-----------------------------------------------------------------------------\n"+
						    "----------Datos Personales----------\n"+
						    "Nombre y Apellidos: "+Estudiante.getString("nombres") +" "+Estudiante.getString("apellidos")+".\n"+"Cedula: "+Estudiante.getString("cedula")+".\n"+
						    "Fecha de Ingreso: "+Estudiante.getString("Fecha_Ingreso")+".\n";
				}
			
			//Ahora con la tabla contacto
			 if(Contacto.next()){
				//Si la cadena Datos está vacío
				if(Datos == null) { Datos = ""; }
				
				    //Guardar todos los datos de la persona para ser retornado
				    CambiarContactosNoTiene(Contacto); //Verificar si tiene contacto o no para que se imprima bien
			 }
			 
			//Ahora con la tabla asignatura 
			if(Notas_A.next()) {
				
				Datos = Datos +	"----------Notas de Algoritmo "+N+"----------\n";
			
				//Convirtiendo las notas en 0 a NO TIENE
				RestosdelTexto(Notas_A);
			}
		}catch(SQLException e) {
			
			//Por si no agarra
			JOptionPane.showMessageDialog(null, "Hubo un problema en la consulta.\nVerifique la cedula del estudiante.");
			
			e.printStackTrace();
		}
	}
	
	 public void CambiarContactosNoTiene(ResultSet Columna) throws SQLException {
 		//Llenando los campos de notas de la tabla de algoritmo N
     	Datos = Datos + "----------Contacto----------\n";
     	do
     	{
     		String MetodosContacto[] = {"Telefono", "Facebook", "Twitter", "Instagram", "Correo", "Telegram", "LoL", "WoW"};
     		for (int i = 0; i <= 7; i++) { //Recorre los metodos para contactarse en la base de datos
     			if(Columna.getString(MetodosContacto[i]).equals("")) { //Si no tiene un contacto, aparece como NO TIENE
     				Datos = Datos + MetodosContacto[i] + ": NO TIENE. \n";
     			} 
     			else //Si le pusieron algun dato
     			{
     				if(i == 6) {
     					Datos = Datos + "League of Legends: " + Columna.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
     				}
     				else if (i == 7) {
     					Datos = Datos + "World of Warcraft: " + Columna.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
     				}
     				else {
     					Datos = Datos + MetodosContacto[i] + ": " + Columna.getString(MetodosContacto[i])+". \n"; //Imprimiendo el valor del campo
     				}
     			}
     		}
     	}while(Columna.next()); 

     }
	 
	 public void RestosdelTexto(ResultSet cursando) throws SQLException {

     	//Llenando el campo de las notas de algoritmo 2
     	String Vector[] = { "Nota1", "Nota2", "Nota3", "Nota4", "Nota5"};
     		
 		//Como no ha llegado a algoritmo 2 aun, se le pone como no tiene
 		for(int i = 0; i <= 4; i++) { 
 		    	
 		   	if(cursando.getInt(Vector[i]) == 0) {
 		    	Datos = Datos + "Nota "+(i+1)+": NO TIENE. \n";
 		    }
 		    else {
 		    	Datos = Datos + "Nota "+(i+1)+": "+cursando.getInt(Vector[i])+". \n";
 		    }
 		}

 		//Barra final de las notas
 	    Datos = Datos + "-----------------------------------------------------------------------------\n";
     }
	 
	 public void RegistrarGanadores(Modulo Mode) {
			//Ingresar datos de un estudiante
            String ActMaraton = "UPDATE maraton set id_Ganador_Oro = ?, id_Ganador_Plata = ?, id_Ganador_Bronce = ? where  id_asignatura = ? AND Seccion = ? AND Unidad = ?";
			try {
				
				//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

				if(conexion == null) {
					
					conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
				
					//2. Creacion del Statement
					
					stt = conexion.createStatement();
				}
			
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			JOptionPane.showMessageDialog(null,"Guardando...");
			
			//Id de los ganadores
			IdGanador1 = Mode.getIdGanador1();
			IdGanador2 = Mode.getIdGanador2();
			IdGanador3 = Mode.getIdGanador3();
		    
			//Datos a ingresar
			PSMaraton = conexion.prepareStatement(ActMaraton);
			PSMaraton.setInt(1, Mode.getIdGanador1());
			PSMaraton.setInt(2, Mode.getIdGanador2());
			PSMaraton.setInt(3, Mode.getIdGanador3());
			PSMaraton.setInt(4, Mode.getAsignatura());
			PSMaraton.setInt(5, Mode.getSeccion());
			PSMaraton.setInt(6, Mode.getUnidad());
			PSMaraton.execute();
			
			//Informe sobre el proceso concluido
			JOptionPane.showMessageDialog(null,"Ganadores registrados con éxito.");
			
			}catch(SQLException e) {
				//Informe sobre que no conecta con la base de datos
				JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
				
				//Para informar sobre donde o que ocasiona el error
				e.printStackTrace();
			}
		}
	 
	//Metodo para la eliminacion
	public void SuspenderMaraton(int IdMaraton) {
				
		try {
			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
					
			//Usar una base de datos
					
			String use = "use genoal;"; //Instruccion SQL
					
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
					
			//Eliminacion
			//Tabla Contacto
			PSMaraton = conexion.prepareStatement("DELETE FROM Maraton WHERE id = ?;");
			PSMaraton.setInt(1, IdMaraton);
			PSMaraton.execute();
			
			//Mensaje que el proceso está concluido
			JOptionPane.showMessageDialog(null, "Proceso Concluido.\nMaraton Suspendido.");
		}catch(SQLException e) {
			
			//Mensaje de advertencia
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Hubo un problema en la actualizacion de los datos.\nVerifica la cedula ingresada o la conexion de la base de datos.");			
		}
	}
}
