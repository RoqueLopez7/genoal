package Modulo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
import javax.swing.JComboBox;

import Vista.IngresarNota;
import Vista.RegistrarEstudiante;
import Vista.Vista;

public class ConexionNotas {
	//Caracteristicas necesarias para conectar con la base de datos
	Connection conexion; //Conectar con la base de datos
	Statement stt; //Para el use de la base de datos
	PreparedStatement PSAlgoritmo = null, Mistatement;//Para las instruccion SQL
	PreparedStatement PSEstudiante = null;
	ResultSet Algoritmo1, Estudiante;
	
	public Vista Ventana;//Conectando a la vista principal
	RegistrarEstudiante RE; //Conectando a la vista de Registrar estudiante.
	IngresarNota IN;		
	
	//Variables imporantes para el desarrollo de la conexion
	public int N, iteracion = 0, idEstudiante = 0; //N es para diferenciar entre las unidades, si es la unidad 1 o la 2 y así
	public String Datos; //Datos es para almacenar toda la información que se requiera en una sola variable.
		
	public ConexionNotas() {
		
	}
	
	//Metodo para la insercion
	public void Actualizar(Modulo Mode, int Algoritmo, int Unidad, int IDestudiante, boolean borrar, boolean actualizar) {
	    //Ingresar datos de un estudiante
		String InNotasAlgoritmo = "UPDATE notas_algoritmo"+Algoritmo+" SET Nota"+Unidad+" = ? WHERE id_estudiante ="+IDestudiante+";";
		
		try {
					
			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
				
			//Usar una base de datos
				
			String use = "use genoal;"; //Instruccion SQL
				
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
				
			//Ingresar nota de algoritmo
	        PSAlgoritmo = conexion.prepareStatement(InNotasAlgoritmo);
		    PSAlgoritmo.setInt(1, Mode.getNotaAlgoritmo());
		    PSAlgoritmo.execute();

		    if(!borrar && !actualizar) {
		        //Informe sobre que la inserción se cumplió con éxito
		        JOptionPane.showMessageDialog(null,"Nota ingresada con éxito.");
		    }else if(actualizar){ 
		    	//Informe sobre que se borro la nota con éxito
		        JOptionPane.showMessageDialog(null,"Nota actualizada con éxito.");	
		    }else{
		        //Informe sobre que se borro la nota con éxito
		        JOptionPane.showMessageDialog(null,"Nota borrada con éxito.");		    	
		    }
		}catch(SQLException e){
			//Informe sobre que no conecta con la base de datos
			JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
			
			//Para informar sobre donde o que ocasiona el error
			e.printStackTrace();
		}	
	}		
	public void BusquedaAutomatica(Modulo Mode, JComboBox Lista, JComboBox ListaID, int Algoritmo) {
		//Conectar con la base de datos
		
		try {

			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
			
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//3. Crear el PreparedStatement
			
			PSEstudiante = conexion.prepareStatement("SELECT id, nombres, apellidos FROM estudiante WHERE Seccion = ? AND id_asignatura ="+Algoritmo);
			PSEstudiante.setInt(1, Mode.getSeccion());
			PSEstudiante.execute();
			
			//Se crean los ResultSets			
			//Algoritmo1 = PSAlgoritmo1.executeQuery();
			Estudiante = PSEstudiante.executeQuery();
			
			//la tabla notas de algoritmo 1
			iteracion = 0;
			
			while(Estudiante.next()) {
				
				//Datos 
				Lista.addItem((iteracion + 1)+" "+Estudiante.getString("nombres")+" "+Estudiante.getString("apellidos"));
				
				//Numero de identificación de cada estudiante
				ListaID.addItem(Estudiante.getInt("id"));
				
				//Incremento de la iteracion
				iteracion = iteracion + 1;
				
			}
	    }catch(SQLException e){
			//Informe sobre que no conecta con la base de datos
			JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
			
			//Para informar sobre donde o que ocasiona el error
			e.printStackTrace();
		}
	}
	
	public void BusquedaNota(Modulo Mode, int Unidad, int Algoritmo, int IdEstudiante) {
		//Conectar con la base de datos
		
		try {

			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
					
			//Usar una base de datos
					
			String use = "use genoal;"; //Instruccion SQL
					
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			//3. Crear el PreparedStatement
			PSAlgoritmo = conexion.prepareStatement("SELECT Nota"+Unidad+", id_estudiante FROM notas_algoritmo"+Algoritmo+" WHERE id_estudiante = ? ORDER BY id > 0;");
			PSAlgoritmo.setInt(1, IdEstudiante);
			PSAlgoritmo.execute();
					
			//Se crean los ResultSets			
		    Algoritmo1 = PSAlgoritmo.executeQuery();
			
		    while(Algoritmo1.next()) {
				
			    Datos = "-----------------------------------------------------------------------------\n"+
								    "----------Nota----------\n"+
								    "Nota de la unidad "+Unidad+": "+Algoritmo1.getInt("Nota"+Unidad);
				idEstudiante = Algoritmo1.getInt("id_estudiante");		
			}
	    }catch(SQLException e){
					//Informe sobre que no conecta con la base de datos
					JOptionPane.showMessageDialog(null,"ERROR EN LA OPERACIÓN CON LA BASE DE DATOS");
					
					//Para informar sobre donde o que ocasiona el error
					e.printStackTrace();
		}
	}
	
	//Metodo para buscar al estudiante por la seccion
	public int BusquedaIDSeccion(Modulo Mode) throws SQLException{
		
		//Variables necesarias para buscar el id del vehiculo
		int seccion = Mode.getSeccion();
		try {
			//1. Conexion | Para arreglar lo de la zona horaria "?useTimeZone=true&serverTimezone=UTC"

			if(conexion == null) {
				
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306?useTimezone=true&serverTimezone=UTC", "root", "");
			
				//2. Creacion del Statement
				
				stt = conexion.createStatement();
			}
			
			//Usar una base de datos
			
			String use = "use genoal;"; //Instruccion SQL
			
			stt.executeQuery(use); //Aplicando la insctruccion SQL guardada en use
			
			Mistatement = conexion.prepareStatement("Select id from estudiante where id =?");
			Mistatement.setInt(1,seccion);
			ResultSet rs = Mistatement.executeQuery();
			if(rs.next()) { //Si hay datos en la base de datos
				do {
					int idEstudiante = rs.getInt("id"); 
				}while(rs.next());
			}
		}catch(SQLException e) { JOptionPane.showMessageDialog(null, "Hubo un problema al buscar el id del estudiante"); e.printStackTrace();}
		//return idNotas;
		return 0;
	}
}
