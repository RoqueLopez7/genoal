package Modulo;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Modulo {

	//Atributos
		//Estudiante
		private String Nombres, Apellidos, Cedula;
		private java.sql.Date Fecha;
		private int Seccion, Asignatura;
	
		//Contacto
		private String Telefono, Facebook,Twitter, Instragram, Correo, Telegram, LoL, WoW;

		//Nota de Algoritmo
		private int Nota, Unidad;
		
		//Id de ganadores de maraton
		private int id1, id2, id3;
		
		//Seguridad
		private String Pass, Email;
		
		//Inicializar Fecha
		SimpleDateFormat FechaInicial; 
		java.util.Date F;
		Long FechaLarga;
		java.sql.Date FechaIni;

	public Modulo(){
		//Llamada de metodos
		InicializarEstudiantes();
		IniciarlizarContacto();
		InicializarNota();
		InicializarIdGanadores();
	}

	//--------->Setters<---------
		//--------->Estudiante<---------
		public void setNombres(String Nombres){ this.Nombres = Nombres;}
		public void setApellidos(String Apellidos){ this.Apellidos = Apellidos;}
		public void setCedula(String Cedula){ this.Cedula = Cedula;}
		public void setAsignatura(int Asignatura){ this.Asignatura = Asignatura;}
		public void setFecha_Ingreso(Date Fecha){ this.Fecha = Fecha;}
        public void setSeccion(int Seccion) { this.Seccion = Seccion; }

        //--------->Contacto<---------
		public void setTelefono(String Telefono){ this.Telefono = Telefono;}
		public void setFacebook(String Facebook){ this.Facebook = Facebook;}
		public void setTwitter(String Twitter){ this.Twitter = Twitter;}
		public void setInstagram(String Instragram){ this.Instragram = Instragram;}
		public void setCorreo(String Correo){ this.Correo = Correo;}
		public void setTelegram(String Telegram){ this.Telegram = Telegram;}
		public void setLoL(String LoL){ this.LoL = LoL;}
		public void setWoW(String WoW) { this.WoW = WoW; };

		//--------->Nota Algoritmo<---------		
		public void setNotaAlgoritmo(int Nota) { this.Nota = Nota;}
		
		//--------->Unidad<---------
		public void setUnidad(int Unidad) {this.Unidad = Unidad;}
		
		//--------->Ids de ganadores de maraton<---------
		public void setIdGanador1(int id1) {this.id1 = id1;}
		public void setIdGanador2(int id2) {this.id2 = id2;}
		public void setIdGanador3(int id3) {this.id3 = id3;}
		
		//--------->Seguridad<---------
		public void setPass(String Pass) {this.Pass = Pass;}
		public void setEmail(String Email) {this.Email = Email;}

	//--------->Getters<---------
		//--------->Estudiante<---------
		public String getNombres(){ return Nombres;}
		public String getApellidos(){return Apellidos;}
		public String getCedula(){return Cedula;}
		public int getAsignatura(){return Asignatura;}
		public Date getFecha_Ingreso(){return Fecha;}
		public int getSeccion() {return Seccion;}

		//--------->Contacto<---------
		public String getTelefono(){return Telefono;}
		public String getFacebook(){return Facebook;}
		public String getTwitter(){return Twitter;}
		public String getInstagram(){return Instragram;}
		public String getCorreo(){return Correo;}
		public String getTelegram(){return Telegram;}
		public String getLoL(){return LoL;}
		public String getWoW(){return WoW;}

		//--------->Notas de Algoritmo 1<---------
		public int getNotaAlgoritmo() {return Nota;}
		
		//--------->Unidad<---------
		public int getUnidad() {return Unidad;}
		
		//--------->Id ganadores<---------
		public int getIdGanador1() {return id1;}
		public int getIdGanador2() {return id2;}
		public int getIdGanador3() {return id3;}
		
		//--------->Seguridad<---------
		public String getPass() {return Pass;}
		public String getEmail() {return Email;}

	//--------->INICIALIZAR ATRIBUTOS<---------
		public void InicializarEstudiantes(){
			//Inicializa todos los atributos correspondientes
			this.Nombres= "";
			this.Apellidos = "";
			this.Cedula = "";
			this.Asignatura = 0;
			this.Fecha = FechaInicial();
			this.Seccion = 0;
		}

		public void IniciarlizarContacto(){
			//Inicializa todos los atributos correspondientes
			this.Telefono = "";
			this.Facebook = "";
			this.Twitter = "";
			this.Instragram = "";
			this.Correo = "";
			this.Telegram = "";
			this.LoL = "";
		}

		public void InicializarNota(){
			//Inicializa todos los atributos correspondientes
			Nota = 0;
			Unidad = 0;
		}
		
		public void InicializarIdGanadores(){
			//Inicializa todos los atributos correspondientes
			id1 = 0;
			id2 = 0;
			id3 = 0;
		}
		
		public void InicializarSeguridad() {
			
			//Inicializa todos los atributos correspondientes
			this.Pass = "";
			this.Email = "";
		}
		
		public java.sql.Date FechaInicial(){
			
			//Fecha inicial
			FechaInicial = new SimpleDateFormat("yyyy-MM-dd");

			try {
				F = FechaInicial.parse("2021-01-01"); //Colocando el valor inicial
				
				FechaLarga = F.getTime(); //pasando a una variable larga
				
				FechaIni = new java.sql.Date(FechaLarga); //transformandolo a un dato aceptable para SQL
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //estableciendo fecha inicial
			
			//Retorna el valor inicial de F
			return FechaIni;
		}
}
